USE [master]
GO
/****** Object:  Database [TGP]    Script Date: 02-03-2023 14:57:06 ******/
CREATE DATABASE [TGP]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'VIstara', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\VIstara.mdf' , SIZE = 15360KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'VIstara_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\VIstara_log.ldf' , SIZE = 15040KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [TGP] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [TGP].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [TGP] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [TGP] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [TGP] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [TGP] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [TGP] SET ARITHABORT OFF 
GO
ALTER DATABASE [TGP] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [TGP] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [TGP] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [TGP] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [TGP] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [TGP] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [TGP] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [TGP] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [TGP] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [TGP] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [TGP] SET  DISABLE_BROKER 
GO
ALTER DATABASE [TGP] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [TGP] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [TGP] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [TGP] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [TGP] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [TGP] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [TGP] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [TGP] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [TGP] SET  MULTI_USER 
GO
ALTER DATABASE [TGP] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [TGP] SET DB_CHAINING OFF 
GO
ALTER DATABASE [TGP] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [TGP] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [TGP]
GO
/****** Object:  UserDefinedFunction [dbo].[fnBase64ToBinary]    Script Date: 02-03-2023 14:57:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnBase64ToBinary]
(
    @Str AS NVARCHAR(MAX)
)
RETURNS VARBINARY(MAX)
AS
BEGIN
    RETURN (
        SELECT
                CONVERT(
                    VARBINARY(MAX), CAST('' AS XML).value('xs:base64Binary(sql:column("BASE64_COLUMN"))', 'VARBINARY(MAX)')
                )
        FROM    (SELECT @Str AS BASE64_COLUMN) A
    );
END;

GO
/****** Object:  UserDefinedFunction [dbo].[fnBinaryToBase64]    Script Date: 02-03-2023 14:57:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnBinaryToBase64]
(
    @Var AS VARBINARY(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
    RETURN (
        SELECT  @Var AS '*' FOR XML PATH('')
    );
END;

GO
/****** Object:  Table [dbo].[Test]    Script Date: 02-03-2023 14:57:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Test](
	[ID] [nchar](10) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TGP_COMMON_LIST_MASTER]    Script Date: 02-03-2023 14:57:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TGP_COMMON_LIST_MASTER](
	[VALUE] [nvarchar](max) NULL,
	[CATEGORY] [nvarchar](100) NULL,
	[CATEGORY_ID] [numeric](18, 0) NULL,
	[SOFTDELETE] [nchar](1) NULL,
	[SORT_ORDER] [numeric](18, 0) NULL,
	[DESCRIPTION] [nvarchar](50) NULL,
	[CREATED_DATE] [datetime] NULL,
	[CREATED_BY] [nvarchar](200) NULL,
	[MODIFIED_BY] [nvarchar](200) NULL,
	[MODIFIED_DATE] [datetime] NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ADDITIONAL_DATA] [nvarchar](max) NULL,
 CONSTRAINT [PK_YourTable] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TGP_MERCHANT_MASTER]    Script Date: 02-03-2023 14:57:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TGP_MERCHANT_MASTER](
	[MERCHANT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[NAME] [nvarchar](50) NULL CONSTRAINT [DF__MERCHANT_M__NAME__76969D2E]  DEFAULT (NULL),
	[CONTACT_NO] [nvarchar](50) NULL CONSTRAINT [DF__MERCHANT___CONTA__778AC167]  DEFAULT (NULL),
	[EMAIL] [nvarchar](50) NULL CONSTRAINT [DF__MERCHANT___EMAIL__787EE5A0]  DEFAULT (NULL),
	[STATUS] [nvarchar](50) NULL CONSTRAINT [DF__MERCHANT___STATU__797309D9]  DEFAULT (NULL),
	[EXPIRY_DATE] [date] NULL CONSTRAINT [DF_TGP_MERCHANT_MASTER_EXPIRY_DATE]  DEFAULT (getdate()),
	[CREATED_BY] [nvarchar](30) NULL CONSTRAINT [DF__MERCHANT___CREAT__7A672E12]  DEFAULT (NULL),
	[CREATED_DATE] [datetime] NULL,
	[MODIFIED_BY] [nvarchar](30) NULL CONSTRAINT [DF__MERCHANT___MODIF__7B5B524B]  DEFAULT (NULL),
	[MODIFIED_DATE] [datetime] NULL,
 CONSTRAINT [PK_MERCHANT_MASTER] PRIMARY KEY CLUSTERED 
(
	[MERCHANT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TGP_MERCHANT_POLICY_MAPPING]    Script Date: 02-03-2023 14:57:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TGP_MERCHANT_POLICY_MAPPING](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MERCHANT_ID] [numeric](18, 0) NULL CONSTRAINT [DF__MERCHANT___MERCH__7D439ABD]  DEFAULT (NULL),
	[POLICY_NO] [nvarchar](30) NULL CONSTRAINT [DF__MERCHANT___POLIC__7E37BEF6]  DEFAULT (NULL),
	[STATUS] [nvarchar](30) NULL CONSTRAINT [DF__MERCHANT___STATU__7F2BE32F]  DEFAULT (NULL),
	[CREATED_BY] [nvarchar](30) NULL CONSTRAINT [DF__MERCHANT___CREAT__00200768]  DEFAULT (NULL),
	[CREATED_DATE] [datetime] NULL CONSTRAINT [DF__MERCHANT___CREAT__01142BA1]  DEFAULT (getdate()),
	[MODIFIED_BY] [nvarchar](30) NULL CONSTRAINT [DF__MERCHANT___MODIF__02084FDA]  DEFAULT (NULL),
	[MODIFIED_DATE] [datetime] NULL,
 CONSTRAINT [PK_MERCHANT_POLICY_MAPPING] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TGP_NOMINEE_DETAILS]    Script Date: 02-03-2023 14:57:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TGP_NOMINEE_DETAILS](
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[customer_id] [nvarchar](20) NOT NULL,
	[nominee1_title] [nvarchar](10) NULL,
	[nominee1_name] [nvarchar](100) NULL,
	[nominee1_gender] [nvarchar](10) NULL,
	[nominee1_dob] [nvarchar](10) NULL,
	[nominee1_mobile_number] [nvarchar](10) NULL,
	[nominee1_email_id] [nvarchar](100) NULL,
	[nominee1_relationship] [nvarchar](20) NULL,
	[nominee1_percentage] [nvarchar](3) NULL,
	[appointee1_title] [nvarchar](10) NULL,
	[appointee1_name] [nvarchar](100) NULL,
	[appointee1_relationship] [nvarchar](20) NULL,
	[appointee1_gender] [nvarchar](10) NULL,
	[appointee1_dob] [nvarchar](10) NULL,
	[nominee2_title] [nvarchar](10) NULL,
	[nominee2_name] [nvarchar](100) NULL,
	[nominee2_gender] [nvarchar](10) NULL,
	[nominee2_dob] [nvarchar](10) NULL,
	[nominee2_mobile_number] [nvarchar](20) NULL,
	[nominee2_email_id] [nvarchar](100) NULL,
	[nominee2_relationship] [nvarchar](20) NULL,
	[nominee2_percentage] [nvarchar](3) NULL,
	[appointee2_title] [nvarchar](10) NULL,
	[appointee2_name] [nvarchar](100) NULL,
	[appointee2_relationship] [nvarchar](20) NULL,
	[appointee2_gender] [nvarchar](10) NULL,
	[appointee2_dob] [nvarchar](10) NULL,
	[created_by] [nvarchar](50) NULL,
	[created_date] [datetime] NULL CONSTRAINT [DF_TGP_nominee_details_created_date]  DEFAULT (getdate()),
	[modified_by] [nvarchar](50) NULL,
	[modified_date] [datetime] NULL CONSTRAINT [DF_TGP_nominee_details_modified_date]  DEFAULT (getdate()),
 CONSTRAINT [PK_nominee_details] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TGP_OTP_TRN_DETAILS]    Script Date: 02-03-2023 14:57:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TGP_OTP_TRN_DETAILS](
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[customer_id] [nvarchar](20) NULL,
	[otp_id] [nvarchar](20) NULL,
	[otp_code] [numeric](18, 0) NULL,
	[otp_mobile] [numeric](18, 0) NULL,
	[otp_email] [nvarchar](100) NULL,
	[otp_verified] [char](1) NULL CONSTRAINT [DF_TGP_otp_trn_details_otp_verified]  DEFAULT ('N'),
	[otp_gen_time] [datetime] NULL CONSTRAINT [DF_TGP_otp_trn_details_otp_gen_time]  DEFAULT (sysdatetime()),
	[otp_verified_time] [datetime] NULL,
	[created_date] [datetime] NULL CONSTRAINT [DF_TGP_otp_trn_details_created_date]  DEFAULT (sysdatetime()),
 CONSTRAINT [PK_otp_trn_details] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TGP_PAYMENT_DETAILS]    Script Date: 02-03-2023 14:57:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TGP_PAYMENT_DETAILS](
	[id] [nvarchar](50) NULL,
	[customer_id] [nvarchar](20) NULL,
	[bank_ref_num] [nvarchar](100) NULL,
	[mihpayid] [nchar](50) NULL,
	[transaction_description] [nvarchar](50) NULL,
	[mode] [nvarchar](50) NULL,
	[bank_code] [nvarchar](50) NULL,
	[status] [nvarchar](50) NULL,
	[unmapped_status] [nvarchar](50) NULL,
	[response_json] [nvarchar](max) NULL,
	[error] [nvarchar](50) NULL,
	[error_Message] [nvarchar](max) NULL,
	[created_date] [datetime] NULL CONSTRAINT [DF_TGP_PAYMENT_DETAILS_created_date_1]  DEFAULT (getdate()),
	[modified_date] [datetime] NULL CONSTRAINT [DF_TGP_PAYMENT_DETAILS_modified_date_1]  DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TGP_PERSONAL_DETAILS]    Script Date: 02-03-2023 14:57:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TGP_PERSONAL_DETAILS](
	[customer_id] [nvarchar](20) NOT NULL,
	[urn] [nvarchar](20) NULL,
	[tier] [nvarchar](20) NULL,
	[name] [nvarchar](100) NULL,
	[dob] [nvarchar](10) NULL,
	[email_id] [nvarchar](100) NULL,
	[mobile_number] [numeric](20, 0) NULL,
	[merchant_id] [numeric](18, 0) NULL,
	[policy_number] [nvarchar](20) NULL,
	[sum_assured] [numeric](18, 2) NULL,
	[premium_amount] [numeric](18, 2) NULL,
	[premium_without_gst] [numeric](18, 2) NULL,
	[premium_gst] [numeric](18, 2) NULL,
	[total_premium] [numeric](18, 2) NULL,
	[rider_without_gst] [numeric](18, 2) NULL,
	[rider_premium_gst] [numeric](18, 2) NULL,
	[total_rider] [numeric](18, 2) NULL,
	[gender] [nvarchar](15) NULL,
	[pan_no] [nvarchar](10) NULL,
	[address1] [nvarchar](200) NULL,
	[address2] [nvarchar](200) NULL,
	[address3] [nvarchar](200) NULL,
	[pin_code] [numeric](10, 0) NULL,
	[city] [nvarchar](100) NULL,
	[state] [nvarchar](100) NULL,
	[organization_type] [nvarchar](20) NULL,
	[occupation] [nvarchar](20) NULL,
	[annual_income] [numeric](18, 0) NULL,
	[nationality] [nvarchar](50) NULL,
	[residential_status] [nvarchar](50) NULL,
	[country_residence] [nvarchar](50) NULL,
	[good_health_flag] [nchar](1) NULL,
	[stage] [nvarchar](1) NULL,
	[coi] [nvarchar](50) NULL,
	[coi_pdf] [image] NULL,
	[mef_pdf] [image] NULL,
	[is_submitted] [nchar](1) NULL CONSTRAINT [DF_TGP_PERSONAL_DETAILS_is_submitted]  DEFAULT (N'N'),
	[is_drop_link_sent] [nchar](1) NULL CONSTRAINT [DF_TGP_PERSONAL_DETAILS_is_drop_link_sent]  DEFAULT ('N'),
	[drop_link] [nvarchar](1000) NULL,
	[created_date] [datetime] NULL CONSTRAINT [DF_TGP_personal_details_created_date]  DEFAULT (getdate()),
	[created_by] [nvarchar](50) NULL,
	[modified_date] [datetime] NULL CONSTRAINT [DF_TGP_personal_details_modified_date]  DEFAULT (getdate()),
	[modified_by] [nvarchar](50) NULL,
 CONSTRAINT [PK_personal_details] PRIMARY KEY CLUSTERED 
(
	[customer_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TGP_POLICY_MASTER]    Script Date: 02-03-2023 14:57:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TGP_POLICY_MASTER](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[POLICY_NO] [nvarchar](30) NULL CONSTRAINT [DF__POLICY_MA__POLIC__70DDC3D8]  DEFAULT (NULL),
	[BENEFIT_COVERAGE] [nvarchar](30) NULL CONSTRAINT [DF__POLICY_MA__BENEF__71D1E811]  DEFAULT (NULL),
	[POLICY_TERM] [nvarchar](100) NULL CONSTRAINT [DF__POLICY_MA__POLIC__72C60C4A]  DEFAULT (NULL),
	[PLAN_NAME] [nvarchar](100) NULL,
	[PLAN_BRIEF] [nvarchar](100) NULL,
	[SUB_OFFICE_CODE] [nvarchar](100) NULL,
	[CREATED_BY] [nvarchar](30) NULL CONSTRAINT [DF__POLICY_MA__CREAT__73BA3083]  DEFAULT (NULL),
	[CREATED_DATE] [datetime] NULL,
	[MODIFIED_BY] [nvarchar](30) NULL CONSTRAINT [DF__POLICY_MA__MODIF__74AE54BC]  DEFAULT (NULL),
	[MODIFIED_DATE] [datetime] NULL,
 CONSTRAINT [PK_POLICY_MASTER] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TGP_PROPERTY_CONFIGS]    Script Date: 02-03-2023 14:57:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TGP_PROPERTY_CONFIGS](
	[DESCRIPTION] [nvarchar](250) NULL DEFAULT (NULL),
	[PROPERTY] [nvarchar](50) NULL DEFAULT (NULL),
	[VALUE] [nvarchar](500) NULL DEFAULT (NULL),
	[IS_FRONT_END] [nvarchar](5) NULL DEFAULT (NULL),
	[ID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_VGP_PROPERTY_CONFIGS] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TGP_SERVICE_CALL_LOGS]    Script Date: 02-03-2023 14:57:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TGP_SERVICE_CALL_LOGS](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CUSTOMER_ID] [varchar](20) NULL CONSTRAINT [DF__service_c__CUSTO__173876EA]  DEFAULT (NULL),
	[SERVICE] [varchar](100) NULL CONSTRAINT [DF__service_c__SERVI__182C9B23]  DEFAULT (NULL),
	[SOURCE_IP] [varchar](20) NULL CONSTRAINT [DF__service_c__SOURC__1920BF5C]  DEFAULT (NULL),
	[STATUS_CODE] [varchar](50) NULL CONSTRAINT [DF__service_c__STATU__1A14E395]  DEFAULT (NULL),
	[ERROR_MSG] [varchar](500) NULL CONSTRAINT [DF__service_c__ERROR__1B0907CE]  DEFAULT (NULL),
	[TIME_TAKEN] [varchar](20) NULL CONSTRAINT [DF__service_c__TIME___1BFD2C07]  DEFAULT (NULL),
	[REQUEST_OBJ] [varchar](5000) NULL CONSTRAINT [DF__service_c__REQUE__1CF15040]  DEFAULT (NULL),
	[REQUEST_TIME] [datetime] NULL CONSTRAINT [DF__service_c__REQUE__1DE57479]  DEFAULT (NULL),
	[RESPONSE_OBJ] [varchar](5000) NULL CONSTRAINT [DF__service_c__RESPO__1ED998B2]  DEFAULT (NULL),
	[RESPONSE_TIME] [datetime] NULL CONSTRAINT [DF__service_c__RESPO__1FCDBCEB]  DEFAULT (NULL),
	[CREATED_DATE] [datetime] NULL CONSTRAINT [DF__service_c__CREAT__20C1E124]  DEFAULT (NULL),
 CONSTRAINT [PK_service_call_logs] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TGP_STATE_MASTER]    Script Date: 02-03-2023 14:57:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TGP_STATE_MASTER](
	[ID] [int] NULL,
	[STATE_NAME] [nvarchar](100) NULL,
	[STATUS] [nchar](1) NULL
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[ALL_DETAILS]    Script Date: 02-03-2023 14:57:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ALL_DETAILS] AS 
SELECT customer_id
      ,[urn]
      ,[tier]
      ,[name]
      ,[dob]
      ,[email_id]
      ,[mobile_number]
      ,[sum_assured]
      ,[premium_amount]
      ,[gender]
      ,[pan_no]
      ,[address1]
      ,[address2]
      ,[address3]
      ,[pin_code]
      ,[city]
      ,[state]
      ,[organization_type]
      ,[occupation]
      ,[annual_income]
      ,[nationality]
      ,[residential_status]
      ,[country_residence]
      ,[good_health_flag]
  FROM TGP_personal_details

GO
/****** Object:  View [dbo].[VW_ALL_DETAILS]    Script Date: 02-03-2023 14:57:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_ALL_DETAILS]
AS
SELECT dbo.TGP_PERSONAL_DETAILS.customer_id, dbo.TGP_PERSONAL_DETAILS.urn, dbo.TGP_PERSONAL_DETAILS.tier, dbo.TGP_PERSONAL_DETAILS.name, dbo.TGP_PERSONAL_DETAILS.dob, dbo.TGP_PERSONAL_DETAILS.email_id, 
                  dbo.TGP_PERSONAL_DETAILS.mobile_number, dbo.TGP_PERSONAL_DETAILS.sum_assured, dbo.TGP_PERSONAL_DETAILS.premium_amount, dbo.TGP_PERSONAL_DETAILS.gender, dbo.TGP_PERSONAL_DETAILS.pan_no, 
                  dbo.TGP_PERSONAL_DETAILS.address1, dbo.TGP_PERSONAL_DETAILS.address2, dbo.TGP_PERSONAL_DETAILS.address3, dbo.TGP_PERSONAL_DETAILS.pin_code, dbo.TGP_PERSONAL_DETAILS.city, 
                  dbo.TGP_PERSONAL_DETAILS.state, dbo.TGP_PERSONAL_DETAILS.organization_type, dbo.TGP_PERSONAL_DETAILS.occupation, dbo.TGP_PERSONAL_DETAILS.annual_income, dbo.TGP_PERSONAL_DETAILS.nationality, 
                  dbo.TGP_PERSONAL_DETAILS.residential_status, dbo.TGP_PERSONAL_DETAILS.country_residence, dbo.TGP_PERSONAL_DETAILS.good_health_flag, dbo.TGP_PERSONAL_DETAILS.stage, 
                  dbo.TGP_NOMINEE_DETAILS.nominee1_title, dbo.TGP_NOMINEE_DETAILS.nominee1_name, dbo.TGP_NOMINEE_DETAILS.nominee1_gender, dbo.TGP_NOMINEE_DETAILS.nominee1_dob, 
                  dbo.TGP_NOMINEE_DETAILS.nominee1_mobile_number, dbo.TGP_NOMINEE_DETAILS.nominee1_email_id, dbo.TGP_NOMINEE_DETAILS.nominee1_relationship, dbo.TGP_NOMINEE_DETAILS.nominee1_percentage, 
                  dbo.TGP_NOMINEE_DETAILS.appointee1_title, dbo.TGP_NOMINEE_DETAILS.appointee1_name, dbo.TGP_NOMINEE_DETAILS.appointee1_relationship, dbo.TGP_NOMINEE_DETAILS.appointee1_gender, 
                  dbo.TGP_NOMINEE_DETAILS.nominee2_title, dbo.TGP_NOMINEE_DETAILS.nominee2_name, dbo.TGP_NOMINEE_DETAILS.nominee2_gender, dbo.TGP_NOMINEE_DETAILS.nominee2_dob, 
                  dbo.TGP_NOMINEE_DETAILS.nominee2_mobile_number, dbo.TGP_NOMINEE_DETAILS.nominee2_email_id, dbo.TGP_NOMINEE_DETAILS.nominee2_relationship, dbo.TGP_NOMINEE_DETAILS.nominee2_percentage, 
                  dbo.TGP_NOMINEE_DETAILS.appointee2_title, dbo.TGP_NOMINEE_DETAILS.appointee2_name, dbo.TGP_NOMINEE_DETAILS.appointee2_relationship, dbo.TGP_NOMINEE_DETAILS.appointee2_gender, 
                  dbo.TGP_NOMINEE_DETAILS.appointee1_dob, dbo.TGP_NOMINEE_DETAILS.appointee2_dob, dbo.TGP_PERSONAL_DETAILS.coi, dbo.TGP_PERSONAL_DETAILS.policy_number, dbo.TGP_PERSONAL_DETAILS.merchant_id, 
                  dbo.TGP_POLICY_MASTER.PLAN_NAME, dbo.TGP_POLICY_MASTER.PLAN_BRIEF, dbo.TGP_PERSONAL_DETAILS.premium_without_gst, dbo.TGP_PERSONAL_DETAILS.premium_gst, dbo.TGP_PERSONAL_DETAILS.total_premium, 
                  dbo.TGP_PERSONAL_DETAILS.rider_without_gst, dbo.TGP_PERSONAL_DETAILS.rider_premium_gst, dbo.TGP_PERSONAL_DETAILS.total_rider, dbo.TGP_POLICY_MASTER.SUB_OFFICE_CODE
FROM     dbo.TGP_PERSONAL_DETAILS LEFT OUTER JOIN
                  dbo.TGP_NOMINEE_DETAILS ON dbo.TGP_PERSONAL_DETAILS.customer_id = dbo.TGP_NOMINEE_DETAILS.customer_id LEFT OUTER JOIN
                  dbo.TGP_POLICY_MASTER ON dbo.TGP_PERSONAL_DETAILS.policy_number = dbo.TGP_POLICY_MASTER.POLICY_NO

GO
/****** Object:  View [dbo].[VW_POLICY_DETAILS]    Script Date: 02-03-2023 14:57:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_POLICY_DETAILS]
AS
SELECT dbo.TGP_MERCHANT_MASTER.NAME, dbo.TGP_MERCHANT_MASTER.MERCHANT_ID, dbo.TGP_POLICY_MASTER.POLICY_NO, dbo.TGP_POLICY_MASTER.BENEFIT_COVERAGE, dbo.TGP_POLICY_MASTER.POLICY_TERM, 
                  dbo.TGP_POLICY_MASTER.PLAN_NAME, dbo.TGP_POLICY_MASTER.PLAN_BRIEF, dbo.TGP_POLICY_MASTER.SUB_OFFICE_CODE
FROM     dbo.TGP_MERCHANT_MASTER INNER JOIN
                  dbo.TGP_MERCHANT_POLICY_MAPPING ON dbo.TGP_MERCHANT_MASTER.MERCHANT_ID = dbo.TGP_MERCHANT_POLICY_MAPPING.MERCHANT_ID INNER JOIN
                  dbo.TGP_POLICY_MASTER ON dbo.TGP_MERCHANT_POLICY_MAPPING.ID = dbo.TGP_POLICY_MASTER.ID

GO
SET IDENTITY_INSERT [dbo].[TGP_COMMON_LIST_MASTER] ON 

INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'<html>
<body>
<br>
<p>
	Thank you very much for your interest in Group Voluntary Life Top up policy of Tata AIA life insurance company for employees for Wipro Limited and related entities.
</p>
<p>
Your unique reference number is ||CustomerID||. 
</p>
<p>
Please mention this unique reference number in subject line for any future email communication to groupvoluntarysupport@tataaia.com.
</p>
<p>
<b>Please note this is not a confirmation of commencement of insurance policy / death benefit cover for you.</b>  
</p>
<p>
<b>Commencement of death benefit coverage is subject to Underwriting guidelines for the insurer, and acceptance of your cover by the underwriter and receipt of applicable premium plus GST by the insurer.</b>
</p>
<p> 
Applicable premium may be at standard rate (mentioned on Tata AIA link while you registered) /rate up basis your specific health condition. You may be required to undergo medical diagnostic test as per the underwriting guidelines of the insurer. Decision of applicability of standard rate or change in premium shall be as per decision of Underwriters. Accordingly, additional premium will be demanded from you and in case of decline any premium plus taxes paid by you shall be refunded.
</p>
<p>
Confirmation of insurance cover shall be via a separate email once your case is accepted.
</p>
</body>
</html>', N'mailBody', CAST(83 AS Numeric(18, 0)), N'1', CAST(1 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'I have never had any disorder of the heart or circulatory system, high blood pressure, stroke, asthma, or other lung condition, cancer or tumour of any kind, diabetes, 
hepatitis or liver condition, urinary or kidney disorder, depression, mental or psychiatric condition, epilepsy, HIV infection or a positive test to HIV, any disease of the 
brain or the nervous system, blood disorder. I do not currently have, nor received treatment for any medical conditions, disabilities. I do not suffer from any symptoms 
that have persisted for more than seven days. I have not been absent from work due to illness or injury for a continuous period of more than 10 days during the last 
3 years. 
This health Declaration shall form the basis of your enrolment. Any fraud, misstatement of or suppression of a material fact under the policy shall be dealt in 
accordance with Section 45 of the Insurance Act, 1938 as amended from time to time.', N'doghDescription', CAST(1 AS Numeric(18, 0)), N'1', CAST(1 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 2, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'I have never ever (i) had any of the health conditions and/or symptoms mentioned above and (ii) been- by an insurer- declined/deferred, or accepted at special terms , or had any covers reduced for any life, health or accident insurance cover.', N'radio1', CAST(1 AS Numeric(18, 0)), N'1', CAST(2 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 3, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'I may have (i) had some/all of the health conditions and/or symptoms mentioned above OR (ii) been -by an insurer-declined/deferred, or accepted at special terms, or had any cover reduced for any life, health or accident insurance cover.', N'radio2', CAST(1 AS Numeric(18, 0)), N'1', CAST(3 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 4, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'F:/CR/Vistara', N'pdf_path', CAST(2 AS Numeric(18, 0)), N'1', CAST(1 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 5, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (NULL, N'app_form', CAST(3 AS Numeric(18, 0)), N'1', CAST(1 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 6, N'  <html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
      <title>Tata Aia Life Insurance Company limited</title>
      <style type="text/css">
         .tataTable-divider {
         border: 15px solid #0075bc;
         padding: 10px;
         width: 100%;
         margin: 0 auto;
         }
         table.borderTable {
         border: 1px solid #333;
         border-collapse: collapse;
         width: 100%
         }
         table.borderTable th {
         border: 1px solid #333;
         padding: 3px;
         margin: 0;
         border-collapse: collapse;
         color: #333;
         font-weight: 700
         }
         table.borderTable td {
         border: 1px solid #333;
         padding: 3px;
         margin: 0;
         border-collapse: collapse
         }
         h2.blueHeader {
         background: #0075bc;
         color: #fff;
         margin: 2px 0 0;
         padding: 5px 10px;
         font-weight: 700
         }
         img.rupee {
         height: 6px;
         width: 6px
         }
         .kfdform {
         background: #0075bc;
         color: #fff;
         margin: 0 auto;
         padding:8px 10px;
         text-align: center;
         font-weight: 700;
         font-size: 11px;
         }
         .kfd {
         width: 190px;
         background: #0075bc;
         color: #fff;
         margin: 0 auto;
         padding: 4px 10px;
         text-align: center;
         font-weight: 700;
         font-size: 12px;
         }
         h2.kfdblueHeader {
         width: auto;
         background: #0075bc;
         color: #fff;
         margin: 0px 0px 0px 0px;
         padding: 3px 10px;
         font-weight: 700;
         border-radius: 0;
         text-align: left;
         display: block;
         }
         .content {
         width: 97%;
         color: #333;
         padding: 4px 10px;
         font-weight: 400;
         font-size: 8px;
         }
         .note {
         width: 97%;
         color: #333;
         padding: 4px 10px;
         font-weight: 400;
         text-align: left;
         font-size: 7px;
         }
         .address {
         width: 97%;
         color: #333;
         padding: 4px 10px;
         font-weight: 400;
         text-align: center;
         font-size: 8px;
         margin: 0 auto;
         }
         .td-txt-center tr td {
         text-align: center;
         }
         table {
         width: 100%
         }
         table, td {
         vertical-align: middle;
         border-collapse: collapse
         }
         html {
         font-family: times;
         padding: 0;
         margin: 0
         }
         body {
         line-height: 1.1;
         color: #555;
         padding: 0;
         margin: 0;
         font-family: Arial, Helvetica, sans-serif;
         }
         * {
         -webkit-box-sizing: border-box;
         -moz-box-sizing: border-box;
         box-sizing: border-box;
         font-family: ''Roboto'', sans-serif;
         border-collapse: collapse
         }
         .fullwidth {
         width: 100%
         }
         :before, :after {
         -webkit-box-sizing: border-box;
         -moz-box-sizing: border-box;
         box-sizing: border-box
         }
         img.rupee {
         width: 5px;
         position: relative;
         top: 2px;
         }
         .tataTable {
         width:100%;
         margin: 0 auto;
         padding: 10px;
         word-wrap: break-word;
         }
         .bdr-table {
         border: 10px solid #0072bc
         }
         .tataTable tr td {
         padding:2px 4px !important;
         font-size:10px !important;
         }
         .draft-left-equal-td td:first-child {
         width: 40%;
         }
         .photoAffix {
         width: 110px;
         height: 110px;
         margin: 0 0 5px;
         display: inline-block;
         border: 1px solid #bcbdc0;
         vertical-align: top;
         }
         .pull-left {
         float: left;
         }
         .td-with-bg {
         background: #0072bc;
         padding: 4px;
         color: #fff;
         margin: 2px 0px;
         }
         .borderTable tr td {
         border: 1px solid #000;
         }
         p {
         margin: 0 0 4px 0;
         }
         table.sub-table tr td {
         border: 0;
         }
         .text-center {
         text-align: center
         }
         .tearLine {
         border-bottom: 1px solid #333;
         width: 100%;
         margin: 15px 0;
         height: 1px;
         }
         input[type="checkbox"]{
         vertical-align: middle;
         }
         input{ 
         height:20px;
         }
         input.bordernone{
         border:none;
         border-bottom:1px solid #000;
         }
         textarea{
         text-align: center;
         }
         .boxflex{
         float: left;
         margin-right: 10px;
         }
         .checkbox{
         text-align: center;
         height:10px;
         border: 1px solid #888;
         width: 10px;
         border-radius: 2px;
         float: left;
         margin-right:2px; 
         }
         .checkbox.active 
         {
         background-repeat:no-repeat;
         background-position: 1px; 
		 background-color : black;
         background-image: url(''data:image/png+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pg0KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPg0KPHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJDYXBhXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4Ig0KCSB2aWV3Qm94PSIwIDAgNDkwIDQ5MCIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNDkwIDQ5MDsiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KPHBvbHlnb24gcG9pbnRzPSI0NTIuMjUzLDI4LjMyNiAxOTcuODMxLDM5NC42NzQgMjkuMDQ0LDI1Ni44NzUgMCwyOTIuNDY5IDIwNy4yNTMsNDYxLjY3NCA0OTAsNTQuNTI4ICIvPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPC9zdmc+DQo='');
         }     
         .border{
         display: inline-flex; 
         align-items: center;         
         text-align: center; 
         margin: 0px auto; 
         height:18px; 
         border: 1px solid #333;
         padding:3px;
         }
         @media print {
         .pagebreakprint {
         page-break-before: always;
         }
         }
      </style>
   </head>
   <body>
      <table class="tataTable" cellpadding="0" cellspacing="0" border="0" style="padding-top: 0px; margin-top: 0px;">
      <tbody>
         <tr>
            <td>
               <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tataTable-divider" style="padding-top: 0">
                  <tbody>
                     <tr>
                        <td style="padding-top: 0;">
                           <table style="width: 100%; border-collapse: collapse; margin: 0px auto" cellpadding="0" cellspacing="0" border="0">
                              <tbody>
                                 <tr>
                                    <td width="25%" style="padding: 3px 0 0 10px"> </td>
                                    <td width="45%" style="text-align: center">
                                       <h4 style="font-size:14px; margin:20px 0px 5px 0px;">Tata AIA Life Insurance Company Limited</h4>
                                       <div class="kfdform" style=" font-size: 14px;margin:0px 0px 0px 0px;">MEMBER ENROLMENT FORM</div>
                                    </td>
                                    <td width="28%" colspan="2" align="right" style="padding: 10px 10px 0 0; vertical-align: top; text-align: right"> 
                                       <img style="width:155px" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMkAAAAyCAYAAAD2k//fAAAACXBIWXMAAFxGAABcRgEUlENBAAA5+2lUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxMzIgNzkuMTU5Mjg0LCAyMDE2LzA0LzE5LTEzOjEzOjQwICAgICAgICAiPgogICA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPgogICAgICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgICAgICAgICB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iCiAgICAgICAgICAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIKICAgICAgICAgICAgeG1sbnM6cGhvdG9zaG9wPSJodHRwOi8vbnMuYWRvYmUuY29tL3Bob3Rvc2hvcC8xLjAvIgogICAgICAgICAgICB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIKICAgICAgICAgICAgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIKICAgICAgICAgICAgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iCiAgICAgICAgICAgIHhtbG5zOmV4aWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vZXhpZi8xLjAvIj4KICAgICAgICAgPHhtcDpDcmVhdG9yVG9vbD5BZG9iZSBQaG90b3Nob3AgQ0MgMjAxNS41IChNYWNpbnRvc2gpPC94bXA6Q3JlYXRvclRvb2w+CiAgICAgICAgIDx4bXA6Q3JlYXRlRGF0ZT4yMDE5LTAxLTA0VDE1OjM1OjMyKzA1OjMwPC94bXA6Q3JlYXRlRGF0ZT4KICAgICAgICAgPHhtcDpNb2RpZnlEYXRlPjIwMTktMDEtMDlUMTA6NDA6MTIrMDU6MzA8L3htcDpNb2RpZnlEYXRlPgogICAgICAgICA8eG1wOk1ldGFkYXRhRGF0ZT4yMDE5LTAxLTA5VDEwOjQwOjEyKzA1OjMwPC94bXA6TWV0YWRhdGFEYXRlPgogICAgICAgICA8ZGM6Zm9ybWF0PmltYWdlL3BuZzwvZGM6Zm9ybWF0PgogICAgICAgICA8cGhvdG9zaG9wOkNvbG9yTW9kZT4zPC9waG90b3Nob3A6Q29sb3JNb2RlPgogICAgICAgICA8eG1wTU06SW5zdGFuY2VJRD54bXAuaWlkOjg4NjY5NzVjLTIyYjUtNGUxYi05Mzk1LTdkYzZkYWUzZWFjOTwveG1wTU06SW5zdGFuY2VJRD4KICAgICAgICAgPHhtcE1NOkRvY3VtZW50SUQ+YWRvYmU6ZG9jaWQ6cGhvdG9zaG9wOjIzNDg3ZTA1LTU0NWItMTE3Yy1hOTEyLTlmNmJhYzJkNzI4ZDwveG1wTU06RG9jdW1lbnRJRD4KICAgICAgICAgPHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD54bXAuZGlkOjU0NjAxYjgxLTExNTYtNDEyMS04MGZmLTQ5NzJkYjhkZDY2MzwveG1wTU06T3JpZ2luYWxEb2N1bWVudElEPgogICAgICAgICA8eG1wTU06SGlzdG9yeT4KICAgICAgICAgICAgPHJkZjpTZXE+CiAgICAgICAgICAgICAgIDxyZGY6bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6YWN0aW9uPmNyZWF0ZWQ8L3N0RXZ0OmFjdGlvbj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0Omluc3RhbmNlSUQ+eG1wLmlpZDo1NDYwMWI4MS0xMTU2LTQxMjEtODBmZi00OTcyZGI4ZGQ2NjM8L3N0RXZ0Omluc3RhbmNlSUQ+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDp3aGVuPjIwMTktMDEtMDRUMTU6MzU6MzIrMDU6MzA8L3N0RXZ0OndoZW4+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDpzb2Z0d2FyZUFnZW50PkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE1LjUgKE1hY2ludG9zaCk8L3N0RXZ0OnNvZnR3YXJlQWdlbnQ+CiAgICAgICAgICAgICAgIDwvcmRmOmxpPgogICAgICAgICAgICAgICA8cmRmOmxpIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OmFjdGlvbj5zYXZlZDwvc3RFdnQ6YWN0aW9uPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6aW5zdGFuY2VJRD54bXAuaWlkOjg4NjY5NzVjLTIyYjUtNGUxYi05Mzk1LTdkYzZkYWUzZWFjOTwvc3RFdnQ6aW5zdGFuY2VJRD4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OndoZW4+MjAxOS0wMS0wOVQxMDo0MDoxMiswNTozMDwvc3RFdnQ6d2hlbj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OnNvZnR3YXJlQWdlbnQ+QWRvYmUgUGhvdG9zaG9wIENDIDIwMTUuNSAoTWFjaW50b3NoKTwvc3RFdnQ6c29mdHdhcmVBZ2VudD4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OmNoYW5nZWQ+Lzwvc3RFdnQ6Y2hhbmdlZD4KICAgICAgICAgICAgICAgPC9yZGY6bGk+CiAgICAgICAgICAgIDwvcmRmOlNlcT4KICAgICAgICAgPC94bXBNTTpIaXN0b3J5PgogICAgICAgICA8dGlmZjpPcmllbnRhdGlvbj4xPC90aWZmOk9yaWVudGF0aW9uPgogICAgICAgICA8dGlmZjpYUmVzb2x1dGlvbj42MDAwMDAwLzEwMDAwPC90aWZmOlhSZXNvbHV0aW9uPgogICAgICAgICA8dGlmZjpZUmVzb2x1dGlvbj42MDAwMDAwLzEwMDAwPC90aWZmOllSZXNvbHV0aW9uPgogICAgICAgICA8dGlmZjpSZXNvbHV0aW9uVW5pdD4yPC90aWZmOlJlc29sdXRpb25Vbml0PgogICAgICAgICA8ZXhpZjpDb2xvclNwYWNlPjY1NTM1PC9leGlmOkNvbG9yU3BhY2U+CiAgICAgICAgIDxleGlmOlBpeGVsWERpbWVuc2lvbj4yMDE8L2V4aWY6UGl4ZWxYRGltZW5zaW9uPgogICAgICAgICA8ZXhpZjpQaXhlbFlEaW1lbnNpb24+NTA8L2V4aWY6UGl4ZWxZRGltZW5zaW9uPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAKPD94cGFja2V0IGVuZD0idyI/Pl3kLTYAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAnUpJREFUeAEAOp3FYgH///8AAAAAABB2tXwAAABuAAAA3wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFgAAAMwAAABVAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAXQAAAIwAAADhAAAA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAfAAAAfAAAAJsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAyAAAArAAAAO8AAAD8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABMAAADZAAAATgAAAP0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGQAAACGAAAA4AAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAHQAAAHAAAACpAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD1BAUACv79AKqyoQAn7uoA7ggKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEQAAAGIAAABBAAAAGAAAAOMAAAC+AAAApAAAAO8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAKYAAAAmAAAA7gAAAAAAAAAdAAAAqwAAAHwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABwAAABkAAAAOQAAABEAAADgAAAAsQAAAKcAAAD+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADLgtAAEAAAAAAAAAAAAAAAiAAAAFQAAABUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHcAAAAWAAAAFgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHkAAACbAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADQAAACEAAAAhAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAAAAAQAAAD9AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB5AAAAFQAAABUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB2AAAAqQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/AoMAD3XzgAu6+YA6QoNAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMgAAAMMAAAAKAAAAAAAAAAAAAAAzAAAAUAAAAH8AAABAAAAA1AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAA+AAAAGQAAAAAAAAAAAAAAAAAAADQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAE8AAACwAAAAAAAAAAAAAAAAAAAANQAAAFUAAAB+AAAAPwAAAOgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEIAAAArAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAhAAAAUgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP0AAAD9AAAAAAAAAAAAAAAAAAAAAAAAAAAAAABHAAAAIgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMgAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAgAo7uoADPr6AP0CAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGgAAAL4AAAAKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAATAAAAvgAAABEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD+AAAA/gAAAAAAAAAAAAAAAAAAAAAAAAD8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADMAAACwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJgAAALcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABpAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAbgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABfAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/v8A/gEBAPsCAgAB//8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHEAAAAPAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABUAAAB9AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB9AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwAAAAZwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcAAAATgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABNAAAAMQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJAAAAEoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAXgAAACEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAP0BAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4AAABkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAVgAAAAcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAuAAAATwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcgAAAAYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAABUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABMAAAAEAAAAAAAAAAAAAAAAAAAAO4AAAB6AAAA5QAAAAAAAAAAAAAAAAAAABsAAABRAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAATgAAAAAAAAAAAAAAAAAAAAAAAADZAAAAfAAAAPgAAAAAAAAAAAAAAAAAAAAwAAAANQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAE8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABnAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAAAgAAAAAAAAAAAAAAAAAAAAQAAABYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAawAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaAAAAAAAAAAAAAAAAAAAAAAAAACZAAAAhwAAAJMAAAAAAAAAAAAAAAAAAAAAAAAAXwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwAAAGYAAAAAAAAAAAAAAAAAAAAAAAAAkQAAAIUAAACpAAAAAAAAAAAAAAAAAAAAAAAAAGgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAAAAAAAAAAAAD4AAAAAAAAAPMAAADzAAAA9wAAAPgAAAD3AAAA+gAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wAAAPMAAADyAAAA8wAAAPMAAADzAAAA8wAAAAAAAADyAAAAAAAAAAAAAAAAAAAAAAAAAF0AAAAiAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwAAAE8AAAAAAAAAAAAAAAAAAAAAAAAA/QAAAAAAAAD4AAAA8gAAAPcAAAD5AAAA9wAAAPkAAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD1AAAA8gAAAPMAAADzAAAA8wAAAPMAAAAAAAAA8wAAAP4AAAAAAAAAAAAAAAAAAABnAAAAHwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB8AAABLAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKwAAAD0AAAAAAAAAAAAAAAAAAAAAAAAAuQAAAAAAAAC4AAAAAAAAAAAAAAAAAAAAAAAAAEgAAAAjAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEQAAAAdAAAAAAAAAAAAAAAAAAAAAAAAALUAAAAAAAAApwAAAAAAAAAAAAAAAAAAAAAAAABKAAAABwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAcgAAABAAAAAbAAAAGwAAAB0AAAAeAAAAHQAAAAcAAABbAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOcAAAAcAAAADwAAABsAAAAbAAAAGwAAABsAAAAPAAAAPQAAAAAAAAAAAAAAAAAAAAAAAABYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABuAAAAAAAAAAAAAAAAAAAAAAAAAMcAAAAPAAAAFgAAABsAAAAcAAAAHQAAAB0AAAAIAAAAYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANwAAAA8AAAAbAAAAGwAAABsAAAAbAAAADwAAADIAAAD8AAAAAAAAAAAAAAAAAAAAVgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAbwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGMAAAAAAAAAAAAAAAAAAAAAAAAAygAAAMEAAAAAAAAA0QAAAL0AAAAAAAAAAAAAAAAAAAAAAAAAYgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABqAAAAAAAAAAAAAAAAAAAAAAAAALEAAADiAAAAAAAAALkAAADgAAAAAAAAAAAAAAAAAAAAGAAAAGEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAPgAAADxAAAA8wAAAPMAAADtAAAA6wAAAO0AAAAAAAAA5wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD9AAAA8gAAAAAAAADzAAAA8wAAAPMAAADzAAAA8gAAAPUAAAAAAAAAAAAAAAAAAAAyAAAASgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMgAAAD0AAAAAAAAAAAAAAAAAAAD9AAAA8gAAAPMAAAD0AAAA7gAAAOsAAADtAAAAAAAAAOwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPQAAAAAAAAA8wAAAPMAAADzAAAA8wAAAPIAAAD0AAAAAAAAAAAAAAAAAAAAQQAAAD4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEQAAAAtAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAABSAAAAAAAAAAAAAAAAAAAAAAAAALsAAAAAAAAAAAAAAAAAAAC4AAAAAAAAAAAAAAAAAAAAAAAAAFQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYAAAARwAAAAAAAAAAAAAAAAAAAAAAAACtAAAAAAAAAAAAAAAAAAAAsQAAAAAAAAAAAAAAAAAAAAAAAABQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAWQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAAL//wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABSAAAAHwAAAAAAAAAAAAAAAAAAAAAAAAChAAAAAAAAAAAAAAAAAAAAnwAAAPMAAAAAAAAAAAAAAAAAAAAmAAAAVAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcAAAAAcAAAAAAAAAAAAAAAAAAAD0AAAAtgAAAAAAAAAAAAAAAAAAAKgAAAAAAAAAAAAAAAAAAAAAAAAARwAAADQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABwAAAF0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC7AAAA3wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAVQAAAB0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABYAAABcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAuAAAAO4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFwAAAAOAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAD8AgMAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAXQAAAAAAAAAAAAAAAAAAAAAAAADMAAAA2wAAAAAAAAAAAAAAAAAAAO0AAAC0AAAAAAAAAAAAAAAAAAAAAAAAAFMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFQAAAAAAAAAAAAAAAAAAAAAAAAArgAAAO0AAAAAAAAAAAAAAAAAAADIAAAA2AAAAAAAAAAAAAAAAAAAAAAAAABPAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGAAAAAXAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUAAAALQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUAAABeAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABjAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA5gAAAEkAAADKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAATAAAAWQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+gMDAP8AAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOgAAAEwAAAAAAAAAAAAAAAAAAAAAAAAAnQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAqwAAAAAAAAAAAAAAAAAAAAAAAABYAAAAKgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFwAAAAjAAAAAAAAAAAAAAAAAAAAAAAAAK4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJIAAAAAAAAAAAAAAAAAAAAAAAAAaAAAAAsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAxwAAAPYAAADNAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAagAAAAcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAWQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMEAAAAAAAAAyQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAG4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAPUFBgAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFIAAAAAAAAAAAAAAAAAAAAAAAAA+wAAALEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAK8AAADfAAAAAAAAAAAAAAAAAAAAAAAAAEwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABUAAAAAAAAAAAAAAAAAAAAAAAAANsAAACxAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACzAAAA/QAAAAAAAAAAAAAAAAAAABQAAABPAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAPwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJkAAAAAAAAArwAAAO8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABoAAABOAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUwAAAC0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACaAAAAAAAAAKcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAqAAAAQgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAD7AgMA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4AAABjAAAAAAAAAAAAAAAAAAAAAAAAAMgAAADnAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAzgAAAAAAAAAAAAAAAAAAAAAAAABnAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAtAAAATwAAAAAAAAAAAAAAAAAAAAAAAADNAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA5AAAAMcAAAAAAAAAAAAAAAAAAAAAAAAAZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPgAAAC5AAAAAAAAAPIAAADBAAAAUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAawAAAKsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADeAAAAwAAAAAAAAADZAAAAywAAADUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGsAAAC+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAv//AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABNAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAA4AAAA2AAAANkAAADyAAAA+gAAAAYAAAAMAAAAJgAAACQAAAAAAAAAAAAAAAAAAAAAAAAAIgAAAEMAAAC9AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAATQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAATwAAANEAAADcAAAA9AAAAPwAAAAHAAAAEQAAACcAAAAdAAAAAAAAAAAAAAAAAAAAAAAAAEEAAAApAAAA1wAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFgAAAGQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACkAAAA6AAAAAAAAAAAAAAAoQAAAFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD8AAAAwAAAA0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACkAAABXAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAnwAAAAAAAAAAAAAAAAAAAKMAAAA1AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABPAAAAIAAAAOAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAZgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABQAAACcAAAAnAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABoAAAAvQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAGcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAkAAAAuAAAALgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAXgAAAHkAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAE0AAAAJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAswAAAAAAAAAAAAAAAAAAAL0AAADwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAawAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABRAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALQAAAAAAAAAAAAAAAAAAAC0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwAAAGcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALQAAAD4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAASQAAABQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEUAAAAeAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFgAAAADAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABqAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA1gAAALIAAAAAAAAAAAAAAAAAAADzAAAAqgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFUAAAARAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAAAAaAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALgAAADQAAAAAAAAAAAAAAAAAAAA3wAAALUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABYAAAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAF4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAsAAABjAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAASwAAAAAAAAAAAAAAAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA6AAAAMgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPAAAAcQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUAAAAB0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACxAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACXAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAGsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMAAABVAAAAAAAAAAAAAAAAAAAAAAAAAPIAAADXAAAA+wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD+AAAA1gAAAOsAAAAAAAAAAAAAAAAAAAAAAAAAUwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAWAAAARwAAAAAAAAAAAAAAAAAAAAAAAADrAAAA1wAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+QAAANYAAADzAAAAAAAAAAAAAAAAAAAAAAAAAGAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACbAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADMAAAA2QAAACcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFUAAAB+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADuAAAAowAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwQAAAO0AAAATAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABYAAAAkgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABRAAAAHwAAAAAAAAAAAAAAAAAAAAAAAACxAAAAXgAAABEAAAAxAAAAJAAAABQAAAALAAAA9gAAAOwAAADfAAAA0gAAAOgAAACCAAAAfQAAAAAAAAAAAAAAAAAAADUAAABEAAAAvAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAZgAAAAkAAAAAAAAAAAAAAAAAAAAAAAAAlgAAAIAAAAAZAAAALwAAACAAAAAUAAAACQAAAPQAAADpAAAA3gAAANAAAADwAAAAoQAAAF4AAAAAAAAAAAAAAAAAAABOAAAAHAAAAOQAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFAAAAE8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADGAAAA2gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKIAAACFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAoAAAAWQAAAKcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC0AAABEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAtgAAAPUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPQAAAClAAAAbgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOQAAAEUAAAC7AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAXwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAoQAAAAAAAADvAAAAvgAAAL4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAApAAAAH0AAAAAAAAAAAAAAAAAAAAAAAAAVQAAALwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKMAAAAAAAAA5wAAAOcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKMAAABeAAAAAAAAAAAAAAAAAAAAAwAAAGIAAACCAAAAAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFoAAAANAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAmwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC7AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAF4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABbAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJ0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAuAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABfAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAEwAAAAAAAAAAAAAAAAAAAAAAAAA6AAAAMQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANoAAADKAAAAAAAAAAAAAAAAAAAAAAAAAFwAAAATAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEcAAAAzAAAAAAAAAAAAAAAAAAAAAAAAAMgAAADdAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADEAAAA6QAAAAAAAAAAAAAAAAAAAAAAAABTAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABhAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+QAAALkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAywAAAMEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABIAAAALQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMAAAAYQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANwAAADAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALcAAADVAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAWAAAABsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAE/v4AA///AP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKIAAAD5AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAoAAAAAAAAAAAAAAAAAAAAAAAAAAKAAAAVQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABWAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACiAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+AAAAKQAAAAAAAAAAAAAAAAAAAAAAAAALgAAAE0AAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABCAAAAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKIAAADnAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC3AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUgAAABYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACeAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAuAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMAAABoAAAAAAAAAAAAAAAAAAAAAAAAAAAA//4A9gUFAPoDAwAB//8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABWAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAG8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYAAAAXQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAuAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC5AAAAAAAAAAAAAAAAAAAAAAAAAAAAAABVAAAAAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABwAAAAUAAAAAAAAAAAAAAAAAAAAAAAAAfQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACuAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAowAAAPsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABmAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGAAAAAAAAAAAAAAAAAAAAAAAAAH4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAArgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJ8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAawAAABAAAAAAAAAAAAAAAAAAAAAAAP38AOkKDQD0BQcABP/+AAAAAAAAAAAAAAAAAAAAAAAAAABoAAAAJwAAAAAAAAAAAAAAAAAAAAAAAADLAAAAvQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADdAAAArwAAAAAAAAAAAAAAAAAAAAAAAAAoAAAAWAAAAAAAAAAAAAAAAAAAAAMAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAcQAAAAUAAAAAAAAAAAAAAAAAAAAAAAAArQAAAN8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAuwAAAMwAAAAAAAAAAAAAAAAAAAAAAAAAXQAAAGgAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADXAAAAfgAAAPYAAAD+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACwAAAOkAAAD6AAAAAAAAAAAAAAAAAAAABgAAALUAAAC7AAAA9wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAsAAADoAAAAuAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOYAAABgAAAAJQAAAPIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD+AAAAGgAAAA0AAAAAAAAAAAAAAAAAAAAAAAAA1wAAAIAAAAD0AAAA/gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYAAAD7AAAA6AAAAAAAAAAAAAAAAAAAABMAAAChAAAA+AAAAPwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAA1gAAANkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADVAAAAYwAAABsAAAD0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wAAABUAAAAUAAAA8AAAAAAAAAAAAAAAAAACAwAR+PYACfz7APMGBwAAAAAAAAAAAAAAAAAAAAAAAAAA5QAAAI0AAADjAAAAAgAAAAAAAAATAAAAwwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIYAAADWAAAA7QAAAAAAAAD+AAAAHAAAAPQAAAAAAAAAAAAAAAAAAAD8AAAAfwAAABcAAAD1AAAAAAAAABEAAADNAAAAAAAAAAAAAAAAAAAAAAAAANkAAACHAAAA6gAAAAEAAAABAAAAEAAAAKgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB9AAAAPAAAAO0AAAAAAAAA/gAAABsAAADiAAAAAAH///8AAAAAABB2tQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADxEBQAXszBAJC/sADfDxIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADLgtAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQD+AgIAAv79AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAP8AAAICAP8CCAAJ/QAA/wAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAAAI/QAABf4AAP0BAAD4BAAA/gAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/AAAAB/0AAA38/wAD/wAA/wEAAPIFAQD3AwAAAv8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf/+APsECQD6BAkAA/37AP0FBgAH+/QABP35AP8BAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/AIHAAAACADtBfYA7wYBABb5/wDsBwEA/gAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP4BAAAR+gAA9gMAAOYJAAAJ/QAAGPj/AAABAQD9AQAA/QAAAPMFAAD/AAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAAABAAAAFfkAAPIFAADOEgIA9wMAAAP/AAAr8P8AF/gAAOkIAAAC/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB//4A/AMIAPUHEQAJ+vEAJufCABrv1QDjEzEA4BUzAPcGEAAM+OwAAQD+AP8AAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/AAAAAQD4Bg4A/AIGAAH+/QAv2rAA/fXEAGY6EgBe3wAAQOn+AOUKAQD/AAAA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/AAAACvwAAA38/wAM/AAA0xACAJQnAwDxBQAABv4AADnr/gA66/8A+AQAAAX+AAAn8f4A+QMAAO0HAQAD/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/AAAAAgAAAAn8AAAC/wAAEPv/AMYVAgClIQMA6AgAAP0BAAD9AQAAI/QAAGfb/AAn8v8A5gkBAAL/AAD+AQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB////AAAAAAAQdrUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPwCBgD+AQMAAQD/AAAAAAAAAAEAA/76AAP//AD/AAEAAAAAAAAA/wD/AQIA/AIHAP8AAQAD//wA+wMJAAP++wAc7tEAIOzMABLz4gD/AP8AAAEBAO0NIADdFjsA4RUyAAH/AAAH+/MAAAD/AAAAAQAAAAAAAAAAAAAAAAAAAAAABP76AAL//QD/AAEAAf/+APsECQD4BQ4AFfLcAB3t0AAN+ewAE/LgAP0DBgDkCwoA/gD/AHLY/QBI5v0A9AQAAAAAAAD4AwEA+AMAAPkDAAAD/wAACPwAAAAAAAAG/gAADPz/AOAMAQDkCQEAAgAAAKYgAwDdDQEAFPn/APoCAAD4AgEAD/v/AAAAAAD/AAAANe7/AE/j/QAd9v8A8AUBAAEAAAAAAAAAAAAAAAAAAAAAAAAA/gEAAAn9/wAG/gAA7QcBAPoCAAAC/wAA/wAAAAf+AAAF/QAAAAEAAPsCAAD6AgAAA/8AAAf9AAAAAAAAAAAAAAH/AAD8AgAA+AMAAAABAAAB/wAAEvr/AAP/AADlCQEAC/v/ANMRAgCTJwQA/QEAABX5/wAC/wAA/wEAAOcIAQAM/AAAe9T8ADHv/wDyBQAABv3/APgDAQD8AQAAAQAAAAAAAAAAAAAAAAAAAP8BAAAp47QAAf///wAAAAAAEHa1AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC//8A+AQEAAAAAAAI/PwA/gEBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQD/APgFDQAG/PYABvz2AP4BAwAAAAAAAf/+APYHEQD8AwgACfrvAAL+/gD3Bw8A/wABAAv47QAD//wA+AUNAAH//wAz36sAHO3RAPgGDgD4BQ0A/wECAAH//gAK+e8AAAAAAOEVNQDrDiMA7gsdAAEA/gAB//4AAAAAAAAAAAAB//8A/wEBAPQIFAAAAAAACfrxAPYHEQAD/vsALOO4AB/rywAG/PUA+wMIAPkGDgD+AAIAA///APEGAQAF//8AO+n+APgDAAAZ+AAANuz+AA38AAAV+P8A9wQBAOgIAAAD/gAA9wQAAMMWAgDNEwIA6QgAAP8AAAAJ/QAADfsAAP0BAAAAAAAAAAAAAPsCAAAAAAAA/QEAAOcJAQAf9f8Ab9j8ABn3AAD0BAAAAAAAAAAAAAAAAAAAAAAAAAb+AADoCQEA8QUAADLu/wD/AQAA/AEAABT5/wDrBwEA8QUAAAAAAAAP+wAAEPsAAPcDAADuBgAA/wAAAAEAAAD9AQAADPwAABP6AAAAAAAAA///ANkNAQC6GQIA1hACAAX+AADsBwAABP4AAAv8AAD+AQAAAAAAAAAAAAAD/wAA7AcBAA/7/wAr8f8A8gQAACfy/wA76/4AAQAAAPwBAAAAAAAAAAAAAAAAAAAE/wAANd+0AAH///8AAAAAABB2tQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAL//wD8AQEA8w4RAAb7+gAB//8A+QcIAAvy7wAE//8A/gEBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAv//APMIFgAX8tkAMt+uAAH//gD9AgUAAAD/AAP++wDzCRcA3hY4AOgQKAD8AgYAFfPeACHqyQAU8t4AAQAAAPcGDgAF/fgADffqAPcGDwAAAAEAAAD/AAAAAAAAAAAA/wECAAL//AAL9+0A/QMGAPQJFQD//wAAAQD/AAAAAAAAAAAAAf/+AAL//QDsDiIA3hU4AO4MHQAK+u8AL+GzACDrywDzCRYA+wMIAAH//gAAAAAAAAAAAAAAAAAD/wAA+AMAAO4HAQD+AAAABP8AABX4/wAR+v8AROj+AOUJAQDIFQIAB/4AAPYDAADnCQEADvv/AAz8AAD/AAAAAf8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/wAA7wYAAAf+AAAT+QAA+wIAAAAAAAAAAAAAAAAAAAAAAAAF/gAA7QcAAOcIAABJ5/8AdNb8APYDAAC9GQMAvBgCAPUEAAABAAAAE/n/ABX4AAD0BQAA6AgBAAAAAAABAAAA+wIAAAf9AABJ5v4AJ/L+ALkaAwDCFgEA8QUBABD6/wABAAAAB/0AAAX+AAD+AQAAAAAAAAAAAAAAAAAAAAAAAAP/AAD1BAAA8QUBAAEAAAABAAAAGff/AAb+AAD9AQAAAAAAAAAAAAAAAAAABP8AAKi1sAAB////AAAAAAAQdrUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH//wD8BAUA9A0QACnk3gB4yr0ACfz8AIo1QADMHyYAAAECAAD+/QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAACDAAZ6dUAONyhAAb89gD5BAwAAQD/AAAAAAAAAAEABfz2AAABAADfFTgA+QUMACPpxQAH+/QA+gQLAP4CAwAD/foA/wECAPkECwABAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wABAAL//gAE/vkAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAP4ABvv2APgGDgDpDycAE/TgAAv57gD2BhAAAAABAAAA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAP8AAAD4AwAA+AMAAPoCAQAD/wAACP3/AAEAAAAD/wAACP0AAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAL/AAD4AwAA+gMAAAL/AAAAAAAAAAAAAAAAAAAAAAAA/gEAAAf9AAAD/wAA6wcBAELp/gD3AwAAxBUCAAj9AAAK/f8AAAAAAPkCAAD4AwEABf8AAAj9/wAAAAAAAAAAAAIAAAD3AgAAAQABAAj9/wDyBQEAD/v/AAn8AAD+AQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAA/wAAAPcEAAD+AAAAAQAAAAAAAAAAAAAAAAAAAP8BAAC+rbAAAf///wAAAAAAEHa1AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD5AwQAAAAAAHHNwgBvzsIACfz7AP8AAQDzBgcArCYuAOwJCwAJ/PsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA77AADOFAwA3wPTABb17gD2BxEAAf/+AAAAAAAAAAAAAAAAAP8AAQAC//0ACfryAAH//AD4Bg8A/AIGAAEA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAQAE/voAB/z0APoDCgD7AwgAAQD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/AAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAADrCAEAA/8AABL5/wAAAAAA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAP0BAAD9AQAABf4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAs7KwAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+AUFABzy8ABq0MUA/gECANkRFgAAAAAACP37AF3WzQAE/v4A/gEBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAR9v8AuxwAAH8xEQD1BxIAAv/9AAAAAAAAAAAAAAAAAAAAAAABAP8A/wECAPYHEAAAAAAAAAAAAAEA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAP8A/QIFAPYGEQAAAAAAAQD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAAAAAAAAFPj/AAAAAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAAAC/wAAAv8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH+/wD8AgIA4A8SABn08gAD//4AAAAAAPsCAwDhDhEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA9gf/AOUNAgAAAQQAAf/9AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAL/AAD5AwEA+wIAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD6AgAAEfr/AAf9AAD9AQAAAv8AAAAAAAAAAAASAAAAawAAAPYAAACXAAAA9gAAAAAAAAAAAAAAAAAAAAAAAABGAAAAVAAAAKQAAADCAAAAQQAAAEMAAADuAAAAAwAAAP8AAAAAAAAADgAAANoAAACkAAAATQAAADUAAADwAAAAAQAAAAAAAAAAAAAAEQAAANEAAACrAAAAAAAAAAAAAAAAAAAAAAAAAEkAAABPAAAApgAAAMIAAAAUAAAAZAAAAAwAAAChAAAA2wAAAAAAAAAAAAAAYAAAACkAAACTAAAA5AAAAAAAAABaAAAAXQAAACUAAADjAAAArgAAAJsAAAD4AAAAAAAAAFMAAABCAAAAnAAAAM8AAAAAAAAAAAAAAFAAAABHAAAAnQAAAMwAAAAyAAAAUQAAAO8AAAACAAAAAQAAAAMAAAD1AAAAsAAAAOMAAAAAAAAAAAAAAAAAAAAAAAAAMgAAAFMAAAD7AAAApAAAANwAAAAAAAAAAAAAAAAAAABjAAAALQAAAKQAAADMAAAAAAAAAAAAAABUAAAAQQAAAJ4AAADNAAAAAAAAAAAAAABIAAAAXAAAACQAAADeAAAAsQAAAKkAAAAAAAAAAAAAACEAAABdAAAA9QAAAAAAAAAAAAAAAAAAAAMAAAAGAAAAtwAAAM0EAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA7wYBADbs/gAuF/4AyxMCABr2/wD+AQAAAAAAFgAAAIIAAAAAAAAAqgAAAPYAAAAAAAAAAAAAAAAAAAAAAAAAWgAAAF8AAADxAAAAwgAAAFgAAABmAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADSAAAApAAAAGMAAABPAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADBAAAAqwAAAAAAAAAAAAAAAAAAAAAAAABfAAAAVwAAAPQAAADCAAAAGwAAAIcAAAAAAAAAOAAAAEQAAAAAAAAAAAAAAH8AAAAgAAAAsgAAAOQAAACiAAAAXQAAAAAAAAAAAAAAIwAAAEAAAABoAAAALwAAAPwAAABoAAAARAAAAOEAAADPAAAAAAAAAAAAAABsAAAAQwAAAN4AAADMAAAAPgAAAHwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIMAAAAxAAAA3wAAAAAAAAAAAAAAAAAAAGoAAABjAAAAAAAAAPwAAACEAAAAAAAAAAAAAAAAAAAAfwAAAB0AAABBAAAALwAAAAAAAAAAAAAAawAAAEAAAADmAAAAzQAAAAAAAACZAAAAZgAAAAAAAAAAAAAANwAAAFkAAABrAAAAPgAAAAAAAAAuAAAAgQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPkAAADNAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP0BAAAF/wAAQ+j+AAP/AAD8AgAAAAAAAAAAAAAAAAAAAAAAAAAAAPsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP0AAAAAAAAAAAAAAAAAAAD7AAAAAAAAAK8AAAA1AAAAVQAAAFUAAABfAAAAbwAAAAAAAAAGAAAAAAAAAJsAAABJAAAAYwAAAGMAAAByAAAAiQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/gAAAAAAAAD/AAAAAAAAAP4AAAAAAAAAAAAAAEMAAABWAAAAAAAAAAAAAAD5AAAAAAAAAOAAAAAdAAAAXQAAAOkAAAAtAAAACQAAAC4AAADgAAAAKgAAAHsAAAAAAAAAAAAAAAAAAAD5AAAAAAAAAAAAAAAAAAAA+QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA0gAAAEMAAABJAAAAVQAAAOAAAAAPAAAAgwAAAAAAAAAAAAAAAAAAABcAAABaAAAAAAAAAAAAAABVAAAAAAAAAAAAAAAAAAAAAAAAAPwAAAAAAAAALgAAAGwAAAAAAAAAAAAAAP8AAAAAAAAA+AAAAAAAAABjAAAAZgAAAAAAAABxAAAACAAAAFEAAAD5AAAAPQAAAJgAAAAAAAAA+gAAAAAAAADqAAAAXQAAAFwAAABjAAAAZgAAAGsAAAC3AAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAA7AcBADXt/gAz7f4AzBMCABf4/wD/AAAAAAAAAAAAAAAAAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADYAAAAzAAAAMwAAAAAAAAAAAAAAAAAAAAAAAAABgAAAAAAAAC3AAAAuAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAhAAAAKoAAAAAAAAAAAAAAAAAAAD6AAAAHwAAAAAAAADXAAAA1AAAAAAAAADTAAAAXgAAAJIAAADrAAAAAAAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOYAAAC+AAAAAAAAAAAAAAAEAAAApwAAADMAAAAAAAAAAAAAAAAAAABAAAAACQAAAMoAAAAOAAAALgAAAE8AAACxAAAAAAAAAAAAAAAEAAAAAAAAAAAAAACHAAAAngAAAPYAAAAAAAAAAAAAAPgAAAAAAAAAagAAAAAAAACKAAAAkAAAAAAAAACwAAAAbAAAAJsAAAA3AAAAAAAAAPgAAAAAAAAA6gAAAKQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPsCAAAE/wAARej+ANIRAgD9AQAAAv8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALwAAAEUAAAAdAAAABgAAAPkAAAClAAAA+gAAAP0AAAAAAAAAYgAAACQAAAAUAAAABAAAAAcAAACJAAAA4wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADSAAAAxwAAAGcAAABrAAAAlQAAAAAAAAAAAAAABwAAAOYAAAAAAAAAQAAAAMEAAADEAAAAvwAAALwAAAAxAAAAAAAAAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAOwAAAAAAAADDAAAAAAAAAAAAAAAAAAAAYQAAAAAAAAB0AAAAMAAAAJIAAABFAAAAsQAAAAAAAAAAAAAACwAAAO4AAAChAAAAcQAAAIEAAAB1AAAA/gAAAAAAAAD3AAAAAAAAACQAAAAAAAAAwAAAAAAAAAAAAAAAAAAAAJ0AAACdAAAACgAAAAAAAAAJAAAAAAAAABwAAACEAAAAAAAAAAUAAAAWAAAAwAAAAKMAAAD+BAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAtAAAAcwAAAPEAAAD+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAXAAAAoQAAAOoAAAB2AAAA6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABgC/wCX7QYBwify/wAk8/8A3gwAAA77AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEoAAABKAAAAAAAAAAAAAAAAAAAAuQAAAOYAAAD/AAAAAAAAAEwAAABMAAAAAAAAAAAAAAAAAAAAogAAAOMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFAAAAIAAAABQAAAAlAAAAKIAAADpAAAAJwAAAB8AAADFAAAAggAAAH4AAAA+AAAAPgAAAHoAAAAAAAAAqgAAAF8AAAD4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARAAAAP8AAAAAAAAAAAAAAAAAAADRAAAAZwAAAAAAAAAAAAAAFwAAAEcAAAAAAAAAwwAAAPAAAAAAAAAAYgAAAHwAAADwAAAAAAAAAAAAAAASAAAAhAAAAD8AAAB0AAAAhgAAAOUAAAAAAAAA+QAAAAAAAAAOAAAAAAAAAOgAAAAAAAAAAAAAAAAAAAD/AAAAAAAAAPUAAAAAAAAAAQAAAAAAAAAQAAAAewAAAAAAAAAAAAAAAAAAABYAAACoAAAA+QQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJAAAAF8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOEAAADXAAAAWAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAyv8BAMIA/wAABP//AP4BAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADIAAAArAAAABYAAAAIAAAA9wAAAPAAAAAAAAAAAgAAAAAAAACbAAAArgAAABoAAAADAAAABQAAAN8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABIAAADoAAAAawAAAKwAAAB9AAAA9QAAAAAAAAAFAAAAAAAAAH8AAACJAAAAYwAAAC0AAAA2AAAALwAAAFYAAAAHAAAAUAAAAPkAAAAAAAAA/gAAAAAAAAAAAAAAAAAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPUAAADbAAAA/gAAABwAAAAWAAAACwAAAAkAAAD2AAAAAAAAAFgAAAAAAAAAxQAAAAAAAAADAAAA+QAAAAkAAAA/AAAAsQAAAAAAAAAAAAAAAAAAAO4AAABsAAAAxgAAAIMAAAAAAAAAAAAAAAMAAAAAAAAA/wAAAAEAAAAHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAA/wAAAAAAAAAAAAAA6gAAAHMAAAD/AAAABwAAABAAAADSAAAA+gAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD5AAAA+QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP4B/wAA/QEAAP0BAAAB/wAAAAAAAAAAAAAAAAAAAAAA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAvwAAAI0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAuQAAALgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAK0AAADQAAAAdQAAAPYAAAAAAAAA6gAAABoAAAB7AAAAngAAAJMAAAAAAAAAAAAAAEIAAAC9AAAATwAAAAAAAADsAAAAAAAAABEAAAAAAAAAAAAAAAAAAAAOAAAAAAAAAP8AAAAAAAAAAAAAAAAAAADFAAAARwAAAAAAAAAAAAAA2AAAACcAAABhAAAAlQAAAAAAAABPAAAAAAAAADsAAAD/AAAAAAAAAAAAAAAAAAAAYAAAALEAAAAAAAAAAAAAAAAAAAAPAAAAxwAAALsAAABrAAAA8wAAAAAAAAAIAAAAAAAAAOAAAAAhAAAAKwAAAMgAAAAAAAAAAAAAAEcAAACiAAAAswAAAGQAAAD6AAAAAAAAAOsAAACkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAAAOAAAAIgAAACQAAAAgAAAAAAAAAP0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/AAAAAAAAAB4AAAAAAAAAIgAAACIAAAAlAAAAHgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAsAAAAAAAAAewAAAAAAAAAKAAAAAAAAAOgAAAAuAAAAagAAAKgAAAAFAAAAAAAAAAAAAADuAAAAAAAAAPgAAAAAAAAA0AAAAAAAAABWAAAAAAAAAAAAAAAhAAAANwAAAAAAAADlAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAANQAAAAAAAAABwAAAAAAAAApAAAAQQAAAAAAAADBAAAAyAAAAMAAAADOAAAAAAAAAEIAAAABAAAAAAAAAAAAAAAAAAAACAAAAAAAAAB8AAAAAAAAAA0AAAAAAAAACwAAAAAAAAC1AAAAAAAAAIYAAAAmAAAAAAAAABIAAACWAAAAFgAAAAMAAAAAAAAABAAAAAAAAAAKAAAAGQAAABkAAAAiAAAAIgAAACcAAAAUAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYAAAAGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADJAAAAAAAAAAAAAAAAAAAA/wAAAAIAAADtAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAACOAAAAjgAAAAAAAAAAAAAAAAAAAOMAAAAeAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAQAAAAAAAAABAAAAAAAAAAQAAAAEAAAAAAAAALYAAACuAAAAUgAAAAAAAAAZAAAAuAAAAOkAAAAkAAAA8wAAAMcAAAAUAAAALAAAAAAAAACDAAAAAAAAAMAAAAAAAAAAKwAAAPUAAADpAAAAHgAAAAMAAADhAAAAsAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAD0AAAAAAAAABgAAAAAAAAAXQAAAAAAAAC/AAAAQgAAAAAAAAAAAAAACQAAAOEAAAAVAAAAagAAAJUAAAADAAAAAAAAAAAAAAAAAAAAygAAAJUAAABrAAAAAAAAAAkAAAAAAAAAeAAAAEoAAAAiAAAA2QAAAPcAAAAHAAAAAgAAAOUAAABwAAAAAAAAAAUAAAAAAAAAIQAAAOYAAAAAAAAAAAAAAAAAAAAAAAAAmwAAAGYEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOwAAADbAAAA6wAAAP0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB8AAADbAAAA5QAAAOMAAADgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIQAAAOQAAAAAAAAAAAAAAAAAAAAAAAAA9wAAAMoAAAABAAAABgAAAAIAAAD/AAAACwAAAPQAAAAAAAAA3QAAAAAAAADfAAAAAAAAANkAAAAAAAAA4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANEAAAAjAAAA7gAAAAcAAAD+AAAAAAAAABUAAADVAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADWAAAAAAAAAN4AAAAAAAAA9QAAANwAAADhAAAA+gAAAAAAAAAAAAAAewAAACQAAAAKAAAABQAAAAAAAAA2AAAAjgAAAFEAAABAAAAAAAAAAOsAAAB9AAAAmQAAAAAAAADPAAAAPgAAAJEAAAAxAAAAIQAAAP0AAAC2AAAAbgAAAPcAAAAAAAAA5QAAAAAAAADSAAAAAAAAAAAAAAAAAAAAwwAAAFwAAAANAAAAAAAAAAwAAAAAAAAApQAAAAAAAAAAAAAAAAAAAPcAAACMAAAAiQAAAB0AAAAAAAAAwgAAACIAAAD7AAAAAAAAAAAAAACEAAAALQAAAC4AAADeAAAAAAAAAPUAAAA3AAAAjAAAAFQAAAALAAAA+wAAAKUAAABzAAAA8QAAAAAAAADrAAAA2QAAAO8AAAALAAAAAAAAAP8AAAABAAAAHgAAAOEAAAAAAQAA//9PvpfyZNwyrwAAAABJRU5ErkJggg==" alt="LOGO" />
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <h2 class="kfdblueHeader" style="font-size: 12px;">PRODUCT NAME: </h2>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 <tr>
                                    <td width="100%" colspan="2"  style="padding:1px; display: flex;" >
                                       <div class="checkbox">  </div>
                                       Tata AIA Life Insurance Group Loan Protect (UIN: 110N132V02)  
                                    </td>
                                 </tr>
                                 <tr>
                                    <td colspan="2" style="padding:1px; display: flex; ">
                                       <div class="checkbox active">  </div>
                                       Tata AIA Life Insurance Group Sampoorna Raksha (UIN:110N154V01) 
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 <tr>
                                    <td width="20%">  Policy No:</td>
                                    <td>   <span style="float: left; width: 150px; margin:0px;  height:15px; padding: 2px; border: 1px solid #D4D5D7;"> ||policy_number||</span>  </td>
                                 </tr> 
                                 <tr>
                                    <td> Policyholder’s Name: </td>
                                     <td>  <span  style="float: left; width:515px; margin: 0px;  height:15px; padding: 2px; border: 1px solid #D4D5D7;">||policy_holders_name||</span>  </td>

                                    
                                 </tr>
                              </tbody>
                           </table>
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <h2 class="kfdblueHeader" style="font-size: 12px;">INSURER DETAILS  </h2>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 <tr>
                                    <td width="60%"> <span style="float: left; margin-top:2px; margin-right:4px;">  Tata AIA Life Sourcing Branch  </span> 
                                       <span style="float: left; width:250px;  margin: 0px;  height:15px; padding: 2px; border: 1px solid #D4D5D7;"> </span> </td>
                                    <td > 
                                       <span style="float: left; margin-top:2px; margin-right:4px;"> Branch Code </span> 
                                       <span style="float: left; width:185px;  margin: 0px;  height:15px; padding: 2px; border: 1px solid #D4D5D7;"> </span></td>
                                 </tr>
                                 <tr>
                                    <td > <span style="float: left; margin-top:2px; margin-right:4px;"> RM Name </span>  
                                       <span  style=" float: left; width:338px; margin:0px; height:15px; padding: 2px; border: 1px solid #D4D5D7;">||rm_name||</span> </td>
                                    <td > <span style="float: left; margin-top:2px; margin-right:4px;">RM Code </span>
                                        <span style="float: left; width:200px; margin:0px; height:15px; padding: 2px; border: 1px solid #D4D5D7;">||rm_code||</span> </td>
                                 </tr>
                                 
                              </tbody>
                           </table>

                            <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 
                                 <tr>
                                    <td><span style="float: left; margin-top:2px; margin-right:4px;"> Region</span>  
                                       <span style="float: left; width:110px;  margin:0px; margin-right:5px; height:15px; padding: 2px; border: 1px solid #D4D5D7;"> </span>     </td> 
                                    <td><span style="float: left; margin-top:2px; margin-right:4px;">  Sub office Code </span> 
                                        <span style="float: left; width:150px;  margin:0px; margin-right:5px; height:15px; padding: 2px; border: 1px solid #D4D5D7;"> </span>   </td> 
                                    <td > <span style="float: left; margin-top:2px; margin-right:4px;"> Zone </span>  
                                       <span style="float: left; width:202px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;"> </span>  
                                    </td>
                                 </tr>
                              </tbody>
                           </table> 

                        </td>
                     </tr>
                     <tr>
                        <td>
                           <h2 class="kfdblueHeader" style="font-size: 12px;">DETAILS OF THE PROPOSED MEMBER/LIFE ASSURED <span style="font-size:10px;"> (To Be Filled In Block Letters) </span>   </h2>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" style="margin-top:5px;" >
                              <tbody>
                                 <tr>
                                    <td>
                                       <div class="boxflex"> <span class="checkbox active">  </span> Single Application  </div>
                                    </td>
                                    <td>
                                       <div class="boxflex"> <span class="checkbox">  </span> Joint Application </div>
                                    </td>
                                    <td>
                                       <div class="boxflex"> <span class="checkbox">  </span> Primary Applicant </div>
                                    </td>
                                    <td>
                                       <div class="boxflex">
                                          <div class="checkbox"></div>  Secondary Applicant 
                                       </div>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0">
                              <tbody>
                                 <tr>
                                    <td width="15%"> Mr./Mrs.: Name</td>
                                    <td width="85%"><span style="float: left; width:545px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;" > ||employee_name_var||</span></td>
                                       </tr>
							    	</tbody> 
                           </table>

                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>                                 
                                 <tr>
                                    <td> <span style="float:left; margin-top:2px; margin-right:4px;"> Father/Husband Name</span> <span style="float: left; width:450px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;" > </span>  </td>
                                 </tr>
                                 <tr>
                                    <td> <span style="float:left; margin-top:2px; margin-right:4px;"> Address </span> 
                                       <span style="float: left; width:608px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;" >||address1_var||</span> </td>
                                 </tr>
                                  <tr>
                                    <td> <span style="float:left; margin-top:2px; margin-right:40px;">  </span> <span style="float: left; width:608px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;" >||address2_var|| ||address3_var||</span> </td>
                                 </tr>
                                 <tr>
                                    <td><span style="float:left; margin-top:2px; margin-right:4px;"> Landmark </span>   <span style="float: left; width:600px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;" > </span></td>
                                 </tr>
                              </tbody>
                           </table>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 <tr>
                                    <td width="25%" ><span style="float:left; margin-top:2px; margin-right:4px;"> City  </span>  
                                       <span style="float: left; width:120px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;" >||city_var|| </span> </td>
                                    <td width="25%"> <span style="float:left; margin-top:2px; margin-right:4px;"> State </span> 
                                       <span style="float: left; width:120px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;" >||state_var||</span> </td>
                                    <td width="25%"> <span style="float:left; margin-top:2px; margin-right:4px;"> Country </span> 
                                       <span style="float: left; width:110px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;" >||country_var||</span> </td>
                                    <td width="25%"><span style="float:left; margin-top:2px; margin-right:4px;"> Pin Code </span>
                                       <span style="float: left; width:90px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;" >||pincode_var||</span> </td>
                                 </tr>
                              </tbody>
                           </table>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 <tr>
                                    <td width="35%"> <span style="float:left; margin-top:2px; margin-right:4px;"> Tel/Mob </span>  
                                       <span style="float: left; width:170px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;" >||mobilenumber_var||</span> </td>
                                    <td width="75%"><span style="float:left; margin-top:2px; margin-right:4px;"> Email </span>
                                       <span style="float: left; width:380px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;" >||officialemailid_var||</span> </td>
                                 </tr>
                              </tbody>
                           </table>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 <tr>
                                    <td width="35%" >
                                       <strong style="float:left; margin-right: 10px;"> Gender </strong> 
                                       <div class="boxflex"> <span class="checkbox ||genderchecked1||">  </span> Male  </div>
                                       <div class="boxflex"> <span class="checkbox ||genderchecked2||">  </span> Female </div>
                                       <div class="boxflex">  <span class="checkbox ||genderchecked3||">  </span> Transgender </div>
                                    </td>
                                    <td width="10%" style="padding:2px; text-align: right;">Date of Birth </td>
                                    <td width="50%">
                                       <span style="float: left; width:150px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;" >||dob_var||</span>
								       	</td>
                                 </tr>
                                
                              </tbody>
                           </table>

                             <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 
                                 <tr>
                                    <td width="70%" >
                                       <strong style="float:left; margin-right: 10px;"> Marital status  </strong>    
                                       <div class="boxflex"> <span class="checkbox">  </span> Single </div>
                                       <div class="boxflex">  <span class="checkbox">  </span> Married</div>
                                       <div class="boxflex">  <span class="checkbox">  </span> Widow   </div>
                                       <div class="boxflex">  <span class="checkbox">  </span> Divorced </div>
                                        <div class="boxflex">  <span class="checkbox">  </span> Separated </div>
                                    </td>
                                    <td width="30%" > <span style="float:left; margin-top:2px; margin-right:4px; font-weight:600;"> Annual Income </span> 
                                       <span style="float: left; width:90px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;" >||annualincome_var||</span></td>
                                 </tr>
                              </tbody>
                           </table>

                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 <tr>
                                    <td colspan="2">
                                       <strong style="float:left; margin-right: 10px;"> Occupation  </strong>  
                                       <div class="boxflex"> <span class="checkbox">  </span> Business   </div>
                                       <div class="boxflex"> <span class="checkbox ">  </span> Service</div>
                                       <div class="boxflex">   <span class="checkbox">  </span> Professional  </div>
                                       <div class="boxflex">  <span class="checkbox">  </span> Retired  </div>
                                       <div class="boxflex">  <span class="checkbox">  </span> Farmer </div>
                                       <div class="boxflex"> <span class="checkbox">  </span> Student </div>
                                       <div class="boxflex"> <span class="checkbox">  </span> Housewife  </div>
                                       <div class="boxflex"> <span class="checkbox">  </span> Salaried </div>
                                       <div class="boxflex"> <span class="checkbox">  </span> Unemployed </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td width="8%"></td>
                                    <td>
                                       <div class="boxflex">  <span class="checkbox">  </span> Labourer  </div>
                                       <div class="boxflex">   <span class="checkbox">  </span> Others   </div>
                                       <span style="float: left; width:480px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;color:#999;" > (Please specify)</span>

                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 <tr>
                                    <td colspan="2"> <span style="float:left; margin-right: 10px;"> <strong>Name of the Employer:</strong>    </span>  
                                       <span style="float: left; width:532px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;" >Wipro Limited</span>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td colspan="2">
                                       <strong style="float:left; margin-right: 10px;"> Organisation Type:  </strong>   
                                       <div class="boxflex">  <span class="checkbox">  </span> Government  </div>
                                       <div class="boxflex">  <span class="checkbox">  </span> Public Limited</div> 
                                       <div class="boxflex"> <span class="checkbox ">  </span> Private Limited  </div>
                                       <div class="boxflex">  <span class="checkbox">  </span> Partnership Firm </div>
                                       <div class="boxflex">  <span class="checkbox">  </span> Professional </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td width="14%"></td>
                                    <td width="80%" >
                                       <div class="boxflex">  <span class="checkbox">  </span> Others   </div>
                                        <span style="float: left; width:494px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7; color:#999;" > (Please specify)</span>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 <tr>
                                    <td colspan="2"> 
                                       <strong style="float:left; margin-right: 10px;"> Nature of Job/Daily Duties:   </strong> 
                                       <div class="boxflex">   <span class="checkbox">  </span> Professional/Pvt Sector  </div>
                                       <div class="boxflex">    <span class="checkbox ">  </span>Desk Job </div>
                                       <div class="boxflex">   <span class="checkbox">  </span> Manual Labour </div>
                                       <div class="boxflex">   <span class="checkbox">  </span> Skilled Manual Work</div>
                                       <div class="boxflex">    <span class="checkbox">  </span> Supervisory     </div> 
                                       
                                    </td> 
                                 </tr>
                                 <tr>
                                    <td width="19.5%"></td>
                                    <td width="80%" >
                                        <div class="boxflex"> <span class="checkbox">  </span> Heavy Manual Labour </div>                                      
                                       <div class="boxflex">  <span class="checkbox">  </span> Others   </div>
                                        <span style="float: left; width:344px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;color:#999;" > (Please specify) </span>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 <tr>
                                    <td colspan="2">
                                       <strong style="float:left; margin-right: 10px;"> Identity Proof:</strong>   
                                       <div class="boxflex">   <span class="checkbox">  </span>Passport </div>
                                       <div class="boxflex">   <span class="checkbox active">  </span> PAN Card </div>
                                       <div class="boxflex">   <span class="checkbox">  </span> Voter ID Card </div>
                                       <div class="boxflex">   <span class="checkbox">  </span> Driving License  </div>
                                       <div class="boxflex">  <span class="checkbox">  </span> Others </div>
                                       <span style="float: left; width:140px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;color:#999;" > (Please specify)</span>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td width="10%" >   </td>
                                    <td width="80%" > <span style="float:left; margin-right: 10px;"> Specify with ID proof no </span>  <span style="float: left; width:300px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;" >||pannumber_var||</span></td>
                                 </tr>
                              </tbody>
                           </table>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 <tr>
                                    <td  >
                                       <strong style="float:left; margin-right: 10px;"> Address Proof:</strong>  
                                       <div class="boxflex">   <span class="checkbox">  </span> Voter ID Card </div>
                                       <div class="boxflex">  <span class="checkbox">  </span> Electricity  </div>
                                       <div class="boxflex">  <span class="checkbox">  </span> Telephone Bill  </div>
                                       <div class="boxflex">  <span class="checkbox">  </span>Ration Card  </div>
                                       <div class="boxflex">   <span class="checkbox">  </span> Others </div>
                                       <span style="float: left; width:130px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7; color:#999;" > (Please specify) </span>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td >
                                       <strong style="float:left; margin-right: 10px;"> Age Proof: </strong>   
                                       <div class="boxflex">  <span class="checkbox">  </span> Municipal Birth Certificate  </div>
                                       <div class="boxflex">  <span class="checkbox">  </span> Passport  </div>
                                       <div class="boxflex">  <span class="checkbox active">  </span> PAN Card   </div>
                                       <div class="boxflex">  <span class="checkbox">  </span> School Cert./Education Certificate </div>
                                       <div class="boxflex">  <span class="checkbox">  </span>Driving License  </div>
                                       <div class="boxflex" style=" margin-top:8px;">  <span class="checkbox" style="margin-left:56px;">  </span> Others</div>
                                        <span style="float: left; width:460px; margin:0px; margin-top:8px; height:15px; padding:2px; border:1px solid #D4D5D7; color:#999;" > (Please specify) </span>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 <tr>
                                    <td colspan="2">
                                       <strong style="float:left; margin-right: 10px;"> Nationality:</strong>   
                                       <div class="boxflex">  <span class="checkbox active">  </span>Resident Indian  </div>
                                       <div class="boxflex">  <span class="checkbox">  </span> NRI  </div>
                                       <div class="boxflex">  <span class="checkbox">  </span> OCI   </div>
                                       <div class="boxflex">  <span class="checkbox">  </span> PIO </div>
                                       <div class="boxflex">  <span class="checkbox">  </span>Foreign National (Nationality)  
                                       </div>
                                      <span style="float: left; width:218px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;" > </span>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td width="8%"> </td>
                                    <td> <span style="float:left; margin-right: 10px;"> #Country of Residence </span> <span style="float: left; width:105px; margin:0px; margin-right:5px; height:15px; padding:2px; border:1px solid #D4D5D7;" > </span>
                                       <span style="font-size:7px;">  (#If other than resident Indian, kindly mention current country of residence, Passport as an Age Proof is mandatory.) </span>
                                    </td>
                                 </tr>

                                  <tr>
                                    <td colspan="2" height="55px"> </td>
                                    
                                 </tr> 
                              </tbody>
                           </table> 
                        </td>
                     </tr> 
                      
                   
                  </tbody>
               </table>
            </td>
        </tr>   
        </tbody>
    </table> 


    

 <table class="tataTable" cellpadding="0" cellspacing="0" border="0" style="padding-top: 0px; margin-top: 20px;">
      <tbody>
         <tr>
            <td>
               <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tataTable-divider" style="padding-top: 0">
                  <tbody>
                      <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 <tr>
                                    <td width="100%" >
                                       <strong style="float:left; margin-right: 10px;"> Tax Residence Declaration#  </strong>  
                                       <span style="float: left; margin-right: 10px;">   <span class="checkbox active">  </span> I am a Tax resident of India and not of any other country OR  </span>  <span style=" float: left; margin-right: 10px;">  <span class="checkbox">  </span> I am a Tax resident of country/ies other than India mentioned separately in FATCA/ CRS Annexure   (If you are a Tax Resident of another country then please fill in the FATCA/ CRS Annexure)  </span>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 <tr>
                                    <td width="50%" > <span style="float:left; margin-top:2px; margin-right:4px;"> Reference Number: </span>
                                       <span style="float: left; width:230px; margin:0px; margin-right:5px; height:15px; padding:2px; border:1px solid #D4D5D7;" > </span> </td>
                                    <td width="50%" > <span style="float:left; margin-top:2px; margin-right:4px;"> Member ID: </span>
                                      <span style="float: left; width:248px; margin:0px; margin-right:5px; height:15px; padding:2px; border:1px solid #D4D5D7;" > </span>  </td>
                                 </tr>
                                 <tr>
                                    <td colspan="2" ><span style="float:left; margin-top:2px; margin-right:4px;"> Joint Applicant Name: </span> 
                                       <span style="float: left; width:536px; margin:0px; margin-right:5px; height:15px; padding:2px; border:1px solid #D4D5D7;" > </span> </td>
                                 </tr>
                              </tbody>
                           </table>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 <tr>
                                    <td width="70%"><span style="float:left; margin-top:2px; margin-right:4px;"> Relation with Joint  Applicant: 
                                    </span> <span style="float: left; width:300px; margin:0px; margin-right:5px; height:15px; padding:2px; border:1px solid #D4D5D7;" > </span> </td>
                                    <td width="30%"><span style="float:left; margin-top:2px; margin-right:4px;"> Percentage share </span> 
                                       <span style="float: left; width:80px; margin:0px; margin-right:5px; height:15px; padding:2px; border:1px solid #D4D5D7;" > </span>  % </td>
                                 </tr>
                              </tbody>
                           </table>

                           <h2 class="kfdblueHeader" style="font-size: 12px; margin-top:10px;">PLAN DETAILS </h2>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 <tr>
                                    <td width="50%"><span style="float:left; margin-top:2px; margin-right:4px;">  Loan Account/Reference No: </span>
                                        <span style="float: left; width:180px; margin:0px; margin-right:5px; height:15px; padding:2px; border:1px solid #D4D5D7;" > </span></td>

                                    <td width="50%"> <span style="float:left; margin-top:2px; margin-right:4px;">Loan Effective Date: </span>
                                        <span style="float: left; width:206px; margin:0px; margin-right:5px; height:15px; padding:2px; border:1px solid #D4D5D7;" > </span> </td>
                                 </tr>
                                   <tr>
                                    <td width="50%"><span style="float:left; margin-top:2px; margin-right:4px;">Proposed Sum Assured </span>
                                        <span style="float: left; width:190px; margin:0px; margin-right:5px; height:15px; padding:2px; border:1px solid #D4D5D7;" > ||amount_var1||</span></td>

                                    <td width="50%"> <span style="float:left; margin-top:2px; margin-right:4px;">Loan Amount  </span>
                                        <span style="float: left; width:235px; margin:0px; margin-right:5px; height:15px; padding:2px; border:1px solid #D4D5D7;" > </span> </td>
                                 </tr> 
                              </tbody>
                           </table> 
                            
                        </td>
                     </tr> 

                       <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 <tr>
                                    <td> <span style="float:left; margin-top:2px; margin-right:4px;">Loan Term </span>
                                        <span style="float: left; width:70px; margin:0px; margin-right:5px; height:15px; padding:2px; border:1px solid #D4D5D7;" > </span> <span style="float:left; margin-top:2px; margin-right:4px;"> years </span>     <span style="float: left; width:70px; margin:0px; margin-right:5px; height:15px; padding:2px; border:1px solid #D4D5D7;" > </span>   months  </td>
                                       <td> <span style="float:left; margin-top:2px; margin-right:4px;"> Policy Term </span>
                                        <span style="float: left; width:70px; margin:0px; margin-right:5px; height:15px; padding:2px; border:1px solid #D4D5D7;" > </span> <span style="float:left; margin-top:2px; margin-right:4px;"> years </span>     <span style="float: left; width:70px; margin:0px; margin-right:5px; height:15px; padding:2px; border:1px solid #D4D5D7;" > </span>   months  </td> 
                                   </tr>
                              </tbody>
                           </table>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 <tr>
                                    <td> <span style="float:left; margin-top:2px; margin-right:4px;"> Plan Benet (Including Optional Benefits): </span> 
                                        <span style="float: left; width:450px; margin:0px; margin-right:5px; height:15px; padding:2px; border:1px solid #D4D5D7;" > </span>  </td>
                                 </tr> 
                              </tbody>
                           </table>

                             <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody> 
                                 <tr>
                                    <td width="50%">
                                       <span style="float:left; margin-right: 10px;"> <strong> Coverage Type </strong>    </span>
                                       <div class="boxflex">  <span class="checkbox">  </span> Level </div>
                                       <div class="boxflex">   <span class="checkbox">  </span> Reducing  </div>
                                       <div class="boxflex"> <span class="checkbox">  </span> Moratorium and Reducing   </div> 
                                    </td>
                                      <td  width="50%"> 
                                       <span style="float:left; margin-right: 10px;"> <strong>  Moratorium Period: </strong> </span>
                                       <div class="boxflex">  <span class="checkbox">  </span> Yes  </div>
                                        <div class="boxflex">  <span class="checkbox">  </span> No  </div>
                                       <div class="boxflex">   If yes, number of years  <span class="checkbox">  </span> </div> 
                                    </td>
                                    
                                 </tr>
                                  <tr> 
                                      <td colspan="2">  
                                         <div class="boxflex">  <span class="checkbox">  </span> Interest payable  </div>
                                         <div class="boxflex">  <span class="checkbox">  </span> Interest accumulated  </div> 
                                         <div class="boxflex">  <span class="checkbox">  </span> Interest accumulated   If interest is accumulated, then kindly mention the amount</div> 
                                        <span class="border" style="width:120px; border:none;  border-bottom:1px solid #a9a9a9 !important;"> </span>
                                    </td> 
                                 </tr> 
                              </tbody>
                           </table> 

                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                   <tr>
                                    <td colspan="2">
                                       <span style="float:left; margin-right: 10px;"> <strong> Type of Loan </strong>   </span>
                                       <div class="boxflex">   <span class="checkbox">  </span> Home Loan </div>
                                       <div class="boxflex">  <span class="checkbox">  </span> Personal Loan </div>
                                       <div class="boxflex">  <span class="checkbox">  </span> Education Loan   </div>
                                       <div class="boxflex">   <span class="checkbox">  </span> Personal Vehicle Loan  </div>
                                       <div class="boxflex">   <span class="checkbox">  </span> Commercial Vehicle Loan</div>
                                       <div class="boxflex">   <span class="checkbox">  </span> Tractor Loan  </div>
                                       </td> 
                                 </tr>
                                 <tr> 
                                     <td width="10%"></td>
                                    <td> 
                                        <div class="boxflex">   <span class="checkbox">  </span> Loan Against Property </div>
                                       <div class="boxflex">   <span class="checkbox">  </span> Business Loan </div>
                                       <div class="boxflex">  <span class="checkbox">  </span> Others  </div>
                                       <span style="float: left; width:300px; margin:0px; margin-right:5px; height:15px; padding:2px; border:1px solid #D4D5D7; color:#999;" >(Please specify) </span>
                                    </td>

                                 </tr>


                              </tbody>
                           </table> 




                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                               
                                 <tr>
                                    <td><span style="float:left; margin-right: 10px;"> Loan Interest Rate: </span>    <span style="float: left; width:80px; margin:0px; margin-right:5px; height:15px; padding:2px; border:1px solid #D4D5D7;" > </span>  %</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <span style="float:left; margin-right: 10px;"> <strong> Life Insured Option:</strong>  </span>
                                       <div class="boxflex">  <span class="checkbox active">  </span>Single</div>
                                       <div class="boxflex"><span class="checkbox">  </span> Co-Borrower </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td >
                                       <span style="float:left; margin-right: 10px;"><strong>  Premium Payment Option: </strong> </span>  
                                       <div class="boxflex">   <span class="checkbox">  </span>Single </div>
                                       <div class="boxflex">  <span class="checkbox">  </span> Limited Pay </div>
                                       <div class="boxflex">  <span class="checkbox">  </span>Regular Pay </div>
                                       <div class="boxflex">  <span class="checkbox active"> </span> Yearly Renewable </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td> <span style="float:left; margin-right: 10px;"> Premium Payment Term: </span> <span style="float: left; width:100px; margin:0px; margin-right:5px; height:15px; padding:2px; border:1px solid #D4D5D7;" > </span>  years   </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <span style="float:left; margin-right: 10px;">Premium Payment Frequency: </span>    
                                       <div class="boxflex">  <span class="checkbox active">  </span>Single </div>
                                       <div class="boxflex">   <span class="checkbox">  </span> Yearly </div>
                                       <div class="boxflex">   <span class="checkbox">  </span>Quarterly </div>
                                       <div class="boxflex">   <span class="checkbox">  </span>Half Yearly</div>
                                       <div class="boxflex">  <span class="checkbox">  </span>Monthly </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td> <span style="float:left; margin-right: 10px;"> *Premium Amount : </span> 
                                        <span style="float: left; width:120px; margin:0px; margin-right:5px; height:15px; padding:2px; border:1px solid #D4D5D7;" > 
										||amount_var2||
										</span> 
                                       <span style="float: left;">  inclusive of taxes  </span>     <span class="checkbox" style="margin-left:40px; margin-top:3px ;">  </span>    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <span style="float:left; margin-right: 10px;">Payment Details:</span>    
                                       <div class="boxflex">  <span class="checkbox">  </span>Cash </div>
                                       <div class="boxflex">  <span class="checkbox">  </span> Cheque </div>
                                       <div class="boxflex">  <span class="checkbox">  </span>Demand Draft </div>
                                       <div class="boxflex">  <span class="checkbox">  </span> Online (NEFT/RTGS) </div>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 <tr>
                                    <td colspan="4" width="100%" > *Premium is including applicable taxes, cesses &amp; levies. All Premiums are subject to applicable taxes, cesses &amp; levies which will entirely be borne by the Policyholder and will always be paid by the Policyholder along with the payment of Premium. If any imposition (tax or otherwise) is levied by any statutory or administrative body under the Policy, Tata AIA Life Insurance Company Limited reserves the right to claim the same from the Policyholder. Alternatively, Tata AIA Life Insurance Company Limited has the right to deduct the amount from the benefits payable by Us under the Policy.   </td>
                                 </tr>
                                 <tr>
                                    <td colspan="4" width="100%" >
                                       <div class="boxflex">  <span class="checkbox">  </span>Auto Standing Instruction registration for payment of renewal premium – Subsequent premium shall be deducted from initial mode of payment </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td colspan="4" width="100%" >
                                       <div class="boxflex">  <span class="checkbox">  </span>I hereby authorize you to send communication regarding this policy or any existing policy(ies) through WhatsApp service on my registered mobile number </div>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <h2 class="kfdblueHeader" style="font-size: 12px;">PROPOSER DETAILS IN CASE INSURED IS JUVENILE</h2>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                             <tbody>
                                 <tr>
                                    <td width="4%">Name   </td>
                                    <td width="96%"><span style="float: left; width:600px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;"> </span></td>
                                   </tr>
							    	</tbody>
                           </table>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 <tr>
                                    <td width="50%"> <span style="float:left; margin-top:2px; margin-right:4px;"> (Relationship with Proposed Insured) </span> 
                                        <span style="float: left; width:130px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;"> </span>   </td>
                                    <td width="50%"> <span style="float:left; margin-top:2px; margin-right:4px;"> Address </span>
                                        <span style="float: left; width:258px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;"> </span> </td>
                                 </tr>
                                 <tr>
                                    <td colspan="4" width="100%" >   <span style="float: left; width:635px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;"> </span></td>
                                 </tr>
                              </tbody>
                           </table>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 
                                 <tr>
                                    <td width="25%"><span style="float:left; margin-top:2px; margin-right:4px;"> City  </span>  
                                       <span style="float: left; width:120px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;"> </span> </td>
                                    <td width="25%"> <span style="float:left; margin-top:2px; margin-right:4px;"> State </span> 
                                       <span style="float: left; width:120px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;"> </span> </td>
                                    <td width="25%"> <span style="float:left; margin-top:2px; margin-right:4px;"> Pin Code </span> 
                                       <span style="float: left; width:110px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;"> </span> </td>
                                    <td width="25%"><span style="float:left; margin-top:2px; margin-right:4px;"> Tel / Mob</span>
                                       <span style="float: left; width:85px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;"> </span> </td>
                                 </tr>
                              </tbody>
                           </table>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 <tr>
                                    <td width="5%" >Email    </td>
                                    <td  colspan="3"><span style="float: left; width:450px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;"> </span></td>
                                 </tr>
                              </tbody>
                           </table>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 <tr>
                                    <td width="25%" >
                                       <span style="float:left; margin-right: 10px;"> <strong>Gender</strong>  </span>  
                                       <div class="boxflex"> <span class="checkbox">  </span>Male </div>
                                       <div class="boxflex"> <span class="checkbox">  </span>Female  </div>
                                       <div class="boxflex"> <span class="checkbox">  </span>Transgender </div>
                                    </td>
                                    <td width="45%" > <span style="float:left; margin-top:2px; margin-right:4px;"> Date of Birth </span>  <span style="float: left; width:150px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;"> </span></td>
								       	</tr>
                              </tbody>
                           </table>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 <tr>
                                    <td width="33%"> <span style="float:left; margin-top:2px; margin-right:4px;"> Nationality </span>
                                        <span style="float: left; width:130px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;"> </span></td> 
                                         <td width="33%"> <span style="float:left; margin-top:2px; margin-right:4px;"> Residential status </span>
                                        <span style="float: left; width:120px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;"> </span></td> 
                                         <td width="34%"> <span style="float:left; margin-top:2px; margin-right:4px;"> Identity Card Type </span>
                                        <span style="float: left; width:105px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;"> </span></td> 
                                 </tr>
                                  <tr>
                                    <td width="33%"> <span style="float:left; margin-top:2px; margin-right:4px;"> Identity Card No. </span>
                                        <span style="float: left; width:120px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;"> </span></td> 
                                         <td width="33%"> <span style="float:left; margin-top:2px; margin-right:4px;">PAN Card No. </span>
                                        <span style="float: left; width:140px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;"> </span></td> 
                                         <td width="34%"> <span style="float:left; margin-top:2px; margin-right:4px;"> Occupation </span>
                                        <span style="float: left; width:135px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;"> </span></td> 
                                 </tr> 
                                 
                                 <tr>
                                    <td colspan="2" width="70%" > <span style="float:left; margin-top:2px; margin-right:4px;"> Name of the Employer. </span>  
                                        <span style="float: left; width:320px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;"> </span></td>
                                    <td width="30%">  <span style="float:left; margin-top:2px; margin-right:4px;"> Annual Income </span> 
                                        <span style="float: left; width:120px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;"> </span></td>
                                 </tr>
                              </tbody>
                           </table>
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <h2 class="kfdblueHeader" style="font-size: 12px;">DECLARATION OF GOOD HEALTH (For Simplified Issuance):</h2>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 <tr>
                                    <td width="100%">I have never had any disorder of the heart or circulatory system, high blood pressure, stroke, asthma, or other lung condition, cancer or tumour of any kind, diabetes, hepatitis or liver condition, urinary or kidney disorder, depression, mental or psychiatric condition, epilepsy, HIV infection or a positive test to HIV, any disease of the brain or the nervous system, blood disorder. I do not currently have, nor received treatment for any medical conditions, disabilities. I do not suffer from any symptoms that have persisted for more than seven days. I have not been absent from work due to illness or injury for a continuous period of more than 10 days during the last 3 years. <br></br>This health Declaration shall form the basis of your enrolment. Any fraud, misstatement of or suppression of a material fact under the policy shall be dealt in accordance with Section 45 of the Insurance Act, 1938 as amended from time to time.  </td>
                                 </tr>
                                  <tr>
                                  <td height="0px">  </td>
                                 </tr>
							   	 </tbody>
                           </table>
                        </td>
                     </tr>

                    
                     
                  </tbody>
               </table>
            </td>
        </tr>   
        </tbody>
    </table>


    
 <table class="tataTable" cellpadding="0" cellspacing="0" border="0" style="padding-top: 0px; margin-top: 0px;">
      <tbody>
         <tr>
            <td>
               <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tataTable-divider" style="margin-top:20px">
                  <tbody> 
                       <tr>
                        <td>
                           <h2 class="kfdblueHeader" style="font-size: 12px; margin-top:10px;">DECLARATION</h2>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 <tr>
                                    <td width="100%" >I further declare that the above statements are true and complete in every respect related to my health and will form the basis of granting insurance cover to me, from Tata AIA Life Insurance Company Ltd. I further hereby agree and give my consent to the Policyholder for use of the contents of this declaration by Tata AIA Life for examining and processing any claim arising, in respect of the insurance cover that may be provided to me under the referred group policy. <br></br>I hereby confirm that my intent to participate in the above plan for the Policyholder''s customers is purely on a voluntary basis. I confirm and agree that the insurance cover, if provided, will be governed by the provisions of the Insurance Act, 1938, the Policy Contract and the Certicate of Insurance under which the cover will be offered to me. <br></br>I agree and understand that if I contract any of the above diseases between submitting this document and the date of commencement of the cover, I shall not be covered under the policy. I have also not withheld any material information or suppressed any fact. I undertake to notify Tata AIA Life (''The Company'') of any change in my state of health or occupation or any decisions subsequent to the signing of this Enrolment Form and before the acceptance of the risk by the Company. I hereby authorize the Company or any of its approved medical examiners or laboratories to perform the necessary medical assessment and test to underwrite and evaluate my/the Proposed Insured''s health status in relation to this application and any claim arising therefrom. <br></br>I/We hereby permit/authorise the Company to collect, store, communicate and process information relating to this proposal or the resulting Policy/Account and all transactions therein including medical records, by the Company including sharing, transfer and disclosure with any entity or entities, and to the authorities in and/or outside India of any confidential information for compliance with any law or regulation. 
                                    </td>
                                 </tr>
								 </tbody>
                           </table>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" style="margin-top:5px;"  >
                              <tbody>
                                 <tr>
                                    <td  style="padding:2px; text-align: right;">Date </td>
                                    <td   style="padding:2px; ">  <span style="float: left; width:165px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;">||otp_datetime||</span></td>
                                    <td  style="padding:2px; ">I declare that these statements are true.  </td>
                                 </tr>
                                 <tr>
                                    <td width="10%" style="padding:2px; text-align: right;">Place </td>
                                    <td width="20%" >  <span style="float: left; width:165px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;"> </span></td>
                                    <td width="40%" >
                                       <div style="max-width:370px; text-align: center; margin: 0px; height:50px; padding:5px; border: 1px solid #D4D5D7;text-align:left">OTP : ||otp_code||<br></br>||health_Option||</div>
                                      
                                       <span  style="color:#999; margin-top:5px;"> Signature/Right Thumb impression of life to be insured </span> 
                                    </td>
                                 </tr>
                              </tbody>
                           </table> 
                        </td>
                     </tr> 

                     <tr>
                        <td>
                             <table class="borderTable text-center" cellpadding="0" cellspacing="0" border="0" style="margin-top:2px;"  >
                              <tbody>
                                 <tr>
                                    <td class="bdr-top ">Details of Nominee  </td>
                                    <td></td>
                                     <td></td>
                                      <td></td>

                                    <td class="bdr-top ">* Total % should be equal to 100 </td>
                                 </tr>
                                 <tr>
                                    <td width="35%">Name   </td>
                                    <td width="15%">Date of Birth (DDMMYYYY) </td>
                                    <td width="15%">Gender  (M/F)    </td>
                                    <td width="15%">Relationship    </td>
                                    <td width="15%"> Percentage* (%)  Do not enter % in decimals</td>
                                 </tr>
                                 <tr>
                                    <td height="25px">||nomineename_var||</td>
                                    <td>||nomineedob_var||</td>
                                    <td>||nomineegender_var||</td>
                                    <td>||nomineerelationship_var||</td>
                                    <td>||nomineepercentage_var||</td>
                                 </tr>
                                 <tr>
                                    <td height="25px">||nomineename2_var||</td>
                                    <td>||nomineedob2_var||</td>
                                    <td>||nomineegender2_var||</td>
                                    <td>||nomineerelationship2_var||</td>
                                    <td>||nomineepercentage2_var||</td>
                                 </tr>
                              </tbody>
                           </table>


                           
                           <table   cellpadding="0" cellspacing="0" border="0">
                              <tbody>
                                 <tr>
                                    <td>
                                       <p>*Nominee needs to be a major i.e. above 18 years of age and should be one of the following: Husband, Wife, Son, Daughter, Father, Mother, Brother, Sister, Grandfather or Grandmother. In case of Nominee being a Proprietor/Partnership Firm/Limited Company the above condition would not apply. </p>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                           <table class="borderTable text-center" cellpadding="0" cellspacing="0" border="0" >
                              <tbody>
                                 <tr>
                                    <td  colspan="4" style="text-align:left ;">Details of Appointee (where Nominee is a minor, Appointee also to be furnished) </td>
                                 </tr>
                                 <tr>
                                    <td width="35%">Name   </td>
                                    <td width="20%">Date of Birth  (DD-MM-YYYY) </td>
                                    <td width="20%">Gender  (M/F)    </td>
                                    <td width="20%">Relationship    </td>
                                 </tr>
                                 <tr>
                                    <td height="25px"> ||appointeename1_var|| </td>
                                    <td> ||appointeedob1_var|| </td>
                                    <td> ||appointeegender1_var|| </td>
                                    <td> ||appointeerealationship1_var|| </td>
                                 </tr>
  <tr>
                                    <td height="25px"> ||appointeename2_var|| </td>
                                    <td> ||appointeedob2_var|| </td>
                                    <td> ||appointeegender2_var|| </td>
                                    <td> ||appointeerealationship2_var|| </td>
                                 </tr>                               
                                 
                              </tbody>
                           </table>
                        </td>
                     </tr>
                       
                    
                     <tr>
                        <td>
                           <h2 class="kfdblueHeader" style="font-size: 12px;">AUTHORIZATION FORM
                              <span style="font-size:10px; font-weight: 500;"> (Applicable if the Master Policyholder is either of the following): </span>
                           </h2>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" >
                              <tbody>
                                 <tr>
                                    <td width="100%" >1) Reserve Bank of India (RBI) Regulated Scheduled Commercial Banks (including Co-operative Banks) 2) NBFCs having Certificate of Registration from RBI 3) National Housing Bank (NHB) Regulated Housing Finance Companies 4) National Minority Development Finance Corporation (NMDFC) and its State Channelizing Agencies 5) Small Finance Banks regulated by RBI 6) Mutually Aided Cooperative Societies formed and registered under the applicable State Act concerning such Societies 7) Microfinance companies registered under section 8 of the Companies Act, 2013 or 8) Any other category as approved by the Authority. </td>
                                 </tr>
                                 <tr>
                                    <td>I/We <input type="text" class="bordernone" style="border-bottom:1px solid #555; width:300px;" name=""/> an Insured Member under the Group Master Policy bearing no. 
                                          <input type="text" name="" class="bordernone" style="border-bottom:1px solid #555; width:150px;"/> issued to the Master Policyholder
                                          <input type="text" name="" class="bordernone" style="border-bottom:1px solid #555; width:150px;"/>, hereby authorize TATA AIA Life Insurance Company Ltd. to make payment of claim proceeds in favour of the Master Policyholder to the extent of my outstanding loan amount under the Policy in the event of my death and the balance if any to <input type="text" name="" class="bordernone" style="border-bottom:1px solid #555; width:150px;"/>, my Nominee under the Coverage. If the Master Policyholder is other than the above mentioned entities, the benefit amount would be directly paid to <input type="text" name="" class="bordernone" style="border-bottom:1px solid #555; width:150px;"/>, my Nominee under the Coverage. I request the Company to settle the claim proceeds under the Policy as per this Authorization.  </td>
                                 </tr>
								 </tbody>
                           </table>
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" style="margin-top:10px;">
                              <tbody>
                                 <tr align="bottom">
                                    <td width="30%" style="padding:2px; text-align: center;">
                                       <div style="max-width:250px; text-align: center; margin: 0px auto; max-height:60px; height:60px; border: 1px solid #D4D5D7;"> </div>
                                       
                                       Signature of Insured Member     
                                    </td>
                                    <td width="30%" style="padding:2px; text-align: center;">
                                       <div style="max-width:250px; text-align: center; margin: 0px auto; max-height:60px; height:60px; border: 1px solid #D4D5D7;"> </div>
                                         Signature of Proposer (In case of Juvenile)  
                                    </td>
                                    <td width="30%" style="padding:2px; text-align: center;">
                                       <div style="max-width:250px; text-align: center; margin: 0px auto; max-height:60px; height:60px; border: 1px solid #D4D5D7;"> </div>
                                        Signature &amp; Stamp of Policyholder Official 
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                            
                           <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" style="margin-top:20px;" >
                              <tbody>
                                 <tr>
                                    <td  width="5%" style="padding:2px; ">Date </td>
                                    <td width="20%" style="padding:2px; "> <span style="float: left; width:165px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;"> </span></td>
                                    <td width="5%" style="padding:2px; ">Date </td>
                                    <td width="20%" style="padding:2px; "><span style="float: left; width:165px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;"> </span></td>
                                    <td colspan="5"></td>
                                 </tr>
                                 <tr>
                                    <td width="5%" >Place </td>
                                    <td width="20%" > <span style="float: left; width:165px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;"> </span></td>
                                    <td width="5%" >Place </td>
                                    <td width="20%" ><span style="float: left; width:165px; margin:0px; height:15px; padding:2px; border:1px solid #D4D5D7;"> </span></td>
                                    <td colspan="5"></td>
                                 </tr>
                                 <tr>
                                    <td colspan="5" height="10px"></td>
                                 </tr>
                              </tbody>
                           </table> 
                           
                        </td>
                     </tr> 
                   
                  </tbody>
               </table>
            </td>
        </tr>   
        </tbody>
    </table>



    
    
 <table class="tataTable" cellpadding="0" cellspacing="0" border="0" style="padding-top: 0px; margin-top: 0px;">
      <tbody>
         <tr>
            <td>
               <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tataTable-divider" style="margin-top:20px">
                  <tbody>  
                     <tr>
                        <td> 
                           <table class="table-bdr" cellpadding="0" cellspacing="0" border="0" style="margin-top:20px;">
                              <tbody>
                                 <tr>
                                    <td style="padding:10px 0; border: 1px solid; border-top: 2px solid; border-bottom: 2px solid; ">
                                       <div style="padding: 5px;">
                                          <table cellpadding="0" cellspacing="0" border="0">
                                             <tbody>
                                                <tr>
                                                   <td>
                                                        <p><strong> IN CASE THE PROPOSED INSURED/PROPOSER AFFIXES A THUMB IMPRESSION OR SIGNING IN VERNACULAR</strong></p>
                                                        <p>The thumb impression or signature of the Proposed Insured/ Proposer should be attested by a person of standing whose identity can easily be established and this declaration should be made by him/her.</p>
                                                        <p>
                                                         I <input type="text" class="bordernone" name="" style="border-bottom:1px solid #555; width:350px;"/> (name) holding <input type="text" class="bordernone" name="" style="border-bottom:1px solid #555; width:150px;"/> (Identity Card type) <input type="text" class="bordernone" name="" style="border-bottom:1px solid #555; width:190px;"/> (Identity Card no.) hereby declare that I have explained the contents of the enrolment form to the Proposed Insured/ Proposer in <input class="bordernone" type="text" name="" style="border-bottom:1px solid #555; width:190px;"/> language and that I have read out to the Proposed Insured/ Proposer the answers to the questions dictated by the Proposed Insured/ Proposer. The information/answers filled in the enrolment form are exact replication of the information/answers provided to me by the Proposed Insured/ Proposer and that the Proposed Insured/ Proposer has affixed his/her signature/thumb impression on the enrolment form after fully understanding the contents thereof.
                                                        </p>
                                                      
                                                    </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                          <table cellspacing="0" cellpadding="0" class="noinputbox" border="0" style="margin-top:20px;">
                                             <tbody>
                                                <tr align="bottom" >
                                                   <td width="30%" style="padding:2px; text-align: center;">
                                                      <div style="max-width:250px; text-align: center; margin: 0px auto; max-height:60px; height:60px; border: 1px solid #D4D5D7;"> </div> 
                                                      Signature/ Thumb Impression of Proposed Insured  
                                                   </td>
                                                   <td width="30%" style="padding:2px; text-align: center;">
                                                      <div style="max-width:250px; text-align: center; margin: 0px auto; max-height:60px; height:60px; border: 1px solid #D4D5D7;"> </div>
                                                       Signature / Thumb Impression of Proposer (In case of Juvenile)  
                                                   </td>
                                                   <td width="30%" style="padding:2px; text-align: center;">
                                                      <div style="max-width:250px; text-align: center; margin: 0px auto; max-height:60px; height:60px; border: 1px solid #D4D5D7;"> </div>
                                                        Signature of Witness   
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </div>
                                    </td>
                                 </tr>
                              </tbody>
                           </table> 
                           <table class="table-bdr" cellpadding="0" cellspacing="0" border="0" style="margin-top:20px;" >
                              <tbody>
                                 <tr>
                                    <td style="padding: 0; border: 1px solid; border-top: 2px solid; border-bottom: 2px solid;">
                                       <div style="padding: 5px;">
                                          <table cellpadding="0" cellspacing="0" border="0">
                                             <tbody>
                                                <tr>
                                                   <td>
                                                      <p><strong> (Prohibition of Rebates) Section 41 - of the Insurance Act, 1938 as amended from time to time:</strong> 
                                                         1. No person shall allow or offer to allow, either directly or indirectly, as an inducement to any person to take out or renew or continue an insurance in respect of any kind of risk relating to lives or property in India, any rebate of the whole or part of the commission payable or any rebate of the premium shown on the policy, nor shall any person taking out or renewing or continuing a policy accept any rebate, except such rebate as may be allowed in accordance with the published prospectuses or tables of the insurer. 2. Any person making default in complying with the provisions of this section shall be liable for a penalty which may extend to ten lakh rupees. 
                                                      </p>
                                                      <p>
                                                         <strong>SECTION 45 OF THE INSURANCE ACT, 1938 STATES: </strong>
                                                         No policy of life insurance shall be called in question on any ground whatsoever after the expiry of three years from the date of policy, i.e. from the date of issuance of the policy or the date of commencement of risk or the date of revival of the policy or the date of the rider to the policy, whichever is later. A policy of life insurance may be called in question at any time within three years from the date of issuance of the policy or the date of commencement of risk or the date of revival of the policy or the date of the rider to the policy, whichever is later, on the ground of fraud. Provided that the insurer shall have to communicate in writing to the insured or the legal representatives or nominees or assignees of the insured the grounds and materials on which such decision is based. Nothing in this section shall prevent the insurer from calling for proof of age at any time if he is entitled to do so, and no policy shall be deemed to be called in question merely because the terms of the policy adjusted on subsequent proof that the age of the life insured was incorrectly stated in the proposal. For further details, please refer to the Insurance Act, as amended from time to time.
                                                      </p>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </div>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                           <table class="table-bdr" cellpadding="0" cellspacing="0" border="0" style="margin-top:100px;">
                              <tbody>
                                 <tr>
                                    <td style="padding: 0;  ">
                                       <table cellpadding="0" cellspacing="0" border="0">
                                          <tbody>
                                             <tr>
                                                <td>
                                                   <p>Tata AIA Life Insurance Company Limited (IRDAI Regn. No.110) CIN: U66010MH2000PLC128403 Registered &amp; Corporate Office: 14th Floor, Tower A, Peninsula Business Park, Senapati Bapat Marg, Lower Parel, Mumbai - 400013. Trade logo displayed above belongs to Tata Sons Ltd and AIA Group Ltd. and is used by Tata AIA Life Insurance Company Ltd under a licence. For any information including cancellation, claims and complaints, please contact our Insurance Adviser / Intermediary or visit TATA AIA Life’s nearest branch office or call 1-860-266-9966 (local charges apply) or write to us at customercare@tataaia.com. Visit us at: www.tataaia.com. • L&amp;C/Misc/2021/Jan/0005.
                                                   </p>
                                                </td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </td>
                     </tr>
                     
                   
                  </tbody>
               </table>
            </td>
        </tr>   
        </tbody>
    </table>
    

   </body>
</html>
')
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Male', N'gender', CAST(5 AS Numeric(18, 0)), N'1', CAST(2 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 11, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Female', N'gender', CAST(5 AS Numeric(18, 0)), N'1', CAST(3 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 12, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Father', N'relation', CAST(6 AS Numeric(18, 0)), N'1', CAST(1 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 13, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Mother', N'relation', CAST(6 AS Numeric(18, 0)), N'1', CAST(1 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 14, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Brother', N'relation', CAST(6 AS Numeric(18, 0)), N'1', CAST(1 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 15, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Spouse', N'relation', CAST(6 AS Numeric(18, 0)), N'1', CAST(1 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 16, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Voluntary Group Portal', N'mailsubject', CAST(7 AS Numeric(18, 0)), N'1', CAST(1 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 17, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'0', N'mailstatus', CAST(8 AS Numeric(18, 0)), N'1', CAST(1 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 18, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'GROUPPRTL', N'mailsysid', CAST(9 AS Numeric(18, 0)), N'1', CAST(1 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 19, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'GROUP_PORTAL', N'classportal', CAST(10 AS Numeric(18, 0)), N'1', CAST(1 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 20, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Transgender', N'gender', CAST(5 AS Numeric(18, 0)), N'2', CAST(3 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 21, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Sister', N'relation', CAST(6 AS Numeric(18, 0)), N'1', CAST(1 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 22, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Child', N'relation', CAST(6 AS Numeric(18, 0)), N'1', CAST(1 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 23, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'HTML', N'mailType', CAST(14 AS Numeric(18, 0)), N'1', CAST(1 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 53, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Mr', N'title', CAST(15 AS Numeric(18, 0)), N'1', CAST(1 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 54, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Mrs', N'title', CAST(15 AS Numeric(18, 0)), N'1', CAST(2 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 55, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Ms', N'title', CAST(15 AS Numeric(18, 0)), N'1', CAST(3 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 56, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Government', N'organization', CAST(16 AS Numeric(18, 0)), N'1', CAST(1 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 57, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Public Limited', N'organization', CAST(16 AS Numeric(18, 0)), N'1', CAST(2 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 58, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Private Limited', N'organization', CAST(16 AS Numeric(18, 0)), N'1', CAST(3 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 59, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Partnership Firm', N'organization', CAST(16 AS Numeric(18, 0)), N'1', CAST(4 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 60, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Professional', N'organization', CAST(16 AS Numeric(18, 0)), N'1', CAST(5 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 61, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Others', N'organization', CAST(16 AS Numeric(18, 0)), N'1', CAST(6 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 62, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Business', N'occupation', CAST(17 AS Numeric(18, 0)), N'1', CAST(1 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 63, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Service', N'occupation', CAST(17 AS Numeric(18, 0)), N'1', CAST(2 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 64, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Professional', N'occupation', CAST(17 AS Numeric(18, 0)), N'1', CAST(3 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 65, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Retired', N'occupation', CAST(17 AS Numeric(18, 0)), N'1', CAST(4 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 66, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Farmer', N'occupation', CAST(17 AS Numeric(18, 0)), N'1', CAST(5 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 67, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Student', N'occupation', CAST(17 AS Numeric(18, 0)), N'1', CAST(6 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 68, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Housewife', N'occupation', CAST(17 AS Numeric(18, 0)), N'1', CAST(7 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 69, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Salaried', N'occupation', CAST(17 AS Numeric(18, 0)), N'1', CAST(8 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 70, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Unemployed', N'occupation', CAST(17 AS Numeric(18, 0)), N'1', CAST(9 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 71, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Labourer', N'occupation', CAST(17 AS Numeric(18, 0)), N'1', CAST(10 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 72, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'Others', N'occupation', CAST(17 AS Numeric(18, 0)), N'1', CAST(11 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 73, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'<h6 class="qTitle"> I declare that I have never been hospitalized due to Covid, and I have not come into close contact with a Covid positive patient or suffered any Covid symptoms in the last 30 days</h6>
									
									<h6 class="qTitle">1. I am in good health and have never suffered or currently suffering from or received treatment or investigated or awaiting medical or surgical treatment for:</h6>
										
										<ol style="list-style-type:lower-alpha;color:#969696">
											<li> 
												<p class="qTitle">High blood pressure, high Cholesterol, Chest Pain or heart attack or any other heart disease </p>
											</li>
											<li> 
												<p class="qTitle">Cancer, tumor, growth, pre-cancerous or cyst of any kind</p>
											</li>
											<li> 
												<p class="qTitle">Stroke, paralysis in any form, Epilepsy, Cerebrovascular Disease, any psychiatric/mental disorder including any genetic disease, disorder of brain/nervous system or any kind of physical disabilities </p>
											</li>
											<li> 
												<p class="qTitle">Asthma, Tuberculosis, Chronic Obstructive Pulmonary Diseases, Parenchymal Lung Disease, Pulmonary Embolism or other lung disorder. </p>
											</li>
											<li> 
												<p class="qTitle">Diseases or disorder of muscles, bones, spine or joints including arthritis or anemia or any blood disorder or any endocrine disorder including diabetes</p>
											</li>
											<!-- <li> 
												<p class="qTitle">including diabetes </p>
											</li> -->
											<li> 
												<p class="qTitle">Diseases of the kidney, genitourinary, any form of hepatitis or liver disease or digestive system (Including stomach, pancreas, gall bladder, intestine, ulcers or colitis) </p>
											</li>
											<li> 
												<p class="qTitle">Any lung or respiratory diseases (e.g. Asthma, Tuberculosis, COPD, etc.) </p>
											</li>
											<li> 
												<p class="qTitle">Me or my spouse suffering from or been advised to undergo tests related to HIV/AIDS, Hepatitis B, Hepatitis C </p>
											</li>
											<li> 
												<p class="qTitle">Any physical defect, deformity or disability.</p>
											</li>
										</ol>
										
										
										<h6 class="qTitle">2. During the last 5 years, I have not undergone or have never been advised.</h6>
										<ol style="list-style-type:lower-alpha;color:#969696">
											<li> 
												<p class="qTitle">any major surgery or hospitalization for more than one week or leave for more than 5 days on medical grounds.</p>
											</li>
											<li> 
												<p class="qTitle">Any investigations other than normal Health Check-ups and Insurance Medicals or had adverse result for any blood tests, X-Rays, ECG, Stress Test, Biopsies, Ultrasonography, CT Scan, MRI or 2D/3D echo.</p>
											</li>
											
										</ol>
										
										<h6 class="qTitle">3. I do not take part or intend to engage in any business, sport, occupation or in any adventurous hobbies of hazardous nature.</h6><br />
										
										
										<h6 class="qTitle">4. I have never been medically advised treatment or advised to restrain from alcohol, smoking/chewing tobacco or habit forming drugs such as heroin, cocaine,cannabis, Ganja or LSD or any drug not prescribed by physician or have lost more than 10 kg in the last six months. </h6><br />									
										
										<h6 class="qTitle">5. I do not have two or more of my first degree relatives in my immediate family members, whether living or deceased, ever been diagnosed with Diabetes, Cancer,Stroke, Heart ailment, any neurological disorder or any other hereditary/familiar disorder before age 60.</h6><br />
										
										<h6 class="qTitle">6. I do not take any medication or have ever attended to a doctor for any conditions except for cough, cold or similar minor conditions.</h6><br />', N'dough_declaration', CAST(18 AS Numeric(18, 0)), N'1', CAST(1 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 81, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'N', N'static_issuance', CAST(99 AS Numeric(18, 0)), N'1', CAST(1 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 83, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'<div class="modal-body">
        	<h6 class="qTitle">(Prohibition of Rebates) Section 41 - of the Insurance Act, 1938 as amended from time to time: 1. No person shall allow or offer to allow, either directly or indirectly, as an inducement to any person to take out or renew or continue an insurance in respect of any kind of risk relating to lives or property in India, any rebate of the whole or part of the commission payable or any rebate of the premium shown on the policy, nor shall any person taking out or renewing or continuing a policy accept any rebate, except such rebate as may be allowed in accordance with the published prospectuses or tables of the insurer. 2. Any person making default in complying with the provisions of this section shall be liable for a penalty which may extend to ten lakh rupees.</h6>
											<h6 class="qTitle">SECTION 45 OF THE INSURANCE ACT, 1938 STATES: No policy of life insurance shall be called in question on any ground whatsoever after the expiry of three years from the date of policy, i.e. from the date of issuance of the policy or the date of commencement of risk or the date of revival of the policy or the date of the rider to the policy, whichever is later. A policy of life insurance may be called in question at any time within three years from the date of issuance of the policy or the date of commencement of risk or the date of revival of the policy or the date of the rider to the policy, whichever is later, on the ground of fraud. Provided that the insurer shall have to communicate in writing to the insured or the legal representatives or nominees or assignees of the insured the grounds and materials on which such decision is based. Nothing in this section shall prevent the insurer from calling for proof of age at any time if he is entitled to do so, and no policy shall be deemed to be called in question merely because the terms of the policy adjusted on subsequent proof that the age of the life insured was incorrectly stated in the proposal. For further details, please refer to the Insurance Act, as amended from time to time.</h6>
											
											
											
										
      </div>', N'dough_declaration_terms', CAST(19 AS Numeric(18, 0)), N'1', CAST(1 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 84, NULL)
INSERT [dbo].[TGP_COMMON_LIST_MASTER] ([VALUE], [CATEGORY], [CATEGORY_ID], [SOFTDELETE], [SORT_ORDER], [DESCRIPTION], [CREATED_DATE], [CREATED_BY], [MODIFIED_BY], [MODIFIED_DATE], [ID], [ADDITIONAL_DATA]) VALUES (N'1000', N'drop_link_scheduler_frequency', CAST(20 AS Numeric(18, 0)), N'1', CAST(1 AS Numeric(18, 0)), NULL, NULL, NULL, NULL, NULL, 85, NULL)
SET IDENTITY_INSERT [dbo].[TGP_COMMON_LIST_MASTER] OFF
SET IDENTITY_INSERT [dbo].[TGP_MERCHANT_MASTER] ON 

INSERT [dbo].[TGP_MERCHANT_MASTER] ([MERCHANT_ID], [NAME], [CONTACT_NO], [EMAIL], [STATUS], [EXPIRY_DATE], [CREATED_BY], [CREATED_DATE], [MODIFIED_BY], [MODIFIED_DATE]) VALUES (CAST(1 AS Numeric(18, 0)), N'Vistara', NULL, NULL, NULL, CAST(N'2024-01-14' AS Date), NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[TGP_MERCHANT_MASTER] OFF
SET IDENTITY_INSERT [dbo].[TGP_MERCHANT_POLICY_MAPPING] ON 

INSERT [dbo].[TGP_MERCHANT_POLICY_MAPPING] ([ID], [MERCHANT_ID], [POLICY_NO], [STATUS], [CREATED_BY], [CREATED_DATE], [MODIFIED_BY], [MODIFIED_DATE]) VALUES (CAST(1 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'1', N'1', NULL, CAST(N'2022-12-08 20:27:49.477' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[TGP_MERCHANT_POLICY_MAPPING] OFF
SET IDENTITY_INSERT [dbo].[TGP_NOMINEE_DETAILS] ON 

INSERT [dbo].[TGP_NOMINEE_DETAILS] ([id], [customer_id], [nominee1_title], [nominee1_name], [nominee1_gender], [nominee1_dob], [nominee1_mobile_number], [nominee1_email_id], [nominee1_relationship], [nominee1_percentage], [appointee1_title], [appointee1_name], [appointee1_relationship], [appointee1_gender], [appointee1_dob], [nominee2_title], [nominee2_name], [nominee2_gender], [nominee2_dob], [nominee2_mobile_number], [nominee2_email_id], [nominee2_relationship], [nominee2_percentage], [appointee2_title], [appointee2_name], [appointee2_relationship], [appointee2_gender], [appointee2_dob], [created_by], [created_date], [modified_by], [modified_date]) VALUES (CAST(10005 AS Numeric(18, 0)), N'231101171053198', N'Mr', N'Nominee One', N'Male', N'17-01-1989', NULL, NULL, N'Brother', N'100', N'', N'', N'', NULL, N'', N'', N'', N'', N'', NULL, NULL, N'', N'', N'', N'', N'', NULL, N'', NULL, CAST(N'2023-01-11 17:11:51.963' AS DateTime), NULL, CAST(N'2023-01-11 17:11:51.963' AS DateTime))
INSERT [dbo].[TGP_NOMINEE_DETAILS] ([id], [customer_id], [nominee1_title], [nominee1_name], [nominee1_gender], [nominee1_dob], [nominee1_mobile_number], [nominee1_email_id], [nominee1_relationship], [nominee1_percentage], [appointee1_title], [appointee1_name], [appointee1_relationship], [appointee1_gender], [appointee1_dob], [nominee2_title], [nominee2_name], [nominee2_gender], [nominee2_dob], [nominee2_mobile_number], [nominee2_email_id], [nominee2_relationship], [nominee2_percentage], [appointee2_title], [appointee2_name], [appointee2_relationship], [appointee2_gender], [appointee2_dob], [created_by], [created_date], [modified_by], [modified_date]) VALUES (CAST(10006 AS Numeric(18, 0)), N'231301165318670', N'Mr', N'Nominee One', N'Male', N'08-01-1996', NULL, NULL, N'Brother', N'100', N'', N'', N'', NULL, N'', N'', N'', N'', N'', NULL, NULL, N'', N'', N'', N'', N'', NULL, N'', NULL, CAST(N'2023-01-13 17:35:02.767' AS DateTime), NULL, CAST(N'2023-01-13 17:35:02.767' AS DateTime))
INSERT [dbo].[TGP_NOMINEE_DETAILS] ([id], [customer_id], [nominee1_title], [nominee1_name], [nominee1_gender], [nominee1_dob], [nominee1_mobile_number], [nominee1_email_id], [nominee1_relationship], [nominee1_percentage], [appointee1_title], [appointee1_name], [appointee1_relationship], [appointee1_gender], [appointee1_dob], [nominee2_title], [nominee2_name], [nominee2_gender], [nominee2_dob], [nominee2_mobile_number], [nominee2_email_id], [nominee2_relationship], [nominee2_percentage], [appointee2_title], [appointee2_name], [appointee2_relationship], [appointee2_gender], [appointee2_dob], [created_by], [created_date], [modified_by], [modified_date]) VALUES (CAST(10007 AS Numeric(18, 0)), N'231301191820106', N'Mr', N'Nominee One', N'Male', N'05-01-2000', NULL, NULL, N'Brother', N'100', N'', N'', N'', NULL, N'', N'', N'', N'', N'', NULL, NULL, N'', N'', N'', N'', N'', NULL, N'', NULL, CAST(N'2023-01-13 19:19:11.983' AS DateTime), NULL, CAST(N'2023-01-13 19:19:11.983' AS DateTime))
INSERT [dbo].[TGP_NOMINEE_DETAILS] ([id], [customer_id], [nominee1_title], [nominee1_name], [nominee1_gender], [nominee1_dob], [nominee1_mobile_number], [nominee1_email_id], [nominee1_relationship], [nominee1_percentage], [appointee1_title], [appointee1_name], [appointee1_relationship], [appointee1_gender], [appointee1_dob], [nominee2_title], [nominee2_name], [nominee2_gender], [nominee2_dob], [nominee2_mobile_number], [nominee2_email_id], [nominee2_relationship], [nominee2_percentage], [appointee2_title], [appointee2_name], [appointee2_relationship], [appointee2_gender], [appointee2_dob], [created_by], [created_date], [modified_by], [modified_date]) VALUES (CAST(20006 AS Numeric(18, 0)), N'231301191820106', N'Mr', N'Nominee One', N'Male', N'05-01-2000', NULL, NULL, N'Brother', N'100', N'', N'', N'', NULL, N'', N'', N'', N'', N'', NULL, NULL, N'', N'', N'', N'', N'', NULL, N'', NULL, CAST(N'2023-01-16 13:34:49.713' AS DateTime), NULL, CAST(N'2023-01-16 13:34:49.713' AS DateTime))
SET IDENTITY_INSERT [dbo].[TGP_NOMINEE_DETAILS] OFF
SET IDENTITY_INSERT [dbo].[TGP_OTP_TRN_DETAILS] ON 

INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(10160 AS Numeric(18, 0)), N'231101171053198', N'303171217011123', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'E', CAST(N'2023-01-11 17:12:17.307' AS DateTime), CAST(N'2023-01-11 17:12:51.210' AS DateTime), CAST(N'2023-01-11 17:12:17.307' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(10161 AS Numeric(18, 0)), N'231101171053198', N'211511217011123', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-11 17:12:51.213' AS DateTime), CAST(N'2023-01-11 17:13:03.497' AS DateTime), CAST(N'2023-01-11 17:12:51.213' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(10162 AS Numeric(18, 0)), N'231101171053198', N'717593617011123', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-11 17:36:59.727' AS DateTime), CAST(N'2023-01-11 17:37:09.847' AS DateTime), CAST(N'2023-01-11 17:36:59.727' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(10163 AS Numeric(18, 0)), N'231101171053198', N'756471700011223', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'E', CAST(N'2023-01-12 00:17:47.760' AS DateTime), CAST(N'2023-01-12 00:18:24.670' AS DateTime), CAST(N'2023-01-12 00:17:47.760' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(10164 AS Numeric(18, 0)), N'231101171053198', N'670241800011223', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-12 00:18:24.670' AS DateTime), CAST(N'2023-01-12 00:18:33.770' AS DateTime), CAST(N'2023-01-12 00:18:24.670' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(10165 AS Numeric(18, 0)), N'231101171053198', N'500262100011223', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-12 00:21:26.507' AS DateTime), CAST(N'2023-01-12 00:21:34.447' AS DateTime), CAST(N'2023-01-12 00:21:26.507' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(10166 AS Numeric(18, 0)), N'231101171053198', N'940472300011223', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-12 00:23:47.943' AS DateTime), CAST(N'2023-01-12 00:23:59.550' AS DateTime), CAST(N'2023-01-12 00:23:47.943' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(10167 AS Numeric(18, 0)), N'231101171053198', N'792432700011223', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-12 00:27:43.800' AS DateTime), CAST(N'2023-01-12 00:27:49.610' AS DateTime), CAST(N'2023-01-12 00:27:43.800' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(10168 AS Numeric(18, 0)), N'231101171053198', N'737503800011223', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-12 00:38:50.740' AS DateTime), CAST(N'2023-01-12 00:38:57.443' AS DateTime), CAST(N'2023-01-12 00:38:50.740' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(10169 AS Numeric(18, 0)), N'231101171053198', N'578264100011223', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-12 00:41:26.580' AS DateTime), CAST(N'2023-01-12 00:41:35.073' AS DateTime), CAST(N'2023-01-12 00:41:26.580' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(10170 AS Numeric(18, 0)), N'231101171053198', N'580134400011223', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-12 00:44:13.583' AS DateTime), CAST(N'2023-01-12 00:44:18.943' AS DateTime), CAST(N'2023-01-12 00:44:13.583' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20162 AS Numeric(18, 0)), N'231101171053198', N'339222211011223', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-12 11:22:22.350' AS DateTime), CAST(N'2023-01-12 11:22:26.337' AS DateTime), CAST(N'2023-01-12 11:22:22.350' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20163 AS Numeric(18, 0)), N'231101171053198', N'254483913011223', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-12 13:39:48.257' AS DateTime), CAST(N'2023-01-12 13:39:55.077' AS DateTime), CAST(N'2023-01-12 13:39:48.257' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20164 AS Numeric(18, 0)), N'231101171053198', N'648334113011223', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-12 13:41:33.653' AS DateTime), CAST(N'2023-01-12 13:41:42.320' AS DateTime), CAST(N'2023-01-12 13:41:33.653' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20165 AS Numeric(18, 0)), N'231101171053198', N'839334313011223', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-12 13:43:33.843' AS DateTime), CAST(N'2023-01-12 13:43:40.230' AS DateTime), CAST(N'2023-01-12 13:43:33.843' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20166 AS Numeric(18, 0)), N'231101171053198', N'438334713011223', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-12 13:47:33.440' AS DateTime), CAST(N'2023-01-12 13:47:41.007' AS DateTime), CAST(N'2023-01-12 13:47:33.440' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20167 AS Numeric(18, 0)), N'231101171053198', N'207494913011223', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-12 13:49:49.210' AS DateTime), CAST(N'2023-01-12 13:49:56.547' AS DateTime), CAST(N'2023-01-12 13:49:49.210' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20168 AS Numeric(18, 0)), N'231101171053198', N'354550118011223', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-12 18:01:55.357' AS DateTime), CAST(N'2023-01-12 18:02:01.263' AS DateTime), CAST(N'2023-01-12 18:01:55.357' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20169 AS Numeric(18, 0)), N'231101171053198', N'141270418011223', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-12 18:04:27.147' AS DateTime), CAST(N'2023-01-12 18:04:32.513' AS DateTime), CAST(N'2023-01-12 18:04:27.147' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20170 AS Numeric(18, 0)), N'231101171053198', N'096180718011223', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-12 18:07:18.100' AS DateTime), CAST(N'2023-01-12 18:07:23.347' AS DateTime), CAST(N'2023-01-12 18:07:18.100' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20171 AS Numeric(18, 0)), N'231101171053198', N'611221219011223', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-12 19:12:22.617' AS DateTime), CAST(N'2023-01-12 19:12:46.347' AS DateTime), CAST(N'2023-01-12 19:12:22.617' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20172 AS Numeric(18, 0)), N'231101171053198', N'247242219011223', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-12 19:22:24.247' AS DateTime), CAST(N'2023-01-12 19:22:29.147' AS DateTime), CAST(N'2023-01-12 19:22:24.247' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20173 AS Numeric(18, 0)), N'231101171053198', N'038032519011223', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-12 19:25:03.043' AS DateTime), CAST(N'2023-01-12 19:25:07.743' AS DateTime), CAST(N'2023-01-12 19:25:03.043' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20174 AS Numeric(18, 0)), N'231101171053198', N'275483419011223', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-12 19:34:48.290' AS DateTime), CAST(N'2023-01-12 19:34:56.037' AS DateTime), CAST(N'2023-01-12 19:34:48.290' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20175 AS Numeric(18, 0)), N'231101171053198', N'663361820011223', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-12 20:18:36.667' AS DateTime), CAST(N'2023-01-12 20:18:44.823' AS DateTime), CAST(N'2023-01-12 20:18:36.667' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20176 AS Numeric(18, 0)), N'231101171053198', N'127351920011223', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-12 20:19:35.133' AS DateTime), CAST(N'2023-01-12 20:19:46.203' AS DateTime), CAST(N'2023-01-12 20:19:35.133' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20177 AS Numeric(18, 0)), N'231101171053198', N'260592220011223', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-12 20:22:59.263' AS DateTime), CAST(N'2023-01-12 20:23:05.093' AS DateTime), CAST(N'2023-01-12 20:22:59.263' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20178 AS Numeric(18, 0)), N'231101171053198', N'011264011011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-13 11:40:26.020' AS DateTime), CAST(N'2023-01-13 11:40:37.423' AS DateTime), CAST(N'2023-01-13 11:40:26.020' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20179 AS Numeric(18, 0)), N'231101171053198', N'630475312011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-13 12:53:47.633' AS DateTime), CAST(N'2023-01-13 12:54:00.463' AS DateTime), CAST(N'2023-01-13 12:53:47.633' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20180 AS Numeric(18, 0)), N'231101171053198', N'719445512011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-13 12:55:44.727' AS DateTime), CAST(N'2023-01-13 12:55:51.747' AS DateTime), CAST(N'2023-01-13 12:55:44.727' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20181 AS Numeric(18, 0)), N'231101171053198', N'275065912011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-13 12:59:06.277' AS DateTime), CAST(N'2023-01-13 12:59:13.523' AS DateTime), CAST(N'2023-01-13 12:59:06.277' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20182 AS Numeric(18, 0)), N'231101171053198', N'573400213011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-13 13:02:40.577' AS DateTime), CAST(N'2023-01-13 13:02:45.787' AS DateTime), CAST(N'2023-01-13 13:02:40.577' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20183 AS Numeric(18, 0)), N'231101171053198', N'094080513011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-13 13:05:08.097' AS DateTime), CAST(N'2023-01-13 13:05:32.517' AS DateTime), CAST(N'2023-01-13 13:05:08.097' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20184 AS Numeric(18, 0)), N'231101171053198', N'233123213011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-13 13:32:12.237' AS DateTime), CAST(N'2023-01-13 13:32:18.750' AS DateTime), CAST(N'2023-01-13 13:32:12.237' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20185 AS Numeric(18, 0)), N'231301165318670', N'052263517011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-13 17:35:26.053' AS DateTime), CAST(N'2023-01-13 17:35:32.713' AS DateTime), CAST(N'2023-01-13 17:35:26.053' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20186 AS Numeric(18, 0)), N'231301165318670', N'313024317011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-13 17:43:02.320' AS DateTime), CAST(N'2023-01-13 17:43:08.113' AS DateTime), CAST(N'2023-01-13 17:43:02.320' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20187 AS Numeric(18, 0)), N'231301165318670', N'827541818011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-13 18:18:54.830' AS DateTime), CAST(N'2023-01-13 18:19:02.343' AS DateTime), CAST(N'2023-01-13 18:18:54.830' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20188 AS Numeric(18, 0)), N'231301165318670', N'029022618011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-13 18:26:02.037' AS DateTime), CAST(N'2023-01-13 18:26:07.067' AS DateTime), CAST(N'2023-01-13 18:26:02.037' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20189 AS Numeric(18, 0)), N'231301165318670', N'254194118011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'N', CAST(N'2023-01-13 18:41:19.253' AS DateTime), NULL, CAST(N'2023-01-13 18:41:19.253' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20190 AS Numeric(18, 0)), N'231301165318670', N'931420419011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-13 19:04:42.933' AS DateTime), CAST(N'2023-01-13 19:04:48.610' AS DateTime), CAST(N'2023-01-13 19:04:42.933' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20191 AS Numeric(18, 0)), N'231301191820106', N'594301919011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'karthik.chiliveru@comunus.in', N'Y', CAST(N'2023-01-13 19:19:30.597' AS DateTime), CAST(N'2023-01-13 19:19:37.410' AS DateTime), CAST(N'2023-01-13 19:19:30.597' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20192 AS Numeric(18, 0)), N'231301191820106', N'241532319011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'karthik.chiliveru@comunus.in', N'E', CAST(N'2023-01-13 19:23:53.247' AS DateTime), CAST(N'2023-01-13 19:25:11.670' AS DateTime), CAST(N'2023-01-13 19:23:53.247' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20193 AS Numeric(18, 0)), N'231301191820106', N'673112519011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'karthik.chiliveru@comunus.in', N'Y', CAST(N'2023-01-13 19:25:11.677' AS DateTime), CAST(N'2023-01-13 19:25:16.067' AS DateTime), CAST(N'2023-01-13 19:25:11.677' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20194 AS Numeric(18, 0)), N'231301191820106', N'920483119011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'karthik.chiliveru@comunus.in', N'Y', CAST(N'2023-01-13 19:31:48.930' AS DateTime), CAST(N'2023-01-13 19:31:53.210' AS DateTime), CAST(N'2023-01-13 19:31:48.930' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20195 AS Numeric(18, 0)), N'231301191820106', N'856374319011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'karthik.chiliveru@comunus.in', N'Y', CAST(N'2023-01-13 19:43:37.870' AS DateTime), CAST(N'2023-01-13 19:43:46.213' AS DateTime), CAST(N'2023-01-13 19:43:37.870' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20196 AS Numeric(18, 0)), N'231301191820106', N'475144519011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'karthik.chiliveru@comunus.in', N'Y', CAST(N'2023-01-13 19:45:14.490' AS DateTime), CAST(N'2023-01-13 19:45:19.063' AS DateTime), CAST(N'2023-01-13 19:45:14.490' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20197 AS Numeric(18, 0)), N'231301191820106', N'550254619011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'karthik.chiliveru@comunus.in', N'Y', CAST(N'2023-01-13 19:46:25.550' AS DateTime), CAST(N'2023-01-13 19:46:30.127' AS DateTime), CAST(N'2023-01-13 19:46:25.550' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20198 AS Numeric(18, 0)), N'231301191820106', N'327555519011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'karthik.chiliveru@comunus.in', N'Y', CAST(N'2023-01-13 19:55:55.330' AS DateTime), CAST(N'2023-01-13 19:56:04.137' AS DateTime), CAST(N'2023-01-13 19:55:55.330' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20199 AS Numeric(18, 0)), N'231301191820106', N'653155619011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'karthik.chiliveru@comunus.in', N'Y', CAST(N'2023-01-13 19:56:15.657' AS DateTime), CAST(N'2023-01-13 19:56:23.397' AS DateTime), CAST(N'2023-01-13 19:56:15.657' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20200 AS Numeric(18, 0)), N'231301191820106', N'763415619011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'karthik.chiliveru@comunus.in', N'Y', CAST(N'2023-01-13 19:56:41.767' AS DateTime), CAST(N'2023-01-13 19:56:49.123' AS DateTime), CAST(N'2023-01-13 19:56:41.767' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20201 AS Numeric(18, 0)), N'231301191820106', N'565380020011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'karthik.chiliveru@comunus.in', N'Y', CAST(N'2023-01-13 20:00:38.567' AS DateTime), CAST(N'2023-01-13 20:00:47.850' AS DateTime), CAST(N'2023-01-13 20:00:38.567' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20202 AS Numeric(18, 0)), N'231301191820106', N'639230120011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'karthik.chiliveru@comunus.in', N'Y', CAST(N'2023-01-13 20:01:23.640' AS DateTime), CAST(N'2023-01-13 20:01:30.283' AS DateTime), CAST(N'2023-01-13 20:01:23.640' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20203 AS Numeric(18, 0)), N'231301191820106', N'114330420011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'karthik.chiliveru@comunus.in', N'Y', CAST(N'2023-01-13 20:04:33.117' AS DateTime), CAST(N'2023-01-13 20:04:40.490' AS DateTime), CAST(N'2023-01-13 20:04:33.117' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(20204 AS Numeric(18, 0)), N'231301191820106', N'001460720011323', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'karthik.chiliveru@comunus.in', N'Y', CAST(N'2023-01-13 20:07:46.007' AS DateTime), CAST(N'2023-01-13 20:07:51.127' AS DateTime), CAST(N'2023-01-13 20:07:46.007' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(30163 AS Numeric(18, 0)), N'231301165318670', N'734455216011723', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-17 16:52:45.743' AS DateTime), CAST(N'2023-01-17 16:52:51.327' AS DateTime), CAST(N'2023-01-17 16:52:45.743' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(30164 AS Numeric(18, 0)), N'231301165318670', N'895275516011723', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-17 16:55:27.900' AS DateTime), CAST(N'2023-01-17 16:55:33.210' AS DateTime), CAST(N'2023-01-17 16:55:27.900' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(30165 AS Numeric(18, 0)), N'231301165318670', N'402275716011723', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'srinivas.chiliveri@comunus.in', N'Y', CAST(N'2023-01-17 16:57:27.407' AS DateTime), CAST(N'2023-01-17 16:57:33.983' AS DateTime), CAST(N'2023-01-17 16:57:27.407' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(40163 AS Numeric(18, 0)), N'231301191820106', N'647044610020923', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'karthik.chiliveru@comunus.in', N'Y', CAST(N'2023-02-09 10:46:04.657' AS DateTime), CAST(N'2023-02-09 10:46:09.580' AS DateTime), CAST(N'2023-02-09 10:46:04.657' AS DateTime))
INSERT [dbo].[TGP_OTP_TRN_DETAILS] ([id], [customer_id], [otp_id], [otp_code], [otp_mobile], [otp_email], [otp_verified], [otp_gen_time], [otp_verified_time], [created_date]) VALUES (CAST(40164 AS Numeric(18, 0)), N'231301191820106', N'949534610020923', CAST(123456 AS Numeric(18, 0)), CAST(9890838902 AS Numeric(18, 0)), N'karthik.chiliveru@comunus.in', N'Y', CAST(N'2023-02-09 10:46:53.950' AS DateTime), CAST(N'2023-02-09 10:46:59.027' AS DateTime), CAST(N'2023-02-09 10:46:53.950' AS DateTime))
SET IDENTITY_INSERT [dbo].[TGP_OTP_TRN_DETAILS] OFF
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231101171053198-1', N'231101171053198', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2023-01-12 00:38:57.507' AS DateTime), CAST(N'2023-01-12 00:38:57.507' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231101171053198-2', N'231101171053198', N'ce90d54f-d46a-4d3c-b2a5-5180ff45efbf', N'403993715528090583                                ', NULL, NULL, NULL, NULL, NULL, N'{"mihpayid":"403993715528090583","mode":"NB","status":"success","unmappedstatus":"captured","key":"2Siejr","txnid":"231101171053198-2","amount":"1000.00","discount":"0.00","net_amount_debit":"1000","addedon":"2023-01-12 00:41:34","productinfo":"TATA VISTARA","firstname":"srinivas chilveri","lastname":"","address1":"","address2":"","city":"","state":"","country":"","zipcode":"","email":"srinivas.chiliveri@comunus.in","phone":"9890838902","udf1":"","udf2":"","udf3":"","udf4":"","udf5":"","udf6":"","udf7":"","udf8":"","udf9":"","udf10":"","hash":"216f3dbfe9017167eab09f3c13fa99d82bbe6316f1e14758abd49cbaa0c1babf50e91d0596160d128caba85d4d720dcbe7c2f10ec43950a7454c5c65e84ceb34","field1":"","field2":"","field3":"","field4":"","field5":"","field6":"","field7":"","field8":"","field9":"Transaction Completed Successfully","payment_source":"payu","bank_ref_num":"ce90d54f-d46a-4d3c-b2a5-5180ff45efbf","bankcode":"ADBB","error":"E000","error_Message":"No Error","pg_TYPE":""}', N'E000', N'No Error', CAST(N'2023-01-12 00:41:35.150' AS DateTime), CAST(N'2023-01-12 00:42:21.100' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231101171053198-3', N'231101171053198', N'b801dc2e-ae50-43f8-992c-1aa394ba2bef', N'403993715528090585                                ', NULL, NULL, NULL, NULL, NULL, N'{"mihpayid":"403993715528090585","mode":"NB","status":"success","unmappedstatus":"captured","key":"2Siejr","txnid":"231101171053198-3","amount":"1000.00","discount":"0.00","net_amount_debit":"1000","addedon":"2023-01-12 00:44:17","productinfo":"TATA VISTARA","firstname":"srinivas chilveri","lastname":"","address1":"","address2":"","city":"","state":"","country":"","zipcode":"","email":"srinivas.chiliveri@comunus.in","phone":"9890838902","udf1":"","udf2":"","udf3":"","udf4":"","udf5":"","udf6":"","udf7":"","udf8":"","udf9":"","udf10":"","hash":"534879efb7425b5127d1378eb2f91befe0e231d2a1100a0fc0c0e23253d061ef6d37c35a52cc8e9241f807b1ef07fe39bb9c6f6dbefe385e5989a0fdcaacd959","field1":"","field2":"","field3":"","field4":"","field5":"","field6":"","field7":"","field8":"","field9":"Transaction Completed Successfully","payment_source":"payu","bank_ref_num":"b801dc2e-ae50-43f8-992c-1aa394ba2bef","bankcode":"SBIB","error":"E000","error_Message":"No Error","pg_TYPE":""}', N'E000', N'No Error', CAST(N'2023-01-12 00:44:18.993' AS DateTime), CAST(N'2023-01-12 00:44:46.423' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231101171053198-4', N'231101171053198', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2023-01-12 11:22:26.400' AS DateTime), CAST(N'2023-01-12 11:22:26.400' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231101171053198-5', N'231101171053198', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2023-01-12 13:39:55.130' AS DateTime), CAST(N'2023-01-12 13:39:55.130' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231101171053198-6', N'231101171053198', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2023-01-12 13:41:42.370' AS DateTime), CAST(N'2023-01-12 13:41:42.370' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231101171053198-10', N'231101171053198', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2023-01-12 18:02:01.350' AS DateTime), CAST(N'2023-01-12 18:02:01.350' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231101171053198-7', N'231101171053198', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2023-01-12 13:43:40.283' AS DateTime), CAST(N'2023-01-12 13:43:40.283' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231101171053198-8', N'231101171053198', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2023-01-12 13:47:41.037' AS DateTime), CAST(N'2023-01-12 13:47:41.037' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231101171053198-11', N'231101171053198', N'0ec833d0-09b1-4868-a6e1-0ecf797ac3b2', N'403993715528097264                                ', N'Transaction Completed Successfully', N'NB', N'OBCB', N'success', N'captured', N'{"mihpayid":"403993715528097264","mode":"NB","status":"success","unmappedstatus":"captured","key":"2Siejr","txnid":"231101171053198-11","amount":"1000.00","discount":"0.00","net_amount_debit":"1000","addedon":"2023-01-12 18:04:31","productinfo":"TATA VISTARA","firstname":"srinivas chilveri","lastname":"","address1":"","address2":"","city":"","state":"","country":"","zipcode":"","email":"srinivas.chiliveri@comunus.in","phone":"9890838902","udf1":"","udf2":"","udf3":"","udf4":"","udf5":"","udf6":"","udf7":"","udf8":"","udf9":"","udf10":"","hash":"16533060dc040f3358d30c5d9841b6781db2512a754e6a79341a207284b3ad9f34e96bacd7185316fc0004f34e159781f6c04c0a3a6818acad80841ed3cbdacc","field1":"","field2":"","field3":"","field4":"","field5":"","field6":"","field7":"","field8":"","field9":"Transaction Completed Successfully","payment_source":"payu","bank_ref_num":"0ec833d0-09b1-4868-a6e1-0ecf797ac3b2","bankcode":"OBCB","error":"E000","error_Message":"No Error","pg_TYPE":""}', N'E000', N'No Error', CAST(N'2023-01-12 18:04:32.563' AS DateTime), CAST(N'2023-01-12 18:05:19.907' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231101171053198-12', N'231101171053198', N'', N'403993715528097327                                ', N'Invalid mpin', N'UPI', N'UPI', N'failure', N'userCancelled', N'{"mihpayid":"403993715528097327","mode":"UPI","status":"failure","unmappedstatus":"userCancelled","key":"2Siejr","txnid":"231101171053198-12","amount":"1000.00","discount":"0.00","net_amount_debit":"0.00","addedon":"2023-01-12 18:08:59","productinfo":"TATA VISTARA","firstname":"srinivas chilveri","lastname":"","address1":"","address2":"","city":"","state":"","country":"","zipcode":"","email":"srinivas.chiliveri@comunus.in","phone":"9890838902","udf1":"","udf2":"","udf3":"","udf4":"","udf5":"","udf6":"","udf7":"","udf8":"","udf9":"","udf10":"","hash":"de04b92b9207afd645b623918617902f4bebe48d3f931382fe21ccccc45c320698cb53cbbaf27033ca899187bc6c75a8124d0f95c88a09e5ec6a6cb30e6f0d78","field1":"srini@payu","field2":"0","field3":"","field4":"srinivas chilveri","field5":"AXI3gqoyBnsnBmVvPw0OnFV5736fpOuDSbt","field6":"","field7":"Invalid mpin","field8":"generic","field9":"Invalid mpin","payment_source":"payu","bank_ref_num":"","bankcode":"UPI","error":"E000","error_Message":"No Error","pg_TYPE":""}', N'E000', N'No Error', CAST(N'2023-01-12 18:07:23.400' AS DateTime), CAST(N'2023-01-12 18:09:44.340' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231101171053198-13', N'231101171053198', N'21bbee3f-ebb6-4734-b9ba-6cb3b5738c8d', N'403993715528097938                                ', N'Transaction Completed Successfully', N'NB', N'SBIB', N'success', N'captured', N'{"mihpayid":"403993715528097938","mode":"NB","status":"success","unmappedstatus":"captured","key":"2Siejr","txnid":"231101171053198-13","amount":"1.00","discount":"0.00","net_amount_debit":"1","addedon":"2023-01-12 19:12:45","productinfo":"TATA VISTARA","firstname":"srinivas chilveri","lastname":"","address1":"","address2":"","city":"","state":"","country":"","zipcode":"","email":"srinivas.chiliveri@comunus.in","phone":"9890838902","udf1":"","udf2":"","udf3":"","udf4":"","udf5":"","udf6":"","udf7":"","udf8":"","udf9":"","udf10":"","hash":"da2b16cb46e6d0d23831f34e7126b4417aff80b38d7d81d558838e318591116ec6237f80fab1f6a39eafae6af62a90bfe4b790b8e16189e538d1bedba31710fc","field1":"","field2":"","field3":"","field4":"","field5":"","field6":"","field7":"","field8":"","field9":"Transaction Completed Successfully","payment_source":"payu","bank_ref_num":"21bbee3f-ebb6-4734-b9ba-6cb3b5738c8d","bankcode":"SBIB","error":"E000","error_Message":"No Error","pg_TYPE":""}', N'E000', N'No Error', CAST(N'2023-01-12 19:12:46.417' AS DateTime), CAST(N'2023-01-12 19:13:46.703' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231301165318670-1', N'231301165318670', N'7623ef3f-b90c-495d-972e-3e46f84c3fed', N'403993715528104815                                ', N'Transaction Completed Successfully', N'NB', N'162B', N'success', N'captured', N'{"mihpayid":"403993715528104815","mode":"NB","status":"success","unmappedstatus":"captured","key":"2Siejr","txnid":"231301165318670-1","amount":"100.00","discount":"0.00","net_amount_debit":"100","addedon":"2023-01-13 17:35:33","productinfo":"TATA VISTARA","firstname":"Srinivas Chliveri","lastname":"","address1":"","address2":"","city":"","state":"","country":"","zipcode":"","email":"srinivas.chiliveri@comunus.in","phone":"9890838902","udf1":"","udf2":"","udf3":"","udf4":"","udf5":"","udf6":"","udf7":"","udf8":"","udf9":"","udf10":"","hash":"f0fae3bd5150f4de20f50f288897b0e2bdfe9dcad50b9b51698c261ba80b1876e68d0bb3e63303037fa07b2b523e64882eddb1c21785983b3e249934dce75adf","field1":"","field2":"","field3":"","field4":"","field5":"","field6":"","field7":"","field8":"","field9":"Transaction Completed Successfully","payment_source":"payu","bank_ref_num":"7623ef3f-b90c-495d-972e-3e46f84c3fed","bankcode":"162B","error":"E000","error_Message":"No Error","pg_TYPE":""}', N'E000', N'No Error', CAST(N'2023-01-13 17:35:32.827' AS DateTime), CAST(N'2023-01-13 17:36:28.013' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231101171053198-17', N'231101171053198', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2023-01-13 11:40:37.570' AS DateTime), CAST(N'2023-01-13 11:40:37.570' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231301165318670-2', N'231301165318670', N'29eeaa58-3583-4512-a881-677a2df028f5', N'403993715528104932                                ', N'Transaction Completed Successfully', N'NB', N'SBIB', N'success', N'captured', N'{"mihpayid":"403993715528104932","mode":"NB","status":"success","unmappedstatus":"captured","key":"2Siejr","txnid":"231301165318670-2","amount":"100.00","discount":"0.00","net_amount_debit":"100","addedon":"2023-01-13 17:43:08","productinfo":"TATA VISTARA","firstname":"Srinivas Chliveri","lastname":"","address1":"","address2":"","city":"","state":"","country":"","zipcode":"","email":"srinivas.chiliveri@comunus.in","phone":"9890838902","udf1":"","udf2":"","udf3":"","udf4":"","udf5":"","udf6":"","udf7":"","udf8":"","udf9":"","udf10":"","hash":"30065fc73db931b5bf074c68e57ff6b2f6050d1e5bca975d52f73b10a1e95d1c9ea6b56c43398b6f698929d5ab07d6a3f58eb3eba198a881b4014737492b9b4f","field1":"","field2":"","field3":"","field4":"","field5":"","field6":"","field7":"","field8":"","field9":"Transaction Completed Successfully","payment_source":"payu","bank_ref_num":"29eeaa58-3583-4512-a881-677a2df028f5","bankcode":"SBIB","error":"E000","error_Message":"No Error","pg_TYPE":""}', N'E000', N'No Error', CAST(N'2023-01-13 17:43:08.180' AS DateTime), CAST(N'2023-01-13 17:43:40.247' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231301165318670-3', N'231301165318670', N'be216036-cef3-44ea-9328-00889baedf74', N'403993715528105420                                ', N'Transaction Completed Successfully', N'NB', N'HDFB', N'success', N'captured', N'{"mihpayid":"403993715528105420","mode":"NB","status":"success","unmappedstatus":"captured","key":"2Siejr","txnid":"231301165318670-3","amount":"100.00","discount":"0.00","net_amount_debit":"100","addedon":"2023-01-13 18:26:08","productinfo":"TATA VISTARA","firstname":"Srinivas Chliveri","lastname":"","address1":"","address2":"","city":"","state":"","country":"","zipcode":"","email":"srinivas.chiliveri@comunus.in","phone":"9890838902","udf1":"","udf2":"","udf3":"","udf4":"","udf5":"","udf6":"","udf7":"","udf8":"","udf9":"","udf10":"","hash":"48daa83ab1faf80aa5eba4d5e7c94ea291a540f087709d1244ea52b3b74ea58eb2adbe471a28c00bbcb4a15228ec04ba2ee83435d6fd1474eb11c596f99cc909","field1":"","field2":"","field3":"","field4":"","field5":"","field6":"","field7":"","field8":"","field9":"Transaction Completed Successfully","payment_source":"payu","bank_ref_num":"be216036-cef3-44ea-9328-00889baedf74","bankcode":"HDFB","error":"E000","error_Message":"No Error","pg_TYPE":""}', N'E000', N'No Error', CAST(N'2023-01-13 18:26:07.140' AS DateTime), CAST(N'2023-01-13 18:27:27.800' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231301191820106-1', N'231301191820106', N'', N'403993715528105823                                ', N'Insufficient funds in your account', N'NB', N'SBIB', N'failure', N'failed', N'{"mihpayid":"403993715528105823","mode":"NB","status":"failure","unmappedstatus":"failed","key":"2Siejr","txnid":"231301191820106-1","amount":"10.00","discount":"0.00","net_amount_debit":"0.00","addedon":"2023-01-13 19:20:55","productinfo":"TATA VISTARA","firstname":"Karthik Chiliveri","lastname":"","address1":"","address2":"","city":"","state":"","country":"","zipcode":"","email":"karthik.chiliveru@comunus.in","phone":"9890838902","udf1":"","udf2":"","udf3":"","udf4":"","udf5":"","udf6":"","udf7":"","udf8":"","udf9":"","udf10":"","hash":"0af4bde21e493b5b915de750e397c9ce9c8510e33fa917806b3d25aa192b2ef130e0f9f2d16b879b1a991af06a577414518ea3d7ef0836b19d4441038b98d807","field1":"","field2":"","field3":"","field4":"","field5":"","field6":"","field7":"","field8":"","field9":"Insufficient funds in your account","payment_source":"payu","bank_ref_num":"","bankcode":"SBIB","error":"E500","error_Message":"Bank failed to authenticate the customer","pg_TYPE":""}', N'E500', N'Bank failed to authenticate the customer', CAST(N'2023-01-13 19:19:37.517' AS DateTime), CAST(N'2023-01-13 19:21:25.900' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231301191820106-4', N'231301191820106', N'', N'403993715528105945                                ', N'Technical Failure', N'NB', N'SBIB', N'failure', N'failed', N'{"mihpayid":"403993715528105945","mode":"NB","status":"failure","unmappedstatus":"failed","key":"2Siejr","txnid":"231301191820106-4","amount":"10.00","discount":"0.00","net_amount_debit":"0.00","addedon":"2023-01-13 19:44:46","productinfo":"TATA VISTARA","firstname":"Karthik Chiliveri","lastname":"","address1":"","address2":"","city":"","state":"","country":"","zipcode":"","email":"karthik.chiliveru@comunus.in","phone":"9890838902","udf1":"","udf2":"","udf3":"","udf4":"","udf5":"","udf6":"","udf7":"","udf8":"","udf9":"","udf10":"","hash":"8b763f2f6df62ebeac973d7ba53aa75ff06d89f3c03b5b5c783d71dd3624a26ca67309d41da146a575de39c0f3908e321f29fcff3d18c14c77ffe6050ed0aeb7","field1":"","field2":"","field3":"","field4":"","field5":"","field6":"","field7":"","field8":"","field9":"Technical Failure","payment_source":"payu","bank_ref_num":"","bankcode":"SBIB","error":"E500","error_Message":"Bank failed to authenticate the customer","pg_TYPE":""}', N'E500', N'Bank failed to authenticate the customer', CAST(N'2023-01-13 19:43:46.337' AS DateTime), CAST(N'2023-01-13 19:45:02.510' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231301191820106-5', N'231301191820106', N'', N'403993715528105956                                ', N'Transaction Failed at bank end.', N'NB', N'LVCB', N'failure', N'failed', N'{"mihpayid":"403993715528105956","mode":"NB","status":"failure","unmappedstatus":"failed","key":"2Siejr","txnid":"231301191820106-5","amount":"10.00","discount":"0.00","net_amount_debit":"0.00","addedon":"2023-01-13 19:46:03","productinfo":"TATA VISTARA","firstname":"Karthik Chiliveri","lastname":"","address1":"","address2":"","city":"","state":"","country":"","zipcode":"","email":"karthik.chiliveru@comunus.in","phone":"9890838902","udf1":"","udf2":"","udf3":"","udf4":"","udf5":"","udf6":"","udf7":"","udf8":"","udf9":"","udf10":"","hash":"99628b37c537a5e4885de1a188b7296f1b997d9cd99b3a5b87396f2366442f302f6ec522c44e0fc427e1f209861ba704e3eaad80da944ca84b8cf37ff11286cb","field1":"","field2":"","field3":"","field4":"","field5":"","field6":"","field7":"","field8":"","field9":"Transaction Failed at bank end.","payment_source":"payu","bank_ref_num":"","bankcode":"LVCB","error":"E500","error_Message":"Bank failed to authenticate the customer","pg_TYPE":""}', N'E500', N'Bank failed to authenticate the customer', CAST(N'2023-01-13 19:45:19.107' AS DateTime), CAST(N'2023-01-13 19:46:18.900' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231301191820106-6', N'231301191820106', N'c6da29b7-ec8e-4a6a-97a4-955a1a6231bd', N'403993715528105961                                ', N'Transaction Completed Successfully', N'NB', N'SBIB', N'success', N'captured', N'{"mihpayid":"403993715528105961","mode":"NB","status":"success","unmappedstatus":"captured","key":"2Siejr","txnid":"231301191820106-6","amount":"10.00","discount":"0.00","net_amount_debit":"10","addedon":"2023-01-13 19:46:30","productinfo":"TATA VISTARA","firstname":"Karthik Chiliveri","lastname":"","address1":"","address2":"","city":"","state":"","country":"","zipcode":"","email":"karthik.chiliveru@comunus.in","phone":"9890838902","udf1":"","udf2":"","udf3":"","udf4":"","udf5":"","udf6":"","udf7":"","udf8":"","udf9":"","udf10":"","hash":"759d2e29937e56bbf5f08b1f6382952893dc18438a79a78d45ef372a900d283638e4135141d08575a626a1b99f96a4cc6abbc726996b01bdcc140fbb3dc94e55","field1":"","field2":"","field3":"","field4":"","field5":"","field6":"","field7":"","field8":"","field9":"Transaction Completed Successfully","payment_source":"payu","bank_ref_num":"c6da29b7-ec8e-4a6a-97a4-955a1a6231bd","bankcode":"SBIB","error":"E000","error_Message":"No Error","pg_TYPE":""}', N'E000', N'No Error', CAST(N'2023-01-13 19:46:30.160' AS DateTime), CAST(N'2023-01-13 19:46:53.340' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231301165318670-5', N'231301165318670', N'', N'403993715528123190                                ', N'Technical Failure', N'CASH', N'CASH', N'failure', N'failed', N'{"mihpayid":"403993715528123190","mode":"CASH","status":"failure","unmappedstatus":"failed","key":"2Siejr","txnid":"231301165318670-5","amount":"100.00","discount":"0.00","net_amount_debit":"0.00","addedon":"2023-01-17 16:54:16","productinfo":"TATA VISTARA","firstname":"Srinivas Chliveri","lastname":"","address1":"","address2":"","city":"","state":"","country":"","zipcode":"","email":"srinivas.chiliveri@comunus.in","phone":"9890838902","udf1":"","udf2":"","udf3":"","udf4":"","udf5":"","udf6":"","udf7":"","udf8":"","udf9":"","udf10":"","hash":"42673e1df9968cfcc5b1ab73b8cbef8c38d6a626af61ab7965b02fc1d2cceed910dcffb5301b466941fe36e663694de4879b2d78052ae6abcecd17926cdb295e","field1":"","field2":"","field3":"","field4":"","field5":"","field6":"","field7":"","field8":"","field9":"Technical Failure","payment_source":"payu","bank_ref_num":"","bankcode":"CASH","error":"E500","error_Message":"Bank failed to authenticate the customer","pg_TYPE":""}', N'E500', N'Bank failed to authenticate the customer', CAST(N'2023-01-17 16:52:51.507' AS DateTime), CAST(N'2023-01-17 16:54:54.883' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231301165318670-6', N'231301165318670', N'', N'403993715528123214                                ', N'Transaction Failed at bank end', N'UPI', N'UPI', N'failure', N'userCancelled', N'{"mihpayid":"403993715528123214","mode":"UPI","status":"failure","unmappedstatus":"userCancelled","key":"2Siejr","txnid":"231301165318670-6","amount":"100.00","discount":"0.00","net_amount_debit":"0.00","addedon":"2023-01-17 16:55:33","productinfo":"TATA VISTARA","firstname":"Srinivas Chliveri","lastname":"","address1":"","address2":"","city":"","state":"","country":"","zipcode":"","email":"srinivas.chiliveri@comunus.in","phone":"9890838902","udf1":"","udf2":"","udf3":"","udf4":"","udf5":"","udf6":"","udf7":"","udf8":"","udf9":"","udf10":"","hash":"b8b10f320c237a65ef2fd5bdbebae3ee5bcefd05d2e26e6afefe0581288f566fa32d2781572b4f2f56676006150b8a1e82a19b1595b177923944a245fb2ee532","field1":"srini@payu","field2":"0","field3":"","field4":"Srinivas Chliveri","field5":"AXIuLpWBAG6KLKOm9sTGfpYbb0MFyfbTOE9","field6":"","field7":"Transaction Failed at bank end","field8":"generic","field9":"Transaction Failed at bank end","payment_source":"payu","bank_ref_num":"","bankcode":"UPI","error":"E000","error_Message":"No Error","pg_TYPE":""}', N'E000', N'No Error', CAST(N'2023-01-17 16:55:33.313' AS DateTime), CAST(N'2023-01-17 16:56:45.967' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231301165318670-7', N'231301165318670', N'231301165318670-7', N'403993715528123238                                ', N'Transaction completed successfully', N'UPI', N'UPI', N'success', N'captured', N'{"mihpayid":"403993715528123238","mode":"UPI","status":"success","unmappedstatus":"captured","key":"2Siejr","txnid":"231301165318670-7","amount":"100.00","discount":"0.00","net_amount_debit":"100","addedon":"2023-01-17 16:57:34","productinfo":"TATA VISTARA","firstname":"Srinivas Chliveri","lastname":"","address1":"","address2":"","city":"","state":"","country":"","zipcode":"","email":"srinivas.chiliveri@comunus.in","phone":"9890838902","udf1":"","udf2":"","udf3":"","udf4":"","udf5":"","udf6":"","udf7":"","udf8":"","udf9":"","udf10":"","hash":"652b7dc7bac037af1b2e2ed9b16e828f076a4e2cbcbc6d5fe338772d6d411e01086d5f6a05f3dc88710a89995221f12fe50f2f2b1f73c165eb375c839fc0f35d","field1":"7385660157@payu","field2":"231301165318670-7","field3":"","field4":"Srinivas Chliveri","field5":"AXI9Ir9tBuP4CWMC9zfWwKFItGbBUrS6JCd","field6":"","field7":"Transaction completed successfully","field8":"generic","field9":"Transaction completed successfully","payment_source":"payu","bank_ref_num":"231301165318670-7","bankcode":"UPI","error":"E000","error_Message":"No Error","pg_TYPE":""}', N'E000', N'No Error', CAST(N'2023-01-17 16:57:34.053' AS DateTime), CAST(N'2023-01-17 16:58:24.250' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231101171053198-9', N'231101171053198', N'231101171053198-9', N'403993715528094017                                ', N'Transaction completed successfully', N'UPI', N'UPI', N'success', NULL, N'{"mihpayid":"403993715528094017","mode":"UPI","status":"success","unmappedstatus":"captured","key":"2Siejr","txnid":"231101171053198-9","amount":"1000.00","discount":"0.00","net_amount_debit":"1000","addedon":"2023-01-12 13:49:55","productinfo":"TATA VISTARA","firstname":"srinivas chilveri","lastname":"","address1":"","address2":"","city":"","state":"","country":"","zipcode":"","email":"srinivas.chiliveri@comunus.in","phone":"9890838902","udf1":"","udf2":"","udf3":"","udf4":"","udf5":"","udf6":"","udf7":"","udf8":"","udf9":"","udf10":"","hash":"21763d14a5c5edc54af1e1cfc669a612a28402f50f54af2810ebc141230bd1124162c60854906a6a032eb3e1e4b808ddaf8d126ede6cbacd09d429a02fe73ca1","field1":"srini@payu","field2":"231101171053198-9","field3":"","field4":"srinivas chilveri","field5":"AXIrO5d6BLyvBTLkUJmJget9vEhBFBSnabq","field6":"","field7":"Transaction completed successfully","field8":"generic","field9":"Transaction completed successfully","payment_source":"payu","bank_ref_num":"231101171053198-9","bankcode":"UPI","error":"E000","error_Message":"No Error","pg_TYPE":""}', N'E000', N'No Error', CAST(N'2023-01-12 13:49:56.590' AS DateTime), CAST(N'2023-01-12 13:50:56.983' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231101171053198-14', N'231101171053198', N'3b1e306b-8fef-487f-95c4-0e78d8f99c00', N'403993715528098020                                ', N'Transaction Completed Successfully', N'NB', N'SBIB', N'success', N'captured', N'{"mihpayid":"403993715528098020","mode":"NB","status":"success","unmappedstatus":"captured","key":"2Siejr","txnid":"231101171053198-14","amount":"1.00","discount":"0.00","net_amount_debit":"1","addedon":"2023-01-12 19:22:28","productinfo":"TATA VISTARA","firstname":"srinivas chilveri","lastname":"","address1":"","address2":"","city":"","state":"","country":"","zipcode":"","email":"srinivas.chiliveri@comunus.in","phone":"9890838902","udf1":"","udf2":"","udf3":"","udf4":"","udf5":"","udf6":"","udf7":"","udf8":"","udf9":"","udf10":"","hash":"c544cc1010e6252033f814fc05a07bf17968d32232353f135b1fb24f42cee45a9faabd777450e76c3cd069ac834ccb5cd54d5f5f1c120502d945020de4b5d35f","field1":"","field2":"","field3":"","field4":"","field5":"","field6":"","field7":"","field8":"","field9":"Transaction Completed Successfully","payment_source":"payu","bank_ref_num":"3b1e306b-8fef-487f-95c4-0e78d8f99c00","bankcode":"SBIB","error":"E000","error_Message":"No Error","pg_TYPE":""}', N'E000', N'No Error', CAST(N'2023-01-12 19:22:29.197' AS DateTime), CAST(N'2023-01-12 19:23:11.500' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231101171053198-15', N'231101171053198', N'ae917522-9591-4635-b07e-59d4ca5a4a79', N'403993715528098037                                ', N'Transaction Completed Successfully', N'NB', N'ADBB', N'success', N'captured', N'{"mihpayid":"403993715528098037","mode":"NB","status":"success","unmappedstatus":"captured","key":"2Siejr","txnid":"231101171053198-15","amount":"1.00","discount":"0.00","net_amount_debit":"1","addedon":"2023-01-12 19:25:07","productinfo":"TATA VISTARA","firstname":"srinivas chilveri","lastname":"","address1":"","address2":"","city":"","state":"","country":"","zipcode":"","email":"srinivas.chiliveri@comunus.in","phone":"9890838902","udf1":"","udf2":"","udf3":"","udf4":"","udf5":"","udf6":"","udf7":"","udf8":"","udf9":"","udf10":"","hash":"d061a22d63fe522385eeb1e73a55d2c01683a8e3522c7afd85defb919ce3167bbf329008f05a41920eaf3529b2b1c4549c8caae12bade982605a93312ca1d0ad","field1":"","field2":"","field3":"","field4":"","field5":"","field6":"","field7":"","field8":"","field9":"Transaction Completed Successfully","payment_source":"payu","bank_ref_num":"ae917522-9591-4635-b07e-59d4ca5a4a79","bankcode":"ADBB","error":"E000","error_Message":"No Error","pg_TYPE":""}', N'E000', N'No Error', CAST(N'2023-01-12 19:25:07.783' AS DateTime), CAST(N'2023-01-12 19:25:42.683' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231101171053198-16', N'231101171053198', N'b3fd893a-97e8-403f-9ea5-eb6a2d00262b', N'403993715528098097                                ', N'Transaction Completed Successfully', N'NB', N'LVCB', N'success', N'captured', N'{"mihpayid":"403993715528098097","mode":"NB","status":"success","unmappedstatus":"captured","key":"2Siejr","txnid":"231101171053198-16","amount":"1.00","discount":"0.00","net_amount_debit":"1","addedon":"2023-01-12 19:34:55","productinfo":"TATA VISTARA","firstname":"srinivas chilveri","lastname":"","address1":"","address2":"","city":"","state":"","country":"","zipcode":"","email":"srinivas.chiliveri@comunus.in","phone":"9890838902","udf1":"","udf2":"","udf3":"","udf4":"","udf5":"","udf6":"","udf7":"","udf8":"","udf9":"","udf10":"","hash":"91f792eaa080d0e6226f1cf3f8cdf2df31162ebb9bc110207348a6ca33514e9be8bfd8b3dfa87df8ce9228037682ed10f6a422c2d8a2194ff5499bbf754b3037","field1":"","field2":"","field3":"","field4":"","field5":"","field6":"","field7":"","field8":"","field9":"Transaction Completed Successfully","payment_source":"payu","bank_ref_num":"b3fd893a-97e8-403f-9ea5-eb6a2d00262b","bankcode":"LVCB","error":"E000","error_Message":"No Error","pg_TYPE":""}', N'E000', N'No Error', CAST(N'2023-01-12 19:34:56.103' AS DateTime), CAST(N'2023-01-12 19:35:34.177' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231101171053198-18', N'231101171053198', N'3b0bf423-752a-44c0-a115-454ff2e2847c', N'403993715528101636                                ', N'Transaction Completed Successfully', N'NB', N'ADBB', N'success', N'captured', N'{"mihpayid":"403993715528101636","mode":"NB","status":"success","unmappedstatus":"captured","key":"2Siejr","txnid":"231101171053198-18","amount":"1.00","discount":"0.00","net_amount_debit":"1","addedon":"2023-01-13 12:59:13","productinfo":"TATA VISTARA","firstname":"srinivas chilveri","lastname":"","address1":"","address2":"","city":"","state":"","country":"","zipcode":"","email":"srinivas.chiliveri@comunus.in","phone":"9890838902","udf1":"","udf2":"","udf3":"","udf4":"","udf5":"","udf6":"","udf7":"","udf8":"","udf9":"","udf10":"","hash":"3e7017c032d49e099b9cc4e0d0664fe79c0729ed1154472269c42c678e513fc9c2dbbc699941ae23d63bd4e907127b6cb0df5445d911d4094ecf0c51db5c49c8","field1":"","field2":"","field3":"","field4":"","field5":"","field6":"","field7":"","field8":"","field9":"Transaction Completed Successfully","payment_source":"payu","bank_ref_num":"3b0bf423-752a-44c0-a115-454ff2e2847c","bankcode":"ADBB","error":"E000","error_Message":"No Error","pg_TYPE":""}', N'E000', N'No Error', CAST(N'2023-01-13 12:59:13.610' AS DateTime), CAST(N'2023-01-13 12:59:46.677' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231301165318670-4', N'231301165318670', N'', N'403993715528105733                                ', N'Insufficient funds in your account', N'NB', N'LVCB', N'failure', N'failed', N'{"mihpayid":"403993715528105733","mode":"NB","status":"failure","unmappedstatus":"failed","key":"2Siejr","txnid":"231301165318670-4","amount":"100.00","discount":"0.00","net_amount_debit":"0.00","addedon":"2023-01-13 19:06:35","productinfo":"TATA VISTARA","firstname":"Srinivas Chliveri","lastname":"","address1":"","address2":"","city":"","state":"","country":"","zipcode":"","email":"srinivas.chiliveri@comunus.in","phone":"9890838902","udf1":"","udf2":"","udf3":"","udf4":"","udf5":"","udf6":"","udf7":"","udf8":"","udf9":"","udf10":"","hash":"537c01eab2b9a271b87b38967ec5696a2211e0cb194cffa31f0e72a40a2939b6ed036be32c5cc97ac6a9569b6a02fc008ea0ed4dd4ac9b9b8826d03d6c6bde25","field1":"","field2":"","field3":"","field4":"","field5":"","field6":"","field7":"","field8":"","field9":"Insufficient funds in your account","payment_source":"payu","bank_ref_num":"","bankcode":"LVCB","error":"E500","error_Message":"Bank failed to authenticate the customer","pg_TYPE":""}', N'E500', N'Bank failed to authenticate the customer', CAST(N'2023-01-13 19:04:48.717' AS DateTime), CAST(N'2023-01-13 19:07:04.033' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231301191820106-2', N'231301191820106', N'', N'403993715528105854                                ', N'Transaction Failed', N'NB', N'SBIB', N'failure', N'failed', N'{"mihpayid":"403993715528105854","mode":"NB","status":"failure","unmappedstatus":"failed","key":"2Siejr","txnid":"231301191820106-2","amount":"10.00","discount":"0.00","net_amount_debit":"0.00","addedon":"2023-01-13 19:26:23","productinfo":"TATA VISTARA","firstname":"Karthik Chiliveri","lastname":"","address1":"","address2":"","city":"","state":"","country":"","zipcode":"","email":"karthik.chiliveru@comunus.in","phone":"9890838902","udf1":"","udf2":"","udf3":"","udf4":"","udf5":"","udf6":"","udf7":"","udf8":"","udf9":"","udf10":"","hash":"1e7345525575c509e2bf4ae7269db3abe7ae8f9480cc544111a47022008399b2de12a67067a358aa8af191be01cacb7aeb27a2de6c9c60e8145ab121058fd109","field1":"","field2":"","field3":"","field4":"","field5":"","field6":"","field7":"","field8":"","field9":"Transaction Failed","payment_source":"payu","bank_ref_num":"","bankcode":"SBIB","error":"E500","error_Message":"Bank failed to authenticate the customer","pg_TYPE":""}', N'E500', N'Bank failed to authenticate the customer', CAST(N'2023-01-13 19:25:16.123' AS DateTime), CAST(N'2023-01-13 19:26:55.403' AS DateTime))
INSERT [dbo].[TGP_PAYMENT_DETAILS] ([id], [customer_id], [bank_ref_num], [mihpayid], [transaction_description], [mode], [bank_code], [status], [unmapped_status], [response_json], [error], [error_Message], [created_date], [modified_date]) VALUES (N'231301191820106-3', N'231301191820106', N'', N'403993715528105886                                ', N'Authorization failed at Bank', N'NB', N'SBIB', N'failure', N'failed', N'{"mihpayid":"403993715528105886","mode":"NB","status":"failure","unmappedstatus":"failed","key":"2Siejr","txnid":"231301191820106-3","amount":"10.00","discount":"0.00","net_amount_debit":"0.00","addedon":"2023-01-13 19:33:03","productinfo":"TATA VISTARA","firstname":"Karthik Chiliveri","lastname":"","address1":"","address2":"","city":"","state":"","country":"","zipcode":"","email":"karthik.chiliveru@comunus.in","phone":"9890838902","udf1":"","udf2":"","udf3":"","udf4":"","udf5":"","udf6":"","udf7":"","udf8":"","udf9":"","udf10":"","hash":"27530d0d19237cf9a99c7ca8819c9eff75a4f14697a0c569105840c91761385eb53fb06cd4697b5c7e87d04b8eae826d37f38752408e47cabbeeea324eab18cb","field1":"","field2":"","field3":"","field4":"","field5":"","field6":"","field7":"","field8":"","field9":"Authorization failed at Bank","payment_source":"payu","bank_ref_num":"","bankcode":"SBIB","error":"E500","error_Message":"Bank failed to authenticate the customer","pg_TYPE":""}', N'E500', N'Bank failed to authenticate the customer', CAST(N'2023-01-13 19:31:53.343' AS DateTime), CAST(N'2023-01-13 19:33:31.580' AS DateTime))
INSERT [dbo].[TGP_PERSONAL_DETAILS] ([customer_id], [urn], [tier], [name], [dob], [email_id], [mobile_number], [merchant_id], [policy_number], [sum_assured], [premium_amount], [premium_without_gst], [premium_gst], [total_premium], [rider_without_gst], [rider_premium_gst], [total_rider], [gender], [pan_no], [address1], [address2], [address3], [pin_code], [city], [state], [organization_type], [occupation], [annual_income], [nationality], [residential_status], [country_residence], [good_health_flag], [stage], [coi], [coi_pdf], [mef_pdf], [is_submitted], [is_drop_link_sent], [drop_link], [created_date], [created_by], [modified_date], [modified_by]) VALUES (N'231101171053198', N'ABCD001', N'Gold', N'srinivas chilveri', N'08-01-1997', N'srinivas.chiliveri@comunus.in', CAST(9890838902 AS Numeric(20, 0)), CAST(1 AS Numeric(18, 0)), N'GSRP000031', CAST(100000.00 AS Numeric(18, 2)), CAST(1.00 AS Numeric(18, 2)), CAST(1000.00 AS Numeric(18, 2)), CAST(200.00 AS Numeric(18, 2)), CAST(1200.00 AS Numeric(18, 2)), CAST(1000.00 AS Numeric(18, 2)), CAST(10.00 AS Numeric(18, 2)), CAST(1010.00 AS Numeric(18, 2)), N'Male', N'BHJBH9867J', N'A1', N'A2', N'A3', CAST(400604 AS Numeric(10, 0)), N'Thane', N'Maharashtra', N'Government', N'Service', CAST(100000 AS Numeric(18, 0)), N'India', N'India', N'India', N'Y', N'8', N'1000000105', NULL, NULL, N'Y', N'N', N'http://localhost:8081/TGP/Login?req=oUQ%2BiKE6EaoqXAn%2BforwardslasheYw35Aua2T6Klhut0nVforwardslash1l1RCw%3D', CAST(N'2023-01-11 17:10:53.210' AS DateTime), NULL, CAST(N'2023-01-13 13:32:30.503' AS DateTime), NULL)
INSERT [dbo].[TGP_PERSONAL_DETAILS] ([customer_id], [urn], [tier], [name], [dob], [email_id], [mobile_number], [merchant_id], [policy_number], [sum_assured], [premium_amount], [premium_without_gst], [premium_gst], [total_premium], [rider_without_gst], [rider_premium_gst], [total_rider], [gender], [pan_no], [address1], [address2], [address3], [pin_code], [city], [state], [organization_type], [occupation], [annual_income], [nationality], [residential_status], [country_residence], [good_health_flag], [stage], [coi], [coi_pdf], [mef_pdf], [is_submitted], [is_drop_link_sent], [drop_link], [created_date], [created_by], [modified_date], [modified_by]) VALUES (N'231301165318670', N'VISTARA-001', N'Gold', N'Srinivas Chliveri', N'05-01-1988', N'srinivas.chiliveri@comunus.in', CAST(9890838902 AS Numeric(20, 0)), CAST(1 AS Numeric(18, 0)), N'GSRP000031', CAST(10000.00 AS Numeric(18, 2)), CAST(100.00 AS Numeric(18, 2)), CAST(10.00 AS Numeric(18, 2)), CAST(10.00 AS Numeric(18, 2)), CAST(10.00 AS Numeric(18, 2)), CAST(10.00 AS Numeric(18, 2)), CAST(20.00 AS Numeric(18, 2)), CAST(10.00 AS Numeric(18, 2)), N'Male', N'BHNKJ9870K', N'A1', N'A2', N'A3', CAST(400604 AS Numeric(10, 0)), N'Thane', N'Maharashtra', N'Government', N'Business', CAST(100000 AS Numeric(18, 0)), N'India', N'India', N'India', N'Y', N'7', N'1000000105', NULL, NULL, N'Y', N'N', N'http://localhost:8081/TGP/Login?req=uWb6fmWcttn0jwE15JUkg44pU0mmQ8oPmkpp7r0dsYI4OML67avxforwardslashz46jYdBBbpT', CAST(N'2023-01-13 16:53:18.680' AS DateTime), NULL, CAST(N'2023-01-17 16:58:24.270' AS DateTime), NULL)
INSERT [dbo].[TGP_PERSONAL_DETAILS] ([customer_id], [urn], [tier], [name], [dob], [email_id], [mobile_number], [merchant_id], [policy_number], [sum_assured], [premium_amount], [premium_without_gst], [premium_gst], [total_premium], [rider_without_gst], [rider_premium_gst], [total_rider], [gender], [pan_no], [address1], [address2], [address3], [pin_code], [city], [state], [organization_type], [occupation], [annual_income], [nationality], [residential_status], [country_residence], [good_health_flag], [stage], [coi], [coi_pdf], [mef_pdf], [is_submitted], [is_drop_link_sent], [drop_link], [created_date], [created_by], [modified_date], [modified_by]) VALUES (N'231301191820106', N'VISTARA-002', N'Gold', N'Karthik Chiliveri', N'01-01-1988', N'karthik.chiliveru@comunus.in', CAST(9890838902 AS Numeric(20, 0)), CAST(1 AS Numeric(18, 0)), N'GSRP000031', CAST(10000.00 AS Numeric(18, 2)), CAST(10.00 AS Numeric(18, 2)), CAST(10.00 AS Numeric(18, 2)), CAST(10.00 AS Numeric(18, 2)), CAST(10.00 AS Numeric(18, 2)), CAST(10.00 AS Numeric(18, 2)), CAST(10.00 AS Numeric(18, 2)), CAST(10.00 AS Numeric(18, 2)), N'Male', N'NHJBH7687U', N'A1', N'', N'', CAST(400604 AS Numeric(10, 0)), N'Thane', N'Maharashtra', N'Government', N'Professional', CAST(100000 AS Numeric(18, 0)), N'India', N'India', N'India', N'Y', N'7', N'1000000105', NULL, NULL, N'Y', N'N', N'http://localhost:8081/TGP/Login?req=uWb6fmWcttn0jwE15JUkg2DHtN282GnTB24efwhYomU0tadPJHrcoWQueWVMCRby', CAST(N'2023-01-13 19:18:20.120' AS DateTime), NULL, CAST(N'2023-01-16 14:50:32.567' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[TGP_POLICY_MASTER] ON 

INSERT [dbo].[TGP_POLICY_MASTER] ([ID], [POLICY_NO], [BENEFIT_COVERAGE], [POLICY_TERM], [PLAN_NAME], [PLAN_BRIEF], [SUB_OFFICE_CODE], [CREATED_BY], [CREATED_DATE], [MODIFIED_BY], [MODIFIED_DATE]) VALUES (CAST(1 AS Numeric(18, 0)), N'GSRP000031', N'Life', N'12', N'Sampoorna Raksha plan', N'Regular Income with an inbuilt Critical Illness benefit', N'VTARA001', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[TGP_POLICY_MASTER] OFF
SET IDENTITY_INSERT [dbo].[TGP_PROPERTY_CONFIGS] ON 

INSERT [dbo].[TGP_PROPERTY_CONFIGS] ([DESCRIPTION], [PROPERTY], [VALUE], [IS_FRONT_END], [ID]) VALUES (N'Service bypass flag while otp', N'OTP_SERVICE_BYPASS', N'Y', N'N', 1)
INSERT [dbo].[TGP_PROPERTY_CONFIGS] ([DESCRIPTION], [PROPERTY], [VALUE], [IS_FRONT_END], [ID]) VALUES (N'Service bypass flag while premium', N'PREMIUM_SERVICE_BYPASS', N'N', N'N', 2)
INSERT [dbo].[TGP_PROPERTY_CONFIGS] ([DESCRIPTION], [PROPERTY], [VALUE], [IS_FRONT_END], [ID]) VALUES (N'Talic AES secretKey', N'TALIC_AES_SECRETKEY', N'AEST@lic@123SECRETKEY', N'N', 3)
INSERT [dbo].[TGP_PROPERTY_CONFIGS] ([DESCRIPTION], [PROPERTY], [VALUE], [IS_FRONT_END], [ID]) VALUES (N'Glimpe Premium Service', N'TALIC_GLIMPSE_PREMIUM_SERVICE', N'https://appsuat.tataaia.com/GlimpseService/services/GlimpseServiceImpl/getPremium', N'N', 4)
INSERT [dbo].[TGP_PROPERTY_CONFIGS] ([DESCRIPTION], [PROPERTY], [VALUE], [IS_FRONT_END], [ID]) VALUES (N'Talic generate otp Service', N'OTP_GENERATE_SERVICE', N'https://lpu.tataaia.com/LifePlaner/OTPGenerationServlet', N'N', 5)
INSERT [dbo].[TGP_PROPERTY_CONFIGS] ([DESCRIPTION], [PROPERTY], [VALUE], [IS_FRONT_END], [ID]) VALUES (N'Talic issuance Service', N'ISSUANCE_PREMIUM_SERVICE', N'https://appsuat.tataaia.com/GlimpseService/services/GlimpseServiceImpl/getGlimpseResponse', N'N', 6)
INSERT [dbo].[TGP_PROPERTY_CONFIGS] ([DESCRIPTION], [PROPERTY], [VALUE], [IS_FRONT_END], [ID]) VALUES (N'Talic generate pdf Service', N'GENERATE_PDF_SERVICE', N'https://appsuat.tataaia.com/GlimpseService/services/GlimpseServiceImpl/getCOI', N'N', 7)
INSERT [dbo].[TGP_PROPERTY_CONFIGS] ([DESCRIPTION], [PROPERTY], [VALUE], [IS_FRONT_END], [ID]) VALUES (N'VIstara Encryption Key', N'VISTARA_ENCRYPTION_KEY', N'!A%D*G-KaPdSgVkYp3s6v9y/B?E(H+Mb', N'N', 8)
INSERT [dbo].[TGP_PROPERTY_CONFIGS] ([DESCRIPTION], [PROPERTY], [VALUE], [IS_FRONT_END], [ID]) VALUES (N'PayU Merchant Key', N'PAYU_MERCHANT_KEY', N'2Siejr', NULL, 1001)
INSERT [dbo].[TGP_PROPERTY_CONFIGS] ([DESCRIPTION], [PROPERTY], [VALUE], [IS_FRONT_END], [ID]) VALUES (N'PayU Merchant Salt', N'PAYU_MERCHANT_SALT', N'RcQBSIM459kO2t3ReFfvmQMvj2I651ot', NULL, 1002)
INSERT [dbo].[TGP_PROPERTY_CONFIGS] ([DESCRIPTION], [PROPERTY], [VALUE], [IS_FRONT_END], [ID]) VALUES (N'PayU Merchant MID', N'PAYU_MERCHANT_MID', N'115206', NULL, 1003)
INSERT [dbo].[TGP_PROPERTY_CONFIGS] ([DESCRIPTION], [PROPERTY], [VALUE], [IS_FRONT_END], [ID]) VALUES (N'Pay U Product Info', N'PAYU_MERCHANT_PRODUCT_INFO', N'TATA VISTARA', NULL, 1004)
INSERT [dbo].[TGP_PROPERTY_CONFIGS] ([DESCRIPTION], [PROPERTY], [VALUE], [IS_FRONT_END], [ID]) VALUES (N'Pay U Payment Link', N'PAYU_PAYMENT_LINK', N'https://test.payu.in/_payment', NULL, 1005)
INSERT [dbo].[TGP_PROPERTY_CONFIGS] ([DESCRIPTION], [PROPERTY], [VALUE], [IS_FRONT_END], [ID]) VALUES (N'Pay U Sucess Redirect URL', N'PAYU_SUCCESS_URL', N'http://192.168.1.51:8081/TGP/successPayment', NULL, 1006)
INSERT [dbo].[TGP_PROPERTY_CONFIGS] ([DESCRIPTION], [PROPERTY], [VALUE], [IS_FRONT_END], [ID]) VALUES (N'Pay U Failure Redirect URL', N'PAYU_FALURE_URL', N'http://192.168.1.51:8081/TGP/failPayment', NULL, 1007)
SET IDENTITY_INSERT [dbo].[TGP_PROPERTY_CONFIGS] OFF
SET IDENTITY_INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ON 

INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(1 AS Numeric(18, 0)), N'221212173200348', N'Glimpse Issuance API', N'192.168.1.42', N'200', N'Success', N'775', N'{   "metaData": {     "partnerId": "GSRP000037"   },   "data": {     "customerDetails": {"sumAssured": 530.00,"basicPremium": "5056.00","policyTerm": 12,"dob": "02-12-2004","dtapplsignedpolicyholder": "12/12/2022","personalDetails": {  "firstName": "ab",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Mother", "firstName":"RBP", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Female", "percentageAllocation":"100", "dob":"01-10-1983", "mobile":"", "title":"Mrs", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "THANE",    "country": "India",    "city": "MUMBAI",    "houseOrFlat": "WAGLE ESTATE",    "street": "ROAD NO 16",    "pinCode": "400070",    "state": "Maharashtra"  },  "gender": "Female",   "Occupation_details": "Others",  "mobile": 9999999999,  "PAN": "DDWPP3159M",  "email": "abc@gnail.com"},"dateOfIntimation": "12/12/2022","benefit_Coverage": "Life","refId": "sacasc","utrAmt": "5056.00","dtapplsignedproposedinsured": "12/12/2022"     }   } }', CAST(N'2022-12-12 17:36:21.040' AS DateTime), N'[{"status":"Fail","errMsg":"MSG : You are not authorize to use this service ","ipAddr":"110.226.177.139"}]', CAST(N'2022-12-12 17:36:21.817' AS DateTime), CAST(N'2022-12-12 17:36:21.040' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(2 AS Numeric(18, 0)), N'221212173200348', N'Glimpse Issuance API', N'192.168.1.42', N'200', N'Success', N'170', N'{   "metaData": {     "partnerId": "GSRP000037"   },   "data": {     "customerDetails": {"sumAssured": 530.00,"basicPremium": "5056.00","policyTerm": 12,"dob": "02-12-2004","dtapplsignedpolicyholder": "12/12/2022","personalDetails": {  "firstName": "ab",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Mother", "firstName":"RBP", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Female", "percentageAllocation":"100", "dob":"01-10-1983", "mobile":"", "title":"Mrs", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "THANE",    "country": "India",    "city": "MUMBAI",    "houseOrFlat": "WAGLE ESTATE",    "street": "ROAD NO 16",    "pinCode": "400070",    "state": "Maharashtra"  },  "gender": "Female",   "Occupation_details": "Others",  "mobile": 9999999999,  "PAN": "DDWPP3159M",  "email": "abc@gnail.com"},"dateOfIntimation": "12/12/2022","benefit_Coverage": "Life","refId": "sacasc","utrAmt": "5056.00","dtapplsignedproposedinsured": "12/12/2022"     }   } }', CAST(N'2022-12-12 17:37:07.227' AS DateTime), N'[{"status":"Fail","errMsg":"MSG : You are not authorize to use this service ","ipAddr":"110.226.177.139"}]', CAST(N'2022-12-12 17:37:07.397' AS DateTime), CAST(N'2022-12-12 17:37:07.227' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(3 AS Numeric(18, 0)), N'221212173200348', N'Glimpse Issuance API', N'192.168.1.42', N'200', N'Success', N'565', N'{   "metaData": {     "partnerId": "GSRP000037"   },   "data": {     "customerDetails": {"sumAssured": 530.00,"basicPremium": "5056.00","policyTerm": 12,"dob": "02-12-2004","dtapplsignedpolicyholder": "12/12/2022","personalDetails": {  "firstName": "ab",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Mother", "firstName":"RBP", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Female", "percentageAllocation":"100", "dob":"01-10-1983", "mobile":"", "title":"Mrs", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "THANE",    "country": "India",    "city": "MUMBAI",    "houseOrFlat": "WAGLE ESTATE",    "street": "ROAD NO 16",    "pinCode": "400070",    "state": "Maharashtra"  },  "gender": "Female",   "Occupation_details": "Others",  "mobile": 9999999999,  "PAN": "DDWPP3159M",  "email": "abc@gnail.com"},"dateOfIntimation": "12/12/2022","benefit_Coverage": "Life","refId": "sacasc","utrAmt": "5056.00","dtapplsignedproposedinsured": "12/12/2022"     }   } }', CAST(N'2022-12-12 17:39:57.373' AS DateTime), N'[{"status":"Fail","errMsg":"MSG : You are not authorize to use this service ","ipAddr":"110.226.177.139"}]', CAST(N'2022-12-12 17:39:57.937' AS DateTime), CAST(N'2022-12-12 17:39:57.373' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(4 AS Numeric(18, 0)), N'221212173200348', N'Glimpse Issuance API', N'192.168.1.42', N'200', N'Success', N'594', N'{   "metaData": {     "partnerId": "GSRP000037"   },   "data": {     "customerDetails": {"sumAssured": 530.00,"basicPremium": "5056.00","policyTerm": 12,"dob": "02-12-2004","dtapplsignedpolicyholder": "12/12/2022","personalDetails": {  "firstName": "ab",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Mother", "firstName":"RBP", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Female", "percentageAllocation":"100", "dob":"01-10-1983", "mobile":"", "title":"Mrs", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "THANE",    "country": "India",    "city": "MUMBAI",    "houseOrFlat": "WAGLE ESTATE",    "street": "ROAD NO 16",    "pinCode": "400070",    "state": "Maharashtra"  },  "gender": "Female",   "Occupation_details": "Others",  "mobile": 9999999999,  "PAN": "DDWPP3159M",  "email": "abc@gnail.com"},"dateOfIntimation": "12/12/2022","benefit_Coverage": "Life","refId": "sacasc","utrAmt": "5056.00","dtapplsignedproposedinsured": "12/12/2022"     }   } }', CAST(N'2022-12-12 17:41:33.050' AS DateTime), N'[{"status":"Fail","errMsg":"MSG : You are not authorize to use this service ","ipAddr":"110.226.177.139"}]', CAST(N'2022-12-12 17:41:33.647' AS DateTime), CAST(N'2022-12-12 17:41:33.050' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(5 AS Numeric(18, 0)), N'221212173200348', N'Glimpse Issuance API', N'192.168.1.42', N'200', N'Success', N'1416', N'{   "metaData": {     "partnerId": "GSRP000037"   },   "data": {     "customerDetails": {"sumAssured": 530.00,"basicPremium": "5056.00","policyTerm": 12,"dob": "02-12-2004","dtapplsignedpolicyholder": "12/12/2022","personalDetails": {  "firstName": "ab",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Mother", "firstName":"RBP", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Female", "percentageAllocation":"100", "dob":"01-10-1983", "mobile":"", "title":"Mrs", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "THANE",    "country": "India",    "city": "MUMBAI",    "houseOrFlat": "WAGLE ESTATE",    "street": "ROAD NO 16",    "pinCode": "400070",    "state": "Maharashtra"  },  "gender": "Female",   "Occupation_details": "Others",  "mobile": 9999999999,  "PAN": "DDWPP3159M",  "email": "abc@gnail.com"},"dateOfIntimation": "12/12/2022","benefit_Coverage": "Life","refId": "sacasc","utrAmt": "5056.00","dtapplsignedproposedinsured": "12/12/2022"     }   } }', CAST(N'2022-12-12 17:42:06.883' AS DateTime), N'[{"status":"Success","data":[{"customerId":"PQRST-10356666666660","coi":"1000000105","policyNo":"GSRP000037"}]}]', CAST(N'2022-12-12 17:42:08.300' AS DateTime), CAST(N'2022-12-12 17:42:06.883' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(6 AS Numeric(18, 0)), N'221212173200348', N'Glimpse Policy PDF', N'192.168.1.42', N'200', N'Success', N'10726', N'{"policyNumber":"GSRP000037","coi":1000000105}', CAST(N'2022-12-12 17:42:08.327' AS DateTime), N'{"PolicyInfoList":[{"PolicyInfo":"<policy pdf response data>","ErrorMessage":""}],"PolicyNumber":"GSRP000037","COI":"1000000105"}', CAST(N'2022-12-12 17:42:19.177' AS DateTime), CAST(N'2022-12-12 17:42:08.327' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(7 AS Numeric(18, 0)), N'221412175558011', N'Glimpse Issuance API', N'192.168.0.163', N'200', N'Success', N'2446', N'{   "metaData": {     "partnerId": "GSRP000037"   },   "data": {     "customerDetails": {"sumAssured": 1200000.00,"basicPremium": "5000.00","policyTerm": 12,"dob": "02-12-2004","dtapplsignedpolicyholder": "14/12/2022","personalDetails": {  "firstName": "Aravind A",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"01-12-2000", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 8000000,  "address": {    "area": "",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Service",  "mobile": 9890838902,  "PAN": "BNHBH6258L",  "email": "abc@gmail.com"},"dateOfIntimation": "14/12/2022","benefit_Coverage": "Life","refId": "AAAA1234","utrAmt": "5000.00","dtapplsignedproposedinsured": "14/12/2022"     }   } }', CAST(N'2022-12-14 17:57:51.630' AS DateTime), N'[{"status":"Success","data":[{"customerId":"PQRST-10356666666660","coi":"1000000105","policyNo":"GSRP000037"}]}]', CAST(N'2022-12-14 17:57:54.077' AS DateTime), CAST(N'2022-12-14 17:57:51.630' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(8 AS Numeric(18, 0)), N'221412175558011', N'Glimpse Policy PDF', N'192.168.0.163', N'200', N'Success', N'18430', N'{"policyNumber":"GSRP000037","coi":1000000105}', CAST(N'2022-12-14 17:57:54.110' AS DateTime), N'{"PolicyInfoList":[{"PolicyInfo":"<policy pdf response data>","ErrorMessage":""}],"PolicyNumber":"GSRP000037","COI":"1000000105"}', CAST(N'2022-12-14 17:58:12.743' AS DateTime), CAST(N'2022-12-14 17:57:54.110' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(9 AS Numeric(18, 0)), N'221212124457880', N'Glimpse Issuance API', N'192.168.1.29', N'200', N'Success', N'701', N'{   "metaData": {     "partnerId": "GSRP000037"   },   "data": {     "customerDetails": {"sumAssured": 100000.00,"basicPremium": "8204.00","policyTerm": 12,"dob": "01-12-1994","dtapplsignedpolicyholder": "15/12/2022","personalDetails": {  "firstName": "Karthik Chilveru",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"01-12-2000", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 500000,  "address": {    "area": "A3",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "A2",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Business",  "mobile": 9890838902,  "PAN": "VHFNK8625L",  "email": "Srinivas.ComunusTechnologiesPvtLtd@tataaia.com"},"dateOfIntimation": "15/12/2022","benefit_Coverage": "Life","refId": "VISTARA123","utrAmt": "8204.00","dtapplsignedproposedinsured": "15/12/2022"     }   } }', CAST(N'2022-12-15 17:11:57.533' AS DateTime), N'[{"status":"Fail","errMsg":"MSG : You are not authorize to use this service ","ipAddr":"110.226.178.236"}]', CAST(N'2022-12-15 17:11:58.233' AS DateTime), CAST(N'2022-12-15 17:11:57.533' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(10 AS Numeric(18, 0)), N'221212124457880', N'Glimpse Issuance API', N'192.168.1.29', N'200', N'Success', N'687', N'{   "metaData": {     "partnerId": "GSRP000037"   },   "data": {     "customerDetails": {"sumAssured": 100000.00,"basicPremium": "8204.00","policyTerm": 12,"dob": "01-12-1994","dtapplsignedpolicyholder": "15/12/2022","personalDetails": {  "firstName": "Karthik Chilveru",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"01-12-2000", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 500000,  "address": {    "area": "A3",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "A2",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Business",  "mobile": 9890838902,  "PAN": "VHFNK8625L",  "email": "Srinivas.ComunusTechnologiesPvtLtd@tataaia.com"},"dateOfIntimation": "15/12/2022","benefit_Coverage": "Life","refId": "VISTARA123","utrAmt": "8204.00","dtapplsignedproposedinsured": "15/12/2022"     }   } }', CAST(N'2022-12-15 17:15:52.717' AS DateTime), N'[{"status":"Fail","errMsg":"MSG : You are not authorize to use this service ","ipAddr":"110.226.178.236"}]', CAST(N'2022-12-15 17:15:53.403' AS DateTime), CAST(N'2022-12-15 17:15:52.717' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(11 AS Numeric(18, 0)), N'221212124457880', N'Glimpse Issuance API', N'192.168.1.29', N'200', N'Success', N'256', N'{   "metaData": {     "partnerId": "GSRP000037"   },   "data": {     "customerDetails": {"sumAssured": 100000.00,"basicPremium": "8204.00","policyTerm": 12,"dob": "01-12-1994","dtapplsignedpolicyholder": "15/12/2022","personalDetails": {  "firstName": "Karthik Chilveru",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"01-12-2000", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 500000,  "address": {    "area": "A3",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "A2",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Business",  "mobile": 9890838902,  "PAN": "VHFNK8625L",  "email": "Srinivas.ComunusTechnologiesPvtLtd@tataaia.com"},"dateOfIntimation": "15/12/2022","benefit_Coverage": "Life","refId": "VISTARA123","utrAmt": "8204.00","dtapplsignedproposedinsured": "15/12/2022"     }   } }', CAST(N'2022-12-15 17:27:33.047' AS DateTime), N'[{"status":"Fail","errMsg":"MSG : You are not authorize to use this service ","ipAddr":"110.226.178.236"}]', CAST(N'2022-12-15 17:27:33.307' AS DateTime), CAST(N'2022-12-15 17:27:33.047' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(12 AS Numeric(18, 0)), N'221212124457880', N'Glimpse Issuance API', N'192.168.1.29', N'200', N'Success', N'661', N'{   "metaData": {     "partnerId": "GSRP000037"   },   "data": {     "customerDetails": {"sumAssured": 100000.00,"basicPremium": "8204.00","policyTerm": 12,"dob": "01-12-1994","dtapplsignedpolicyholder": "15/12/2022","personalDetails": {  "firstName": "Karthik Chilveru",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"01-12-2000", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 500000,  "address": {    "area": "A3",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "A2",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Business",  "mobile": 9890838902,  "PAN": "VHFNK8625L",  "email": "Srinivas.ComunusTechnologiesPvtLtd@tataaia.com"},"dateOfIntimation": "15/12/2022","benefit_Coverage": "Life","refId": "VISTARA123","utrAmt": "8204.00","dtapplsignedproposedinsured": "15/12/2022"     }   } }', CAST(N'2022-12-15 17:28:32.980' AS DateTime), N'[{"status":"Fail","errMsg":"MSG : You are not authorize to use this service ","ipAddr":"110.226.178.236"}]', CAST(N'2022-12-15 17:28:33.640' AS DateTime), CAST(N'2022-12-15 17:28:32.980' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(13 AS Numeric(18, 0)), N'221212124457880', N'Glimpse Issuance API', N'192.168.1.38', N'200', N'Success', N'656', N'{   "metaData": {     "partnerId": "GSRP000037"   },   "data": {     "customerDetails": {"sumAssured": 100000.00,"basicPremium": "8204.00","policyTerm": 12,"dob": "01/12/1994","dtapplsignedpolicyholder": "27/12/2022","personalDetails": {  "firstName": "Karthik Chilveru",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"01/12/2000", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 500000,  "address": {    "area": "A3",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "A2",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Business",  "mobile": 9890838902,  "PAN": "VHFNK8625L",  "email": "Srinivas.ComunusTechnologiesPvtLtd@tataaia.com"},"dateOfIntimation": "27/12/2022","benefit_Coverage": "Life","refId": "VISTARA123","utrAmt": "8204.00","dtapplsignedproposedinsured": "27/12/2022"     }   } }', CAST(N'2022-12-27 13:00:38.943' AS DateTime), N'[{"status":"Fail","errMsg":"MSG : You are not authorize to use this service ","ipAddr":"110.226.185.127"}]', CAST(N'2022-12-27 13:00:39.600' AS DateTime), CAST(N'2022-12-27 13:00:38.943' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(14 AS Numeric(18, 0)), N'221212124457880', N'Glimpse Issuance API', N'192.168.1.38', N'200', N'Success', N'552', N'{   "metaData": {     "partnerId": "GSRP000037"   },   "data": {     "customerDetails": {"sumAssured": 100000.00,"basicPremium": "8204.00","policyTerm": 12,"dob": "01/12/1994","dtapplsignedpolicyholder": "27/12/2022","personalDetails": {  "firstName": "Karthik Chilveru",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"01/12/2000", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 500000,  "address": {    "area": "A3",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "A2",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Business",  "mobile": 9890838902,  "PAN": "VHFNK8625L",  "email": "Srinivas.ComunusTechnologiesPvtLtd@tataaia.com"},"dateOfIntimation": "27/12/2022","benefit_Coverage": "Life","refId": "VISTARA123","utrAmt": "8204.00","dtapplsignedproposedinsured": "27/12/2022"     }   } }', CAST(N'2022-12-27 13:04:06.340' AS DateTime), N'[{"status":"Fail","errMsg":"MSG : You are not authorize to use this service ","ipAddr":"110.226.185.127"}]', CAST(N'2022-12-27 13:04:06.890' AS DateTime), CAST(N'2022-12-27 13:04:06.340' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(15 AS Numeric(18, 0)), N'221212124457880', N'Glimpse Issuance API', N'192.168.1.38', N'200', N'Success', N'558', N'{   "metaData": {     "partnerId": "GSRP000037"   },   "data": {     "customerDetails": {"sumAssured": 100000.00,"basicPremium": "8204.00","policyTerm": 12,"dob": "01/12/1994","dtapplsignedpolicyholder": "27/12/2022","personalDetails": {  "firstName": "Karthik Chilveru",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"01/12/2000", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 500000,  "address": {    "area": "A3",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "A2",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Business",  "mobile": 9890838902,  "PAN": "VHFNK8625L",  "email": "Srinivas.ComunusTechnologiesPvtLtd@tataaia.com"},"dateOfIntimation": "27/12/2022","benefit_Coverage": "Life","refId": "VISTARA123","utrAmt": "8204.00","dtapplsignedproposedinsured": "27/12/2022"     }   } }', CAST(N'2022-12-27 13:04:46.553' AS DateTime), N'[{"status":"Fail","errMsg":"MSG : You are not authorize to use this service ","ipAddr":"110.226.185.127"}]', CAST(N'2022-12-27 13:04:47.110' AS DateTime), CAST(N'2022-12-27 13:04:46.553' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(16 AS Numeric(18, 0)), N'221212124457880', N'Glimpse Issuance API', N'192.168.1.38', N'200', N'Success', N'9419', N'{   "metaData": {     "partnerId": "GSRP000037"   },   "data": {     "customerDetails": {"sumAssured": 100000.00,"basicPremium": "8204.00","policyTerm": 12,"dob": "01/12/1994","dtapplsignedpolicyholder": "27/12/2022","personalDetails": {  "firstName": "Karthik Chilveru",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"01/12/2000", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 500000,  "address": {    "area": "A3",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "A2",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Business",  "mobile": 9890838902,  "PAN": "VHFNK8625L",  "email": "Srinivas.ComunusTechnologiesPvtLtd@tataaia.com"},"dateOfIntimation": "27/12/2022","benefit_Coverage": "Life","refId": "VISTARA123","utrAmt": "8204.00","dtapplsignedproposedinsured": "27/12/2022"     }   } }', CAST(N'2022-12-27 13:08:59.100' AS DateTime), N'[{"status":"Success","data":[{"customerId":"PQRST-10356666666660","coi":"1000000105","policyNo":"GSRP000037"}]}]', CAST(N'2022-12-27 13:09:08.520' AS DateTime), CAST(N'2022-12-27 13:08:59.100' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(17 AS Numeric(18, 0)), N'221212124457880', N'Glimpse Issuance API', N'192.168.1.38', N'200', N'Success', N'9612', N'{   "metaData": {     "partnerId": "GSRP000037"   },   "data": {     "customerDetails": {"sumAssured": 100000.00,"basicPremium": "8204.00","policyTerm": 12,"dob": "01/12/1994","dtapplsignedpolicyholder": "27/12/2022","personalDetails": {  "firstName": "Karthik Chilveru",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"01/12/2000", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 500000,  "address": {    "area": "A3",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "A2",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Business",  "mobile": 9890838902,  "PAN": "VHFNK8625L",  "email": "Srinivas.ComunusTechnologiesPvtLtd@tataaia.com"},"dateOfIntimation": "27/12/2022","benefit_Coverage": "Life","refId": "VISTARA123","utrAmt": "8204.00","dtapplsignedproposedinsured": "27/12/2022"     }   } }', CAST(N'2022-12-27 13:12:16.750' AS DateTime), N'[{"metaData":{"errors":[{"errorCode":["E058"],"errorMessage":["Mismatches in the premium calculated by the system and the premium amount collected."],"refId":"VISTARA123"}],"status":"Fail"}}]', CAST(N'2022-12-27 13:12:26.363' AS DateTime), CAST(N'2022-12-27 13:12:16.750' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(18 AS Numeric(18, 0)), N'222712131734285', N'Glimpse Issuance API', N'192.168.1.38', N'200', N'Success', N'9811', N'{   "metaData": {     "partnerId": "GSRP000037"   },   "data": {     "customerDetails": {"sumAssured": 500000.00,"basicPremium": "9204.00","policyTerm": 12,"dob": "07/12/1989","dtapplsignedpolicyholder": "27/12/2022","personalDetails": {  "firstName": "Applicant One",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"06/12/2001", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 800000,  "address": {    "area": "",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Business",  "mobile": 9890838902,  "PAN": "HBGHJ5169M",  "email": "Srinivas.ComunusTechnologiesPvtLtd@tataaia.com"},"dateOfIntimation": "27/12/2022","benefit_Coverage": "Life","refId": "VISTARA2712221317","utrAmt": "9204.00","dtapplsignedproposedinsured": "27/12/2022"     }   } }', CAST(N'2022-12-27 13:18:48.843' AS DateTime), N'[{"metaData":{"errors":[{"errorCode":["E058"],"errorMessage":["Mismatches in the premium calculated by the system and the premium amount collected."],"refId":"VISTARA2712221317"}],"status":"Fail"}}]', CAST(N'2022-12-27 13:18:58.657' AS DateTime), CAST(N'2022-12-27 13:18:48.843' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(19 AS Numeric(18, 0)), N'222712131734285', N'Glimpse Issuance API', N'192.168.1.38', N'200', N'Success', N'9851', N'{   "metaData": {     "partnerId": "GSRP000037"   },   "data": {     "customerDetails": {"sumAssured": 500000.00,"basicPremium": "9204.00","policyTerm": 12,"dob": "07/12/1989","dtapplsignedpolicyholder": "27/12/2022","personalDetails": {  "firstName": "Applicant One",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"06/12/2001", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 800000,  "address": {    "area": "",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Business",  "mobile": 9890838902,  "PAN": "HBGHJ5169M",  "email": "Srinivas.ComunusTechnologiesPvtLtd@tataaia.com"},"dateOfIntimation": "27/12/2022","benefit_Coverage": "Life","refId": "VISTARA2712221317","utrAmt": "9204.00","dtapplsignedproposedinsured": "27/12/2022"     }   } }', CAST(N'2022-12-27 17:48:52.413' AS DateTime), N'[{"metaData":{"errors":[{"errorCode":["E058"],"errorMessage":["Mismatches in the premium calculated by the system and the premium amount collected."],"refId":"VISTARA2712221317"}],"status":"Fail"}}]', CAST(N'2022-12-27 17:49:02.267' AS DateTime), CAST(N'2022-12-27 17:48:52.413' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20 AS Numeric(18, 0)), N'230501200220843', N'Glimpse Issuance API', N'192.168.1.41', N'200', N'Success', N'1324', N'{   "metaData": {     "partnerId": "GSRP000037"   },   "data": {     "customerDetails": {"sumAssured": 100000.00,"basicPremium": "5000.00","policyTerm": 12,"dob": "06/01/1993","dtapplsignedpolicyholder": "05/01/2023","personalDetails": {  "firstName": "srinivas chilivery",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"02/01/1985", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "A3",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "A2",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Business",  "mobile": 9890838902,  "PAN": "HBJJH9876J",  "email": "Srinivas.ComunusTechnologiesPvtLtd@tataaia.com"},"dateOfIntimation": "05/01/2023","benefit_Coverage": "Life","refId": "VISTARA1111","utrAmt": "5000.00","dtapplsignedproposedinsured": "05/01/2023"     }   } }', CAST(N'2023-01-05 20:03:41.723' AS DateTime), N'[{"status":"Fail","errMsg":"MSG : You are not authorize to use this service ","ipAddr":"110.226.183.79"}]', CAST(N'2023-01-05 20:03:43.047' AS DateTime), CAST(N'2023-01-05 20:03:41.723' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(10001 AS Numeric(18, 0)), N'231101171053198', N'Glimpse Issuance API', N'192.168.1.160', N'200', N'Success', N'894', N'{   "metaData": {     "partnerId": "GSRP000031"   },   "data": {     "customerDetails": {"sumAssured": 100000.00,"basicPremium": "1000.00","policyTerm": 12,"dob": "08/01/1997","dtapplsignedpolicyholder": "11/01/2023","personalDetails": {  "firstName": "srinivas chilveri",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"17/01/1989", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "A3",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "A2",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Service",  "mobile": 9890838902,  "PAN": "BHJBH9867J",  "email": "srinivas.chiliveri@comunus.in"},"dateOfIntimation": "11/01/2023","benefit_Coverage": "Life","refId": "ABCD001","utrAmt": "1000.00","dtapplsignedproposedinsured": "11/01/2023"     }   } }', CAST(N'2023-01-11 17:12:25.320' AS DateTime), N'[{"status":"Fail","errMsg":"MSG : You are not authorize to use this service ","ipAddr":"110.226.183.79"}]', CAST(N'2023-01-11 17:12:26.217' AS DateTime), CAST(N'2023-01-11 17:12:25.320' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(10002 AS Numeric(18, 0)), N'231101171053198', N'Glimpse Issuance API', N'192.168.1.160', N'200', N'Success', N'2101', N'{   "metaData": {     "partnerId": "GSRP000031"   },   "data": {     "customerDetails": {"sumAssured": 100000.00,"basicPremium": "1000.00","policyTerm": 12,"dob": "08/01/1997","dtapplsignedpolicyholder": "11/01/2023","personalDetails": {  "firstName": "srinivas chilveri",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"17/01/1989", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "A3",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "A2",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Service",  "mobile": 9890838902,  "PAN": "BHJBH9867J",  "email": "srinivas.chiliveri@comunus.in"},"dateOfIntimation": "11/01/2023","benefit_Coverage": "Life","refId": "ABCD001","utrAmt": "1000.00","dtapplsignedproposedinsured": "11/01/2023"     }   } }', CAST(N'2023-01-11 17:13:03.507' AS DateTime), N'[{"metaData":{"errors":[{"errorCode":["E004"],"errorMessage":[" Loan Amount Field Cannot be blank and zero."],"refId":"ABCD001"},{"errorCode":["E005"],"errorMessage":[" Loan Tenure can not be blank and Zero"],"refId":"ABCD001"},{"errorCode":["E008"],"errorMessage":[" Invalid Loan Disbursement date Date format."],"refId":"ABCD001"},{"errorCode":["E009"],"errorMessage":[" Date of Loan Disbursement can not be blank"],"refId":"ABCD001"},{"errorCode":["E010"],"errorMessage":[" Sub office Code can not be blank"],"refId":"ABCD001"},{"errorCode":["E011"],"errorMessage":[" Invalid Sub office Code"],"refId":"ABCD001"},{"errorCode":["E012"],"errorMessage":[" Applicant Type Can not be blank."],"refId":"ABCD001"},{"errorCode":["E016"],"errorMessage":[" Percentage Sharing cannot be blank and zero"],"refId":"ABCD001"},{"errorCode":["E017"],"errorMessage":[" Date of intimation can not be future date"],"refId":"ABCD001"},{"errorCode":["E020"],"errorMessage":[" Date of application signed policyholder can not be future date"],"refId":"ABCD001"},{"errorCode":["E023"],"errorMessage":[" Date of application signed Proposed insured can not be future date"],"refId":"ABCD001"},{"errorCode":["E026"],"errorMessage":[" CoverType Can not be blank."],"refId":"ABCD001"},{"errorCode":["E028"],"errorMessage":[" Invalid CoverType."],"refId":"ABCD001"},{"errorCode":["E036"],"errorMessage":[" UTR Number can not be blank."],"refId":"ABCD001"},{"errorCode":["E038"],"errorMessage":[" UTR Date can not be blank."],"refId":"ABCD001"},{"errorCode":["E039"],"errorMessage":[" UTR Date can not be blank."],"refId":"ABCD001"},{"errorCode":["E058"],"errorMessage":["Mismatches in the premium calculated by the system and the premium amount collected."],"refId":"ABCD001"}],"status":"Fail"}}]', CAST(N'2023-01-11 17:13:05.607' AS DateTime), CAST(N'2023-01-11 17:13:03.507' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(10003 AS Numeric(18, 0)), N'231101171053198', N'Glimpse Issuance API', N'192.168.1.160', N'200', N'Success', N'2721', N'{   "metaData": {     "partnerId": "GSRP000031"   },   "data": {     "customerDetails": {"sumAssured": 100000.00,"basicPremium": "1000.00","policyTerm": 12,"dob": "08/01/1997","dtapplsignedpolicyholder": "11/01/2023","personalDetails": {  "firstName": "srinivas chilveri",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"17/01/1989", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": "100000",  "address": {    "area": "A3",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "A2",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Service",  "mobile": 9890838902,  "PAN": "BHJBH9867J",  "email": "srinivas.chiliveri@comunus.in"},"dateOfIntimation": "11/01/2023","benefit_Coverage": "Life","refId": "ABCD001","utrAmt": "1000.00","dtapplsignedproposedinsured": "11/01/2023"     }   } }', CAST(N'2023-01-11 17:37:09.860' AS DateTime), N'[{"metaData":{"errors":[{"errorCode":["E004"],"errorMessage":[" Loan Amount Field Cannot be blank and zero."],"refId":"ABCD001"},{"errorCode":["E005"],"errorMessage":[" Loan Tenure can not be blank and Zero"],"refId":"ABCD001"},{"errorCode":["E008"],"errorMessage":[" Invalid Loan Disbursement date Date format."],"refId":"ABCD001"},{"errorCode":["E009"],"errorMessage":[" Date of Loan Disbursement can not be blank"],"refId":"ABCD001"},{"errorCode":["E010"],"errorMessage":[" Sub office Code can not be blank"],"refId":"ABCD001"},{"errorCode":["E011"],"errorMessage":[" Invalid Sub office Code"],"refId":"ABCD001"},{"errorCode":["E012"],"errorMessage":[" Applicant Type Can not be blank."],"refId":"ABCD001"},{"errorCode":["E016"],"errorMessage":[" Percentage Sharing cannot be blank and zero"],"refId":"ABCD001"},{"errorCode":["E017"],"errorMessage":[" Date of intimation can not be future date"],"refId":"ABCD001"},{"errorCode":["E020"],"errorMessage":[" Date of application signed policyholder can not be future date"],"refId":"ABCD001"},{"errorCode":["E023"],"errorMessage":[" Date of application signed Proposed insured can not be future date"],"refId":"ABCD001"},{"errorCode":["E026"],"errorMessage":[" CoverType Can not be blank."],"refId":"ABCD001"},{"errorCode":["E028"],"errorMessage":[" Invalid CoverType."],"refId":"ABCD001"},{"errorCode":["E036"],"errorMessage":[" UTR Number can not be blank."],"refId":"ABCD001"},{"errorCode":["E038"],"errorMessage":[" UTR Date can not be blank."],"refId":"ABCD001"},{"errorCode":["E039"],"errorMessage":[" UTR Date can not be blank."],"refId":"ABCD001"},{"errorCode":["E058"],"errorMessage":["Mismatches in the premium calculated by the system and the premium amount collected."],"refId":"ABCD001"}],"status":"Fail"}}]', CAST(N'2023-01-11 17:37:12.583' AS DateTime), CAST(N'2023-01-11 17:37:09.860' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20003 AS Numeric(18, 0)), N'231101171053198', N'Glimpse Issuance API', N'192.168.1.50', N'200', N'Success', N'5212', N'{   "metaData": {     "partnerId": "GSRP000031"   },   "data": {     "customerDetails": {"sumAssured": 100000.00,"basicPremium": "1000.00","policyTerm": 12,"dob": "08/01/1997","dtapplsignedpolicyholder": "12/01/2023","personalDetails": {  "firstName": "srinivas chilveri",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"17/01/1989", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "A3",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "A2",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Service",  "mobile": 9890838902,  "PAN": "BHJBH9867J",  "email": "srinivas.chiliveri@comunus.in"},"dateOfIntimation": "12/01/2023","benefit_Coverage": "Life","refId": "ABCD001","utrAmt": "1000.00","dtapplsignedproposedinsured": "12/01/2023"     }   } }', CAST(N'2023-01-12 18:03:40.700' AS DateTime), N'[{"status":"Fail","errMsg":"MSG : You are not authorize to use this service ","ipAddr":"110.226.177.170"}]', CAST(N'2023-01-12 18:03:45.913' AS DateTime), CAST(N'2023-01-12 18:03:40.700' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20004 AS Numeric(18, 0)), N'231101171053198', N'Glimpse Issuance API', N'192.168.1.50', N'200', N'Success', N'2127', N'{   "metaData": {     "partnerId": "GSRP000031"   },   "data": {     "customerDetails": {"sumAssured": 100000.00,"basicPremium": "1000.00","policyTerm": 12,"dob": "08/01/1997","dtapplsignedpolicyholder": "12/01/2023","personalDetails": {  "firstName": "srinivas chilveri",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"17/01/1989", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "A3",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "A2",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Service",  "mobile": 9890838902,  "PAN": "BHJBH9867J",  "email": "srinivas.chiliveri@comunus.in"},"dateOfIntimation": "12/01/2023","benefit_Coverage": "Life","refId": "ABCD001","utrAmt": "1000.00","dtapplsignedproposedinsured": "12/01/2023"     }   } }', CAST(N'2023-01-12 18:05:19.927' AS DateTime), N'[{"status":"Fail","errMsg":"MSG : You are not authorize to use this service ","ipAddr":"110.226.177.170"}]', CAST(N'2023-01-12 18:05:22.053' AS DateTime), CAST(N'2023-01-12 18:05:19.927' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20005 AS Numeric(18, 0)), N'231101171053198', N'Glimpse Issuance API', N'192.168.1.50', N'200', N'Success', N'2349', N'{   "metaData": {     "partnerId": "GSRP000031"   },   "data": {     "customerDetails": {"sumAssured": 100000.00,"basicPremium": "1.00","policyTerm": 12,"dob": "08/01/1997","dtapplsignedpolicyholder": "12/01/2023","personalDetails": {  "firstName": "srinivas chilveri",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"17/01/1989", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "A3",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "A2",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Service",  "mobile": 9890838902,  "PAN": "BHJBH9867J",  "email": "srinivas.chiliveri@comunus.in"},"dateOfIntimation": "12/01/2023","benefit_Coverage": "Life","refId": "ABCD001","utrAmt": "1.00","dtapplsignedproposedinsured": "12/01/2023"     }   } }', CAST(N'2023-01-12 19:23:11.517' AS DateTime), N'[{"metaData":{"errors":[{"errorCode":["E004"],"errorMessage":[" Loan Amount Field Cannot be blank and zero."],"refId":"ABCD001"},{"errorCode":["E005"],"errorMessage":[" Loan Tenure can not be blank and Zero"],"refId":"ABCD001"},{"errorCode":["E008"],"errorMessage":[" Invalid Loan Disbursement date Date format."],"refId":"ABCD001"},{"errorCode":["E009"],"errorMessage":[" Date of Loan Disbursement can not be blank"],"refId":"ABCD001"},{"errorCode":["E010"],"errorMessage":[" Sub office Code can not be blank"],"refId":"ABCD001"},{"errorCode":["E011"],"errorMessage":[" Invalid Sub office Code"],"refId":"ABCD001"},{"errorCode":["E012"],"errorMessage":[" Applicant Type Can not be blank."],"refId":"ABCD001"},{"errorCode":["E016"],"errorMessage":[" Percentage Sharing cannot be blank and zero"],"refId":"ABCD001"},{"errorCode":["E017"],"errorMessage":[" Date of intimation can not be future date"],"refId":"ABCD001"},{"errorCode":["E020"],"errorMessage":[" Date of application signed policyholder can not be future date"],"refId":"ABCD001"},{"errorCode":["E023"],"errorMessage":[" Date of application signed Proposed insured can not be future date"],"refId":"ABCD001"},{"errorCode":["E026"],"errorMessage":[" CoverType Can not be blank."],"refId":"ABCD001"},{"errorCode":["E028"],"errorMessage":[" Invalid CoverType."],"refId":"ABCD001"},{"errorCode":["E036"],"errorMessage":[" UTR Number can not be blank."],"refId":"ABCD001"},{"errorCode":["E038"],"errorMessage":[" UTR Date can not be blank."],"refId":"ABCD001"},{"errorCode":["E039"],"errorMessage":[" UTR Date can not be blank."],"refId":"ABCD001"}],"status":"Fail"}}]', CAST(N'2023-01-12 19:23:13.867' AS DateTime), CAST(N'2023-01-12 19:23:11.517' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20006 AS Numeric(18, 0)), N'231101171053198', N'Glimpse Issuance API', N'192.168.1.50', N'200', N'Success', N'10694', N'{   "metaData": {     "partnerId": "GSRP000031"   },   "data": {     "customerDetails": {"sumAssured": 100000.00,"basicPremium": "1.00","policyTerm": 12,"dob": "08/01/1997","dtapplsignedpolicyholder": "12/01/2023","personalDetails": {  "firstName": "srinivas chilveri",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"17/01/1989", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "A3",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "A2",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Service",  "mobile": 9890838902,  "PAN": "BHJBH9867J",  "email": "srinivas.chiliveri@comunus.in"},"dateOfIntimation": "12/01/2023","benefit_Coverage": "Life","refId": "ABCD001","utrAmt": "1.00","dtapplsignedproposedinsured": "12/01/2023"     }   } }', CAST(N'2023-01-12 19:25:42.700' AS DateTime), N'[{"status":"Success","data":[{"customerId":"PQRST-10356666666660","coi":"1000000105","policyNo":"GSRP000037"}]}]', CAST(N'2023-01-12 19:25:53.397' AS DateTime), CAST(N'2023-01-12 19:25:42.700' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20007 AS Numeric(18, 0)), N'231101171053198', N'Glimpse Policy PDF', N'192.168.1.50', N'200', N'Success', N'1720', N'{"policyNumber":"GSRP000031","coi":1000000105}', CAST(N'2023-01-12 19:25:53.413' AS DateTime), N'{"PolicyInfoList":[{"PolicyInfo":"","ErrorMessage":""}],"PolicyNumber":"GSRP000031","COI":"1000000105"}', CAST(N'2023-01-12 19:25:55.133' AS DateTime), CAST(N'2023-01-12 19:25:53.413' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20008 AS Numeric(18, 0)), N'231101171053198', N'Glimpse Issuance API', N'192.168.1.50', N'200', N'Success', N'11408', N'{   "metaData": {     "partnerId": "GSRP000031"   },   "data": {     "customerDetails": {"sumAssured": 100000.00,"basicPremium": "1.00","policyTerm": 12,"dob": "08/01/1997","dtapplsignedpolicyholder": "12/01/2023","personalDetails": {  "firstName": "srinivas chilveri",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"17/01/1989", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "A3",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "A2",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Service",  "mobile": 9890838902,  "PAN": "BHJBH9867J",  "email": "srinivas.chiliveri@comunus.in"},"dateOfIntimation": "12/01/2023","benefit_Coverage": "Life","refId": "ABCD001","utrAmt": "1.00","dtapplsignedproposedinsured": "12/01/2023"     }   } }', CAST(N'2023-01-12 19:35:34.207' AS DateTime), N'[{"status":"Success","data":[{"customerId":"PQRST-10356666666660","coi":"1000000105","policyNo":"GSRP000037"}]}]', CAST(N'2023-01-12 19:35:45.617' AS DateTime), CAST(N'2023-01-12 19:35:34.207' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20009 AS Numeric(18, 0)), N'231101171053198', N'Glimpse Policy PDF', N'192.168.1.50', N'200', N'Success', N'358', N'{"policyNumber":"GSRP000031","coi":1000000105}', CAST(N'2023-01-12 19:35:45.637' AS DateTime), N'{"PolicyInfoList":[{"PolicyInfo":"","ErrorMessage":""}],"PolicyNumber":"GSRP000031","COI":"1000000105"}', CAST(N'2023-01-12 19:35:45.993' AS DateTime), CAST(N'2023-01-12 19:35:45.637' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20010 AS Numeric(18, 0)), N'231101171053198', N'Glimpse Issuance API', N'192.168.1.50', N'200', N'Success', N'16600', N'{   "metaData": {     "partnerId": "GSRP000031"   },   "data": {     "customerDetails": {"sumAssured": 100000.00,"basicPremium": "1.00","policyTerm": 12,"dob": "08/01/1997","dtapplsignedpolicyholder": "13/01/2023","personalDetails": {  "firstName": "srinivas chilveri",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"17/01/1989", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "A3",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "A2",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Service",  "mobile": 9890838902,  "PAN": "BHJBH9867J",  "email": "srinivas.chiliveri@comunus.in"},"dateOfIntimation": "13/01/2023","benefit_Coverage": "Life","refId": "ABCD001","utrAmt": "1.00","dtapplsignedproposedinsured": "13/01/2023"     }   } }', CAST(N'2023-01-13 13:02:45.827' AS DateTime), N'[{"status":"Success","data":[{"customerId":"PQRST-10356666666660","coi":"1000000105","policyNo":"GSRP000037"}]}]', CAST(N'2023-01-13 13:03:02.427' AS DateTime), CAST(N'2023-01-13 13:02:45.827' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20011 AS Numeric(18, 0)), N'231101171053198', N'Glimpse Policy PDF', N'192.168.1.50', N'200', N'Success', N'5478', N'{"policyNumber":"GSRP000031","coi":1000000105}', CAST(N'2023-01-13 13:03:02.447' AS DateTime), N'{"PolicyInfoList":[{"PolicyInfo":"","ErrorMessage":""}],"PolicyNumber":"GSRP000031","COI":"1000000105"}', CAST(N'2023-01-13 13:03:07.927' AS DateTime), CAST(N'2023-01-13 13:03:02.447' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20012 AS Numeric(18, 0)), N'231101171053198', N'Glimpse Issuance API', N'192.168.1.50', N'200', N'Success', N'12445', N'{   "metaData": {     "partnerId": "GSRP000031"   },   "data": {     "customerDetails": {"sumAssured": 100000.00,"basicPremium": "1.00","policyTerm": 12,"dob": "08/01/1997","dtapplsignedpolicyholder": "13/01/2023","personalDetails": {  "firstName": "srinivas chilveri",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"17/01/1989", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "A3",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "A2",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Service",  "mobile": 9890838902,  "PAN": "BHJBH9867J",  "email": "srinivas.chiliveri@comunus.in"},"dateOfIntimation": "13/01/2023","benefit_Coverage": "Life","refId": "ABCD001","utrAmt": "1.00","dtapplsignedproposedinsured": "13/01/2023"     }   } }', CAST(N'2023-01-13 13:05:32.557' AS DateTime), N'[{"status":"Success","data":[{"customerId":"PQRST-10356666666660","coi":"1000000105","policyNo":"GSRP000037"}]}]', CAST(N'2023-01-13 13:05:45.003' AS DateTime), CAST(N'2023-01-13 13:05:32.557' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20013 AS Numeric(18, 0)), N'231101171053198', N'Glimpse Policy PDF', N'192.168.1.50', N'200', N'Success', N'482', N'{"policyNumber":"GSRP000031","coi":1000000105}', CAST(N'2023-01-13 13:05:45.013' AS DateTime), N'{"PolicyInfoList":[{"PolicyInfo":"","ErrorMessage":""}],"PolicyNumber":"GSRP000031","COI":"1000000105"}', CAST(N'2023-01-13 13:05:45.497' AS DateTime), CAST(N'2023-01-13 13:05:45.013' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20014 AS Numeric(18, 0)), N'231101171053198', N'Glimpse Issuance API', N'192.168.1.50', N'200', N'Success', N'11656', N'{   "metaData": {     "partnerId": "GSRP000031"   },   "data": {     "customerDetails": {"sumAssured": 100000.00,"basicPremium": "1.00","policyTerm": 12,"dob": "08/01/1997","dtapplsignedpolicyholder": "13/01/2023","personalDetails": {  "firstName": "srinivas chilveri",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"17/01/1989", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "A3",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "A2",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Service",  "mobile": 9890838902,  "PAN": "BHJBH9867J",  "email": "srinivas.chiliveri@comunus.in"},"dateOfIntimation": "13/01/2023","benefit_Coverage": "Life","refId": "ABCD001","utrAmt": "1.00","dtapplsignedproposedinsured": "13/01/2023"     }   } }', CAST(N'2023-01-13 13:32:18.820' AS DateTime), N'[{"status":"Success","data":[{"customerId":"PQRST-10356666666660","coi":"1000000105","policyNo":"GSRP000037"}]}]', CAST(N'2023-01-13 13:32:30.477' AS DateTime), CAST(N'2023-01-13 13:32:18.820' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20015 AS Numeric(18, 0)), N'231101171053198', N'Glimpse Policy PDF', N'192.168.1.50', N'200', N'Success', N'2110', N'{"policyNumber":"GSRP000031","coi":1000000105}', CAST(N'2023-01-13 13:32:30.507' AS DateTime), N'{"PolicyInfoList":[{"PolicyInfo":"","ErrorMessage":""}],"PolicyNumber":"GSRP000031","COI":"1000000105"}', CAST(N'2023-01-13 13:32:32.617' AS DateTime), CAST(N'2023-01-13 13:32:30.507' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20016 AS Numeric(18, 0)), N'231101171053198', N'Glimpse Policy PDF', N'192.168.1.50', N'200', N'Success', N'1052', N'{"policyNumber":"GSRP000031","coi":1000000105}', CAST(N'2023-01-13 13:34:03.413' AS DateTime), N'{"PolicyInfoList":[{"PolicyInfo":"","ErrorMessage":""}],"PolicyNumber":"GSRP000031","COI":"1000000105"}', CAST(N'2023-01-13 13:34:04.467' AS DateTime), CAST(N'2023-01-13 13:34:03.413' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20017 AS Numeric(18, 0)), N'231101171053198', N'Glimpse Policy PDF', N'192.168.1.50', N'200', N'Success', N'664', N'{"policyNumber":"GSRP000031","coi":1000000105}', CAST(N'2023-01-13 13:34:24.407' AS DateTime), N'{"PolicyInfoList":[{"PolicyInfo":"","ErrorMessage":""}],"PolicyNumber":"GSRP000031","COI":"1000000105"}', CAST(N'2023-01-13 13:34:25.073' AS DateTime), CAST(N'2023-01-13 13:34:24.407' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20018 AS Numeric(18, 0)), N'231101171053198', N'Glimpse Policy PDF', N'192.168.1.50', N'200', N'Success', N'2905', N'{"policyNumber":"GSRP000031","coi":1000000105}', CAST(N'2023-01-13 14:23:24.587' AS DateTime), N'{"PolicyInfoList":[{"PolicyInfo":"","ErrorMessage":""}],"PolicyNumber":"GSRP000031","COI":"1000000105"}', CAST(N'2023-01-13 14:23:27.493' AS DateTime), CAST(N'2023-01-13 14:23:24.587' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20019 AS Numeric(18, 0)), N'231301165318670', N'Glimpse Issuance API', N'192.168.1.50', N'200', N'Success', N'12099', N'{   "metaData": {     "partnerId": "GSRP000031"   },   "data": {     "customerDetails": {"sumAssured": 10000.00,"basicPremium": "100.00","policyTerm": 12,"dob": "05/01/1988","dtapplsignedpolicyholder": "13/01/2023","personalDetails": {  "firstName": "Srinivas Chliveri",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"08/01/1996", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "A3",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "A2",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Business",  "mobile": 9890838902,  "PAN": "BHNKJ9870K",  "email": "srinivas.chiliveri@comunus.in"},"dateOfIntimation": "13/01/2023","benefit_Coverage": "Life","refId": "VISTARA-001","utrAmt": "100.00","dtapplsignedproposedinsured": "13/01/2023"     }   } }', CAST(N'2023-01-13 17:36:28.043' AS DateTime), N'[{"status":"Success","data":[{"customerId":"PQRST-10356666666660","coi":"1000000105","policyNo":"GSRP000037"}]}]', CAST(N'2023-01-13 17:36:40.143' AS DateTime), CAST(N'2023-01-13 17:36:28.043' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20020 AS Numeric(18, 0)), N'231301165318670', N'Glimpse Policy PDF', N'192.168.1.50', N'200', N'Success', N'1345', N'{"policyNumber":"GSRP000031","coi":1000000105}', CAST(N'2023-01-13 17:36:40.167' AS DateTime), N'{"PolicyInfoList":[{"PolicyInfo":"","ErrorMessage":""}],"PolicyNumber":"GSRP000031","COI":"1000000105"}', CAST(N'2023-01-13 17:36:41.517' AS DateTime), CAST(N'2023-01-13 17:36:40.167' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20021 AS Numeric(18, 0)), N'231301165318670', N'Glimpse Issuance API', N'192.168.1.50', N'200', N'Success', N'11666', N'{   "metaData": {     "partnerId": "GSRP000031"   },   "data": {     "customerDetails": {"sumAssured": 10000.00,"basicPremium": "100.00","policyTerm": 12,"dob": "05/01/1988","dtapplsignedpolicyholder": "13/01/2023","personalDetails": {  "firstName": "Srinivas Chliveri",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"08/01/1996", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "A3",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "A2",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Business",  "mobile": 9890838902,  "PAN": "BHNKJ9870K",  "email": "srinivas.chiliveri@comunus.in"},"dateOfIntimation": "13/01/2023","benefit_Coverage": "Life","refId": "VISTARA-001","utrAmt": "100.00","dtapplsignedproposedinsured": "13/01/2023"     }   } }', CAST(N'2023-01-13 17:43:40.267' AS DateTime), N'[{"status":"Success","data":[{"customerId":"PQRST-10356666666660","coi":"1000000105","policyNo":"GSRP000037"}]}]', CAST(N'2023-01-13 17:43:51.937' AS DateTime), CAST(N'2023-01-13 17:43:40.267' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20022 AS Numeric(18, 0)), N'231301165318670', N'Glimpse Policy PDF', N'192.168.1.50', N'200', N'Success', N'301', N'{"policyNumber":"GSRP000031","coi":1000000105}', CAST(N'2023-01-13 17:43:51.950' AS DateTime), N'{"PolicyInfoList":[{"PolicyInfo":"","ErrorMessage":""}],"PolicyNumber":"GSRP000031","COI":"1000000105"}', CAST(N'2023-01-13 17:43:52.253' AS DateTime), CAST(N'2023-01-13 17:43:51.950' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20023 AS Numeric(18, 0)), N'231101171053198', N'Glimpse Policy PDF', N'192.168.1.50', N'200', N'Success', N'1475', N'{"policyNumber":"GSRP000031","coi":1000000105}', CAST(N'2023-01-13 17:55:33.153' AS DateTime), N'{"PolicyInfoList":[{"PolicyInfo":"","ErrorMessage":""}],"PolicyNumber":"GSRP000031","COI":"1000000105"}', CAST(N'2023-01-13 17:55:34.627' AS DateTime), CAST(N'2023-01-13 17:55:33.153' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20024 AS Numeric(18, 0)), N'231301165318670', N'Glimpse Issuance API', N'192.168.1.50', N'200', N'Success', N'10968', N'{   "metaData": {     "partnerId": "GSRP000031"   },   "data": {     "customerDetails": {"sumAssured": 10000.00,"basicPremium": "100.00","policyTerm": 12,"dob": "05/01/1988","dtapplsignedpolicyholder": "13/01/2023","personalDetails": {  "firstName": "Srinivas Chliveri",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"08/01/1996", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "A3",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "A2",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Business",  "mobile": 9890838902,  "PAN": "BHNKJ9870K",  "email": "srinivas.chiliveri@comunus.in"},"dateOfIntimation": "13/01/2023","benefit_Coverage": "Life","refId": "VISTARA-001","utrAmt": "100.00","dtapplsignedproposedinsured": "13/01/2023"     }   } }', CAST(N'2023-01-13 18:19:02.407' AS DateTime), N'[{"status":"Success","data":[{"customerId":"PQRST-10356666666660","coi":"1000000105","policyNo":"GSRP000037"}]}]', CAST(N'2023-01-13 18:19:13.373' AS DateTime), CAST(N'2023-01-13 18:19:02.407' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20025 AS Numeric(18, 0)), N'231301165318670', N'Glimpse Policy PDF', N'192.168.1.50', N'200', N'Success', N'1313', N'{"policyNumber":"GSRP000031","coi":1000000105}', CAST(N'2023-01-13 18:19:13.390' AS DateTime), N'{"PolicyInfoList":[{"PolicyInfo":"","ErrorMessage":""}],"PolicyNumber":"GSRP000031","COI":"1000000105"}', CAST(N'2023-01-13 18:19:14.703' AS DateTime), CAST(N'2023-01-13 18:19:13.390' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20026 AS Numeric(18, 0)), N'231301165318670', N'Glimpse Policy PDF', N'192.168.1.50', N'200', N'Success', N'662', N'{"policyNumber":"GSRP000031","coi":1000000105}', CAST(N'2023-01-13 18:20:09.030' AS DateTime), N'{"PolicyInfoList":[{"PolicyInfo":"","ErrorMessage":""}],"PolicyNumber":"GSRP000031","COI":"1000000105"}', CAST(N'2023-01-13 18:20:09.690' AS DateTime), CAST(N'2023-01-13 18:20:09.030' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20027 AS Numeric(18, 0)), N'231301191820106', N'Glimpse Issuance API', N'192.168.1.50', N'200', N'Success', N'10910', N'{   "metaData": {     "partnerId": "GSRP000031"   },   "data": {     "customerDetails": {"sumAssured": 10000.00,"basicPremium": "10.00","policyTerm": 12,"dob": "01/01/1988","dtapplsignedpolicyholder": "13/01/2023","personalDetails": {  "firstName": "Karthik Chiliveri",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"05/01/2000", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Professional",  "mobile": 9890838902,  "PAN": "NHJBH7687U",  "email": "karthik.chiliveru@comunus.in"},"dateOfIntimation": "13/01/2023","benefit_Coverage": "Life","refId": "VISTARA-002","utrAmt": "10.00","dtapplsignedproposedinsured": "13/01/2023"     }   } }', CAST(N'2023-01-13 19:46:53.357' AS DateTime), N'[{"status":"Success","data":[{"customerId":"PQRST-10356666666660","coi":"1000000105","policyNo":"GSRP000037"}]}]', CAST(N'2023-01-13 19:47:04.267' AS DateTime), CAST(N'2023-01-13 19:46:53.357' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20028 AS Numeric(18, 0)), N'231301191820106', N'Glimpse Policy PDF', N'192.168.1.50', N'200', N'Success', N'1296', N'{"policyNumber":"GSRP000031","coi":1000000105}', CAST(N'2023-01-13 19:47:04.287' AS DateTime), N'{"PolicyInfoList":[{"PolicyInfo":"","ErrorMessage":""}],"PolicyNumber":"GSRP000031","COI":"1000000105"}', CAST(N'2023-01-13 19:47:05.583' AS DateTime), CAST(N'2023-01-13 19:47:04.287' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20029 AS Numeric(18, 0)), N'231301191820106', N'Glimpse Issuance API', N'192.168.1.50', N'200', N'Success', N'1558', N'{   "metaData": {     "partnerId": "GSRP000031"   },   "data": {     "customerDetails": {"sumAssured": 10000.00,"basicPremium": "10.00","policyTerm": 12,"dob": "01/01/1988","dtapplsignedpolicyholder": "13/01/2023","personalDetails": {  "firstName": "Karthik Chiliveri",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"05/01/2000", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Professional",  "mobile": 9890838902,  "PAN": "NHJBH7687U",  "email": "karthik.chiliveru@comunus.in"},"dateOfIntimation": "13/01/2023","benefit_Coverage": "Life","refId": "VISTARA-002","utrAmt": "10.00","dtapplsignedproposedinsured": "13/01/2023"     }   } }', CAST(N'2023-01-13 19:56:04.197' AS DateTime), N'[{"metaData":{"errors":[{"errorCode":["E004"],"errorMessage":[" Loan Amount Field Cannot be blank and zero."],"refId":"VISTARA-002"},{"errorCode":["E005"],"errorMessage":[" Loan Tenure can not be blank and Zero"],"refId":"VISTARA-002"},{"errorCode":["E008"],"errorMessage":[" Invalid Loan Disbursement date Date format."],"refId":"VISTARA-002"},{"errorCode":["E009"],"errorMessage":[" Date of Loan Disbursement can not be blank"],"refId":"VISTARA-002"},{"errorCode":["E010"],"errorMessage":[" Sub office Code can not be blank"],"refId":"VISTARA-002"},{"errorCode":["E011"],"errorMessage":[" Invalid Sub office Code"],"refId":"VISTARA-002"},{"errorCode":["E012"],"errorMessage":[" Applicant Type Can not be blank."],"refId":"VISTARA-002"},{"errorCode":["E016"],"errorMessage":[" Percentage Sharing cannot be blank and zero"],"refId":"VISTARA-002"},{"errorCode":["E026"],"errorMessage":[" CoverType Can not be blank."],"refId":"VISTARA-002"},{"errorCode":["E028"],"errorMessage":[" Invalid CoverType."],"refId":"VISTARA-002"},{"errorCode":["E036"],"errorMessage":[" UTR Number can not be blank."],"refId":"VISTARA-002"},{"errorCode":["E038"],"errorMessage":[" UTR Date can not be blank."],"refId":"VISTARA-002"},{"errorCode":["E039"],"errorMessage":[" UTR Date can not be blank."],"refId":"VISTARA-002"}],"status":"Fail"}}]', CAST(N'2023-01-13 19:56:05.757' AS DateTime), CAST(N'2023-01-13 19:56:04.197' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20030 AS Numeric(18, 0)), N'231301191820106', N'Glimpse Issuance API', N'192.168.1.50', N'200', N'Success', N'1453', N'{   "metaData": {     "partnerId": "GSRP000031"   },   "data": {     "customerDetails": {"sumAssured": 10000.00,"basicPremium": "10.00","policyTerm": 12,"dob": "01/01/1988","dtapplsignedpolicyholder": "13/01/2023","personalDetails": {  "firstName": "Karthik Chiliveri",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"05/01/2000", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Professional",  "mobile": 9890838902,  "PAN": "NHJBH7687U",  "email": "karthik.chiliveru@comunus.in"},"dateOfIntimation": "13/01/2023","benefit_Coverage": "Life","refId": "VISTARA-002","utrAmt": "10.00","dtapplsignedproposedinsured": "13/01/2023"     }   } }', CAST(N'2023-01-13 19:56:23.427' AS DateTime), N'[{"metaData":{"errors":[{"errorCode":["E004"],"errorMessage":[" Loan Amount Field Cannot be blank and zero."],"refId":"VISTARA-002"},{"errorCode":["E005"],"errorMessage":[" Loan Tenure can not be blank and Zero"],"refId":"VISTARA-002"},{"errorCode":["E008"],"errorMessage":[" Invalid Loan Disbursement date Date format."],"refId":"VISTARA-002"},{"errorCode":["E009"],"errorMessage":[" Date of Loan Disbursement can not be blank"],"refId":"VISTARA-002"},{"errorCode":["E010"],"errorMessage":[" Sub office Code can not be blank"],"refId":"VISTARA-002"},{"errorCode":["E011"],"errorMessage":[" Invalid Sub office Code"],"refId":"VISTARA-002"},{"errorCode":["E012"],"errorMessage":[" Applicant Type Can not be blank."],"refId":"VISTARA-002"},{"errorCode":["E016"],"errorMessage":[" Percentage Sharing cannot be blank and zero"],"refId":"VISTARA-002"},{"errorCode":["E026"],"errorMessage":[" CoverType Can not be blank."],"refId":"VISTARA-002"},{"errorCode":["E028"],"errorMessage":[" Invalid CoverType."],"refId":"VISTARA-002"},{"errorCode":["E036"],"errorMessage":[" UTR Number can not be blank."],"refId":"VISTARA-002"},{"errorCode":["E038"],"errorMessage":[" UTR Date can not be blank."],"refId":"VISTARA-002"},{"errorCode":["E039"],"errorMessage":[" UTR Date can not be blank."],"refId":"VISTARA-002"}],"status":"Fail"}}]', CAST(N'2023-01-13 19:56:24.880' AS DateTime), CAST(N'2023-01-13 19:56:23.427' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20031 AS Numeric(18, 0)), N'231301191820106', N'Glimpse Issuance API', N'192.168.1.50', N'200', N'Success', N'10732', N'{   "metaData": {     "partnerId": "GSRP000031"   },   "data": {     "customerDetails": {"sumAssured": 10000.00,"basicPremium": "10.00","policyTerm": 12,"dob": "01/01/1988","dtapplsignedpolicyholder": "13/01/2023","personalDetails": {  "firstName": "Karthik Chiliveri",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"05/01/2000", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Professional",  "mobile": 9890838902,  "PAN": "NHJBH7687U",  "email": "karthik.chiliveru@comunus.in"},"dateOfIntimation": "13/01/2023","benefit_Coverage": "Life","refId": "VISTARA-002","utrAmt": "10.00","dtapplsignedproposedinsured": "13/01/2023"     }   } }', CAST(N'2023-01-13 19:56:49.153' AS DateTime), N'[{"status":"Success","data":[{"customerId":"PQRST-10356666666660","coi":"1000000105","policyNo":"GSRP000037"}]}]', CAST(N'2023-01-13 19:56:59.887' AS DateTime), CAST(N'2023-01-13 19:56:49.153' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20032 AS Numeric(18, 0)), N'231301191820106', N'Glimpse Policy PDF', N'192.168.1.50', N'200', N'Success', N'90', N'{"policyNumber":"GSRP000031","coi":1000000105}', CAST(N'2023-01-13 19:56:59.900' AS DateTime), N'{"PolicyInfoList":[{"PolicyInfo":"","ErrorMessage":""}],"PolicyNumber":"GSRP000031","COI":"1000000105"}', CAST(N'2023-01-13 19:56:59.993' AS DateTime), CAST(N'2023-01-13 19:56:59.900' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20033 AS Numeric(18, 0)), N'231301191820106', N'Glimpse Issuance API', N'192.168.1.50', N'200', N'Success', N'1444', N'{   "metaData": {     "partnerId": "GSRP000031"   },   "data": {     "customerDetails": {"sumAssured": 10000.00,"basicPremium": "10.00","policyTerm": 12,"dob": "01/01/1988","dtapplsignedpolicyholder": "13/01/2023","personalDetails": {  "firstName": "Karthik Chiliveri",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"05/01/2000", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Professional",  "mobile": 9890838902,  "PAN": "NHJBH7687U",  "email": "karthik.chiliveru@comunus.in"},"dateOfIntimation": "13/01/2023","benefit_Coverage": "Life","refId": "VISTARA-002","utrAmt": "10.00","dtapplsignedproposedinsured": "13/01/2023"     }   } }', CAST(N'2023-01-13 20:00:47.877' AS DateTime), N'[{"metaData":{"errors":[{"errorCode":["E004"],"errorMessage":[" Loan Amount Field Cannot be blank and zero."],"refId":"VISTARA-002"},{"errorCode":["E005"],"errorMessage":[" Loan Tenure can not be blank and Zero"],"refId":"VISTARA-002"},{"errorCode":["E008"],"errorMessage":[" Invalid Loan Disbursement date Date format."],"refId":"VISTARA-002"},{"errorCode":["E009"],"errorMessage":[" Date of Loan Disbursement can not be blank"],"refId":"VISTARA-002"},{"errorCode":["E010"],"errorMessage":[" Sub office Code can not be blank"],"refId":"VISTARA-002"},{"errorCode":["E011"],"errorMessage":[" Invalid Sub office Code"],"refId":"VISTARA-002"},{"errorCode":["E012"],"errorMessage":[" Applicant Type Can not be blank."],"refId":"VISTARA-002"},{"errorCode":["E016"],"errorMessage":[" Percentage Sharing cannot be blank and zero"],"refId":"VISTARA-002"},{"errorCode":["E026"],"errorMessage":[" CoverType Can not be blank."],"refId":"VISTARA-002"},{"errorCode":["E028"],"errorMessage":[" Invalid CoverType."],"refId":"VISTARA-002"},{"errorCode":["E036"],"errorMessage":[" UTR Number can not be blank."],"refId":"VISTARA-002"},{"errorCode":["E038"],"errorMessage":[" UTR Date can not be blank."],"refId":"VISTARA-002"},{"errorCode":["E039"],"errorMessage":[" UTR Date can not be blank."],"refId":"VISTARA-002"}],"status":"Fail"}}]', CAST(N'2023-01-13 20:00:49.323' AS DateTime), CAST(N'2023-01-13 20:00:47.877' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20034 AS Numeric(18, 0)), N'231301191820106', N'Glimpse Issuance API', N'192.168.1.50', N'200', N'Success', N'10547', N'{   "metaData": {     "partnerId": "GSRP000031"   },   "data": {     "customerDetails": {"sumAssured": 10000.00,"basicPremium": "10.00","policyTerm": 12,"dob": "01/01/1988","dtapplsignedpolicyholder": "13/01/2023","personalDetails": {  "firstName": "Karthik Chiliveri",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"05/01/2000", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Professional",  "mobile": 9890838902,  "PAN": "NHJBH7687U",  "email": "karthik.chiliveru@comunus.in"},"dateOfIntimation": "13/01/2023","benefit_Coverage": "Life","refId": "VISTARA-002","utrAmt": "10.00","dtapplsignedproposedinsured": "13/01/2023"     }   } }', CAST(N'2023-01-13 20:01:30.310' AS DateTime), N'[{"status":"Success","data":[{"customerId":"PQRST-10356666666660","coi":"1000000105","policyNo":"GSRP000037"}]}]', CAST(N'2023-01-13 20:01:40.857' AS DateTime), CAST(N'2023-01-13 20:01:30.310' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20035 AS Numeric(18, 0)), N'231301191820106', N'Glimpse Policy PDF', N'192.168.1.50', N'200', N'Success', N'329', N'{"policyNumber":"GSRP000031","coi":1000000105}', CAST(N'2023-01-13 20:01:40.870' AS DateTime), N'{"PolicyInfoList":[{"PolicyInfo":"","ErrorMessage":""}],"PolicyNumber":"GSRP000031","COI":"1000000105"}', CAST(N'2023-01-13 20:01:41.200' AS DateTime), CAST(N'2023-01-13 20:01:40.870' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20036 AS Numeric(18, 0)), N'231301191820106', N'Glimpse Issuance API', N'192.168.1.50', N'200', N'Success', N'12427', N'{   "metaData": {     "partnerId": "GSRP000031"   },   "data": {     "customerDetails": {"sumAssured": 10000.00,"basicPremium": "10.00","policyTerm": 12,"dob": "01/01/1988","dtapplsignedpolicyholder": "13/01/2023","personalDetails": {  "firstName": "Karthik Chiliveri",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"05/01/2000", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Professional",  "mobile": 9890838902,  "PAN": "NHJBH7687U",  "email": "karthik.chiliveru@comunus.in"},"dateOfIntimation": "13/01/2023","benefit_Coverage": "Life","refId": "VISTARA-002","utrAmt": "10.00","dtapplsignedproposedinsured": "13/01/2023"     }   } }', CAST(N'2023-01-13 20:04:40.543' AS DateTime), N'[{"status":"Success","data":[{"customerId":"PQRST-10356666666660","coi":"1000000105","policyNo":"GSRP000037"}]}]', CAST(N'2023-01-13 20:04:52.973' AS DateTime), CAST(N'2023-01-13 20:04:40.543' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20037 AS Numeric(18, 0)), N'231301191820106', N'Glimpse Policy PDF', N'192.168.1.50', N'200', N'Success', N'653', N'{"policyNumber":"GSRP000031","coi":1000000105}', CAST(N'2023-01-13 20:04:52.997' AS DateTime), N'{"PolicyInfoList":[{"PolicyInfo":"","ErrorMessage":""}],"PolicyNumber":"GSRP000031","COI":"1000000105"}', CAST(N'2023-01-13 20:04:53.650' AS DateTime), CAST(N'2023-01-13 20:04:52.997' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20038 AS Numeric(18, 0)), N'231301191820106', N'Glimpse Issuance API', N'192.168.1.50', N'200', N'Success', N'11141', N'{   "metaData": {     "partnerId": "GSRP000031"   },   "data": {     "customerDetails": {"sumAssured": 10000.00,"basicPremium": "10.00","policyTerm": 12,"dob": "01/01/1988","dtapplsignedpolicyholder": "13/01/2023","personalDetails": {  "firstName": "Karthik Chiliveri",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"05/01/2000", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Professional",  "mobile": 9890838902,  "PAN": "NHJBH7687U",  "email": "karthik.chiliveru@comunus.in"},"dateOfIntimation": "13/01/2023","benefit_Coverage": "Life","refId": "VISTARA-002","utrAmt": "10.00","dtapplsignedproposedinsured": "13/01/2023"     }   } }', CAST(N'2023-01-13 20:07:51.183' AS DateTime), N'[{"status":"Success","data":[{"customerId":"PQRST-10356666666660","coi":"1000000105","policyNo":"GSRP000037"}]}]', CAST(N'2023-01-13 20:08:02.327' AS DateTime), CAST(N'2023-01-13 20:07:51.183' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20039 AS Numeric(18, 0)), N'231301191820106', N'Glimpse Policy PDF', N'192.168.1.50', N'200', N'Success', N'196', N'{"policyNumber":"GSRP000031","coi":1000000105}', CAST(N'2023-01-13 20:08:02.347' AS DateTime), N'{"PolicyInfoList":[{"PolicyInfo":"","ErrorMessage":""}],"PolicyNumber":"GSRP000031","COI":"1000000105"}', CAST(N'2023-01-13 20:08:02.543' AS DateTime), CAST(N'2023-01-13 20:08:02.347' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(20040 AS Numeric(18, 0)), N'231301191820106', N'Glimpse Policy PDF', N'192.168.1.50', N'200', N'Success', N'3180', N'{"policyNumber":"GSRP000031","coi":1000000105}', CAST(N'2023-01-13 20:08:09.280' AS DateTime), N'{"PolicyInfoList":[{"PolicyInfo":"","ErrorMessage":""}],"PolicyNumber":"GSRP000031","COI":"1000000105"}', CAST(N'2023-01-13 20:08:12.460' AS DateTime), CAST(N'2023-01-13 20:08:09.280' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(30003 AS Numeric(18, 0)), N'231101171053198', N'Glimpse Policy PDF', N'192.168.1.51', N'200', N'Success', N'1927', N'{"policyNumber":"GSRP000031","coi":1000000105}', CAST(N'2023-01-16 15:32:05.477' AS DateTime), N'{"Status":"Fail","IPAddr":"110.226.179.67","ErrMsg":"MSG : You are not authorize to use this service "}', CAST(N'2023-01-16 15:32:07.413' AS DateTime), CAST(N'2023-01-16 15:32:05.477' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(30004 AS Numeric(18, 0)), N'231101171053198', N'Glimpse Policy PDF', N'192.168.1.51', N'200', N'Success', N'615', N'{"policyNumber":"GSRP000031","coi":1000000105}', CAST(N'2023-01-16 15:32:17.393' AS DateTime), N'{"Status":"Fail","IPAddr":"110.226.179.67","ErrMsg":"MSG : You are not authorize to use this service "}', CAST(N'2023-01-16 15:32:18.010' AS DateTime), CAST(N'2023-01-16 15:32:17.393' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(30005 AS Numeric(18, 0)), N'231301165318670', N'Glimpse Issuance API', N'192.168.1.51', N'200', N'Success', N'3223', N'{   "metaData": {     "partnerId": "GSRP000031"   },   "data": {     "customerDetails": {"sumAssured": 10000.00,"basicPremium": "100.00","policyTerm": 12,"dob": "05/01/1988","dtapplsignedpolicyholder": "17/01/2023","personalDetails": {  "firstName": "Srinivas Chliveri",  "lastName": "",  "nomineeDetails":[ { "relationshipToCustomer":"Brother", "firstName":"Nominee One", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"Male", "percentageAllocation":"100", "dob":"08/01/1996", "mobile":"", "title":"Mr", "appointeeDetails":{ "firstName":"", "lastName":"", "address":{ "area":"", "city":"", "houseOrFlat":"", "street":"", "pinCode":"", "state":"" }, "gender":"", "percentageAllocation":"100", "dob":"", "relationshipToNominee":"", "mobile":"", "title":"" } } ],  "Annual_Income": 100000,  "address": {    "area": "A3",    "country": "India",    "city": "Thane",    "houseOrFlat": "A1",    "street": "A2",    "pinCode": "400604",    "state": "Maharashtra"  },  "gender": "Male",   "Occupation_details": "Business",  "mobile": 9890838902,  "PAN": "BHNKJ9870K",  "email": "srinivas.chiliveri@comunus.in"},"dateOfIntimation": "17/01/2023","benefit_Coverage": "Life","refId": "VISTARA-001","utrAmt": "100.00","dtapplsignedproposedinsured": "17/01/2023"     }   } }', CAST(N'2023-01-17 16:58:24.277' AS DateTime), N'[{"status":"Fail","errMsg":"MSG : You are not authorize to use this service ","ipAddr":"110.226.179.67"}]', CAST(N'2023-01-17 16:58:27.497' AS DateTime), CAST(N'2023-01-17 16:58:24.277' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(40003 AS Numeric(18, 0)), N'231301191820106', N'Glimpse Issuance API', N'192.168.1.67', N'200', N'Success', N'759', N'{"metaData": {"partnerId": "GSRP000031"},"data": {"customerDetails": {    "refId": "VISTARA-002",    "utrAmt": "10.00",    "sumAssured": "10000.00",    "dob": "01/01/1988",    "subofficeCode": "VTARA001",   "basicPremium": "10.00",    "policyTerm": "12",    "dateOfIntimation": "29/01/2022",   "dtapplsignedpolicyholder": "29/01/2022",    "dtapplsignedproposedinsured": "29/01/2022",    "benefit_Coverage": "Life",    "personalDetails": {   "firstName": "Karthik Chiliveri",   "lastName": "",   "dob": "01/01/1988",   "mobile": "9890838902",   "email": "karthik.chiliveru@comunus.in",   "gender": "Male",   "customer_classification": "Gold",   "occupation_code": "",   "Customer_ID": "23421",   "organization": "",   "PAN_": "NHJBH7687U",   "Annual_Income": "100000",   "address": {   "houseOrFlat": "A1",   "street": "",   "area": "",   "city": "Thane",   "state": "Maharashtra",   "country": "India",   "pinCode": "400604"   },   "nomineeDetails": [ {"title": "Mr","firstName": "","dob": "05/01/2000","gender": "Male","relationshipToCustomer": "Brother","percentageAllocation": "100","appointeeDetails": {"title": "","firstName": "","dob": "","relationshipToNominee": ""} }]    }}} }', CAST(N'2023-02-07 03:12:49.653' AS DateTime), N'[{"status":"Fail","errMsg":"MSG : You are not authorize to use this service ","ipAddr":"110.226.183.146"}]', CAST(N'2023-02-09 10:46:10.410' AS DateTime), CAST(N'2023-02-07 03:12:49.653' AS DateTime))
INSERT [dbo].[TGP_SERVICE_CALL_LOGS] ([ID], [CUSTOMER_ID], [SERVICE], [SOURCE_IP], [STATUS_CODE], [ERROR_MSG], [TIME_TAKEN], [REQUEST_OBJ], [REQUEST_TIME], [RESPONSE_OBJ], [RESPONSE_TIME], [CREATED_DATE]) VALUES (CAST(40004 AS Numeric(18, 0)), N'231301191820106', N'Glimpse Issuance API', N'192.168.1.67', N'200', N'Success', N'2907', N'{"metaData": {"partnerId": "GSRP000031"},"data": {"customerDetails": {    "refId": "VISTARA-002",    "utrAmt": "10.00",    "sumAssured": "10000.00",    "dob": "01/01/1988",    "subofficeCode": "VTARA001",   "basicPremium": "10.00",    "policyTerm": "12",    "dateOfIntimation": "29/01/2022",   "dtapplsignedpolicyholder": "29/01/2022",    "dtapplsignedproposedinsured": "29/01/2022",    "benefit_Coverage": "Life",    "personalDetails": {   "firstName": "Karthik Chiliveri",   "lastName": "",   "dob": "01/01/1988",   "mobile": "9890838902",   "email": "karthik.chiliveru@comunus.in",   "gender": "Male",   "customer_classification": "Gold",   "occupation_code": "",   "Customer_ID": "23421",   "organization": "",   "PAN_": "NHJBH7687U",   "Annual_Income": "100000",   "address": {   "houseOrFlat": "A1",   "street": "",   "area": "",   "city": "Thane",   "state": "Maharashtra",   "country": "India",   "pinCode": "400604"   },   "nomineeDetails": [ {"title": "Mr","firstName": "","dob": "05/01/2000","gender": "Male","relationshipToCustomer": "Brother","percentageAllocation": "100","appointeeDetails": {"title": "","firstName": "","dob": "","relationshipToNominee": ""} }]    }}} }', CAST(N'2023-02-07 03:13:39.110' AS DateTime), N'[]', CAST(N'2023-02-09 10:47:02.017' AS DateTime), CAST(N'2023-02-07 03:13:39.110' AS DateTime))
SET IDENTITY_INSERT [dbo].[TGP_SERVICE_CALL_LOGS] OFF
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Andhra Pradesh', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Arunachal Pradesh', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Assam', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Bihar', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Chhattisgarh', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Goa', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Gujarat', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Haryana', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Himachal Pradesh', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Jharkhand', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Karnataka', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Kerala', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Madhya Pradesh', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Maharashtra', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Manipur', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Meghalaya', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Mizoram', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Nagaland', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Odisha', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Punjab', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Rajasthan', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Sikkim', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Tamil Nadu', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Telangana', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Tripura', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Uttar Pradesh', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'Uttarakhand', N'Y')
INSERT [dbo].[TGP_STATE_MASTER] ([ID], [STATE_NAME], [STATUS]) VALUES (NULL, N'West Bengal', N'Y')
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[46] 4[14] 2[28] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "TGP_PERSONAL_DETAILS"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 297
               Right = 223
            End
            DisplayFlags = 280
            TopColumn = 5
         End
         Begin Table = "TGP_NOMINEE_DETAILS"
            Begin Extent = 
               Top = 15
               Left = 398
               Bottom = 145
               Right = 629
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "TGP_POLICY_MASTER"
            Begin Extent = 
               Top = 146
               Left = 307
               Bottom = 339
               Right = 503
            End
            DisplayFlags = 280
            TopColumn = 3
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1176
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1356
         SortOrder = 1416
         GroupBy = 1350
         Filter = 1356
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_ALL_DETAILS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_ALL_DETAILS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "TGP_MERCHANT_MASTER"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 278
               Right = 299
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "TGP_MERCHANT_POLICY_MAPPING"
            Begin Extent = 
               Top = 1
               Left = 343
               Bottom = 260
               Right = 579
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TGP_POLICY_MASTER"
            Begin Extent = 
               Top = 7
               Left = 606
               Bottom = 247
               Right = 924
            End
            DisplayFlags = 280
            TopColumn = 4
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1176
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1356
         SortOrder = 1416
         GroupBy = 1350
         Filter = 1356
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_POLICY_DETAILS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_POLICY_DETAILS'
GO
USE [master]
GO
ALTER DATABASE [TGP] SET  READ_WRITE 
GO
